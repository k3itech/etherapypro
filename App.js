import React, { Component } from 'react'
import {
  StatusBar,
  View,
  AppState
} from 'react-native'
import NetInfo from '@react-native-community/netinfo'
import { Provider } from 'react-redux'
import PushNotification from 'react-native-push-notification'
import moment from 'moment'
import AppRoute from './src/Routers/App'
import store from './src/Utility/ReduxStore'
import Colors from './src/common/Colors'
import { handleNetworkConnectivityStatus } from './src/Redux/Actions'
import Functions from './src/Utility/Functions'

export default class App extends Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange)
    this.netInfoUnSubscribe = NetInfo.addEventListener(this.handleConnectivityChange)
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange)
    if (this.netInfoUnSubscribe) {
      this.netInfoUnSubscribe()
      this.netInfoUnSubscribe
    }
  }

  handleConnectivityChange = (networkStatusData) => {
    if (networkStatusData.isConnected != store.getState().auth.isNetworkConnected) {
      store.dispatch(handleNetworkConnectivityStatus(networkStatusData.isConnected))
    }
  }

  _handleAppStateChange = nextAppState => {

    console.log('nextAppState = ', nextAppState)

    if (nextAppState == 'background' || nextAppState == 'inactive') {
      global.backgroundUnixTime = moment(new Date()).unix() - 30
    }
    else if (nextAppState == 'active') {
      Functions.connectToSocketMethod()

      PushNotification.cancelAllLocalNotifications()

      console.log('current time = ', moment(new Date()).unix())
      console.log('global.backgroundUnixTime = ', global.backgroundUnixTime)
    }
  }

  render() {
    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <StatusBar
            hidden={false}
            backgroundColor={Colors.whiteFour}
            barStyle='dark-content'
          />
          <AppRoute />
        </View>
      </Provider>
    )
  }
}