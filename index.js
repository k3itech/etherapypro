import { 
    AppRegistry,
    LogBox
} from 'react-native'
import KeyboardManager from 'react-native-keyboard-manager'
import App from './App'
import { name as appName } from './app.json'

if (Platform.OS == 'ios') {
    KeyboardManager.setEnable(true)
    KeyboardManager.setEnableDebugging(false)
    KeyboardManager.setKeyboardDistanceFromTextField(10)
    KeyboardManager.setPreventShowingBottomBlankSpace(true)
    KeyboardManager.setEnableAutoToolbar(true)
    KeyboardManager.setToolbarDoneBarButtonItemText('Done')
    KeyboardManager.setToolbarPreviousNextButtonEnable(true)
    KeyboardManager.setToolbarManageBehaviour(0)
    KeyboardManager.setShouldToolbarUsesTextFieldTintColor(false)
    KeyboardManager.setShouldShowTextFieldPlaceholder(true) // deprecated, use setShouldShowToolbarPlaceholder
    KeyboardManager.setShouldShowToolbarPlaceholder(true)
    KeyboardManager.setOverrideKeyboardAppearance(false)
    KeyboardManager.setShouldResignOnTouchOutside(true)
    KeyboardManager.resignFirstResponder()
    KeyboardManager.isKeyboardShowing()
        .then((isShowing) => {
            // ...
        })
}

LogBox.ignoreAllLogs(true)

AppRegistry.registerComponent(appName, () => App)