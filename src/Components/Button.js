//import liraries
import React, { Component } from 'react'
import {
  TouchableOpacity,
  Text,
  Image
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { vw } from '../common/ViewportUnits'

// create a component
class Button extends Component {

  render() {

    const {
      text,
      bgColor,
      width,
      height,
      icon,
      iconColor,
      size,
      textColor,
      disabled,
      borderColor,
      borderStyle,
      image,
      showImage,
      borderRadius,
      shadowColor,
      elevation,
      fontSize,
      fontFamily,
      onPress
    } = this.props

    var Button = {
      height: height,
      width: width,
      borderRadius: borderRadius,
      justifyContent: 'center',
      backgroundColor: bgColor,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      borderWidth: vw(1),
      borderStyle: borderStyle,
      borderColor: borderColor,
      shadowColor: shadowColor == null ? 'transparent' : shadowColor,
      shadowOffset: {
        width: 0,
        height: vw(4)
      },
      shadowRadius: vw(16),
      shadowOpacity: 1,
      elevation: elevation == null ? 0 : elevation
    }

    var textStyle = {
      fontFamily: fontFamily,
      fontSize: fontSize,
      color: textColor
    }

    var imageIconStyle = {
      width: vw(18),
      height: '45%'
    }

    return (
      <TouchableOpacity style={Button} onPress={onPress} disabled={disabled}>
        {
          showImage === true
            ?
            (
              <Image
                style={imageIconStyle}
                resizeMode='contain'
                source={image}
              />
            )
            :
            (
              <Icon
                color={iconColor}
                name={icon}
                size={size}
              />
            )
        }
        <Text style={textStyle}>{text}</Text>
      </TouchableOpacity>
    )
  }
}

// make this component available to the app
export default Button