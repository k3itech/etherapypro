import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TextInput
} from 'react-native'
import Colors from '../common/Colors'
import Fonts from '../common/Fonts'
import { vw } from '../common/ViewportUnits'

// create a component
class CardInput extends Component {

    // render ui
    render() {
        const {
            placeholder,
            onChangeText,
            isSecure,
            keyboardType,
            value,
            editable,
            maxLength,
            autoCapitalize,
            label,
            isWhiteBg,
            multiline
        } = this.props

        const inputInnerStyle = {
            flexDirection: 'row',
            width: '100%',
            height: multiline ? vw(100) : vw(45),
            alignItems: 'center',
            borderRadius: 5,
            shadowColor: Colors.shadowColor,
            shadowOffset: {
                width: 0,
                height: vw(6)
            },
            shadowOpacity: 1,
            elevation: 10,
            backgroundColor: Colors.whiteFour,
            borderWidth: 2,
            borderColor: isWhiteBg ? Colors.whiteFour : Colors.shadowColor
        }

        const inputBox = {
            height: multiline ? vw(45) : vw(100),
            fontFamily: Fonts.semibold,
            fontSize: 14,
            color: Colors.black,
            flex: 1,
            paddingLeft: isWhiteBg ? 8 : 12,
            textAlignVertical: multiline ? 'top' : 'center'
        }

        return (
            <>
            <Text style={{ fontFamily: Fonts.semibold, fontSize: 14, marginBottom: 10 }}>{label}</Text>
            <View style={inputInnerStyle}>
                <TextInput
                    onChangeText={(text) => onChangeText(text)}
                    secureTextEntry={isSecure}
                    style={{ ...inputBox }}
                    placeholder={placeholder}
                    keyboardType={keyboardType ? keyboardType : 'default'}
                    value={value}
                    multiline={multiline}
                    editable={editable}
                    maxLength={maxLength}
                    autoCapitalize={autoCapitalize}
                    placeholderTextColor={'rgba(0, 0, 0, 0.5)'}
                />
            </View>
            </>
        )
    }
}

// make this component available to the app
export default CardInput