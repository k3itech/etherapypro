//import liraries
import React, { Component } from "react"
import {
  View,
  TouchableOpacity
} from "react-native"
import Icon from "react-native-vector-icons/Ionicons"
import styles from "./style"
import Colors from "../../common/Colors"

// create a component
class Checkbox extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <View style={styles.checkBoxHolder}>
          <View style={styles.checkboxStyle}>
            <View style={styles.checkedView}>
              <Icon
                name={this.props.checked == true ? 'md-checkbox' : 'square-outline'}
                size={30}
                color={Colors.appGreen}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

// make this component available to the app
export default Checkbox