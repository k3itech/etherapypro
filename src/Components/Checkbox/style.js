import { StyleSheet } from "react-native"
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'
import { 
  vw, 
  vh 
} from '../../common/ViewportUnits'

const styles = StyleSheet.create({
  uncheckedView: {
    borderWidth: vw(2),
    width: vw(16),
    height: vw(16),
    position: "absolute",
    top: vh(2),
    left: 0,
    borderRadius: vw(3)
  },
  checkboxStyle: {
    width: vw(30),
    height: vw(30)
  },
  checkBoxHolder: {
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center'
  },
  checkBoxLabel: {
    fontFamily: Fonts.regular,
    fontSize: vw(15),
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    color: Colors.yellow
  }
})

// make this component available to the app
export default styles