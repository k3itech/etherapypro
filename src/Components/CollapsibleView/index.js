import React, { Component } from 'react'
import {
    View,
    Text,
    KeyboardAvoidingView
} from 'react-native'
import Accordion from 'react-native-collapsible/Accordion'
import Icon from 'react-native-vector-icons/Ionicons'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors';

const {
    headerOuter,
    arrowStyle,
    headerText,
} = styles

// create a component
class CollapsibleView extends Component {

    state = {
        activeSections: []
    }

    _renderHeader = (content, index, isActive, section) => {

        return (
            <View style={styles.header}>
                <View style={headerOuter}>
                    <View>
                        <Text style={headerText}>{content.title}</Text>
                        <View style={arrowStyle}>
                            <Icon
                                name={isActive ? 'chevron-up' : 'chevron-down-outline'}
                                size={15}
                            />
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    _renderContent = (content, index, isActive, sections) => {
        return (
            <View style={styles.content}>
                <Text style={{ fontFamily: Fonts.regular, paddingTop: 8, paddingBottom: 16, color: Colors.whiteFour }}>{content.content}</Text>
            </View>
        )
    }

    _updateSections = activeSections => {
        this.setState({ activeSections })
    }

    // render ui
    render() {

        let SECTIONS = [
            {
                title: 'What to expect:',
                content: 'dnajksdhjkasdsa sajkhdj sahdksaj dsjkdsa dsahkdj dsahdkj dandkja sajkhdasjk dajkda dajkdas',
                id: 0
            },
            {
                title: 'How this works:',
                content: 'dsa,dnm,asdsanm dnsjkahdjska dsahjdhauiwiquei ejrhwje jdakjsiae rel;tk dsbjhdgahr eqwjkrhwej dsajghdewqye',
                id: 1
            },
            {
                title: 'Here’s the cost:',
                content: 'djsajwk jWQOWIQUOUE NMNDFSFHJGEYUnsakjdkw ejwkrwieur srej ewyuewqeuwjr fskjfks',
                id: 2
            },
            {
                title: 'You won’t be billed for 3 days.',
                content: 'dsjkljdkser ehjkwhrew wegetyknfmsd dnsjkhjs bhjewgyuwyrui dsjkdjk dsjkrhjweejrhew rhejhrwj ewhiuyuwi ehiuwhjerw rehiurhewu ehwuey7wtrygejtr dshjhf',
                id: 3
            }
        ]

        return (
            <KeyboardAvoidingView style={styles.container} behavior={null} enabled>
                <Accordion
                    sections={SECTIONS}
                    activeSections={this.state.activeSections}
                    renderSectionTitle={this._renderSectionTitle}
                    renderHeader={this._renderHeader}
                    renderContent={this._renderContent}
                    onChange={this._updateSections}
                />
            </KeyboardAvoidingView>
        );
    }
}

// make this component available to the app
export default CollapsibleView;