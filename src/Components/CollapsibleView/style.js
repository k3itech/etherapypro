import { StyleSheet } from 'react-native'
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'

const styles = StyleSheet.create({
    headerOuter: {
        backgroundColor: Colors.whiteFour,
        paddingHorizontal: 12,
        paddingVertical: 8,
        position: 'relative',
        height: 44,
        marginBottom: 6,
        justifyContent: 'center'
    },
    headerText: {
        color: Colors.black,
        fontFamily: Fonts.bold,
        fontSize: 14
    },
    arrowStyle: {
        position: 'absolute',
        right: 0,
        bottom: 0
    }
})

// make this style component available to the app
export default styles