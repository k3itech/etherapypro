//import liraries
import React, { Component } from 'react'
import { View } from 'react-native'

// create a component
class Column extends Component {
    render() {
        const { 
            children, 
            width, 
            padding 
        } = this.props
        const main = {
            width: width,
            paddingHorizontal: padding,
            paddingVertical: padding
        }
        return (
            <View style={main}>
                {children}
            </View>
        )
    }
}

// make this component available to the app
export default Column