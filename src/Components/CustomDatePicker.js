import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text
} from 'react-native'
import DatePicker from 'react-native-datepicker'
import Colors from '../common/Colors'
import Fonts from '../common/Fonts'

// create a component
class CustomDatePicker extends Component {

    constructor(props) {
        super(props)

        let date = new Date()
        date = date.getFullYear() - 18

        this.state = {
            minDate: date
        }
    }
    // render ui
    render() {
        const {
            date,
            onDateChange,
            placeholder,
            dateFormat
        } = this.props

        return (
            <View style={{ borderWidth: 1 }}>
                <DatePicker
                    style={{ width: '100%' }}
                    mode="date"
                    date={date}
                    placeholder={placeholder}
                    format={dateFormat}
                    minDate={this.state.minDate}
                    confirmBtnText="Done"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateInput: {
                            height: 45,
                            fontFamily: Fonts.semibold,
                            fontSize: 14,
                            color: Colors.black,
                            backgroundColor: Colors.whiteFour,
                            borderRadius: 5,
                            textAlignVertical: 'center',
                            paddingLeft: 12,
                            paddingRight: 0,
                            shadowColor: Colors.shadowColor,
                            shadowOffset: {
                                width: 0,
                                height: 6
                            },
                            shadowOpacity: 1,
                            elevation: 10
                        },
                        btnTextConfirm: {
                            color: Colors.appGreen
                        },
                        dateText: {
                            color: Colors.black,
                            fontSize: 14,
                            fontFamily: Fonts.semibold
                        }
                    }}
                    onDateChange={(date) => { onDateChange(date) }}
                    showIcon={false}
                />
            </View>
        );
    }
}

// make this component available to the app
export default CustomDatePicker