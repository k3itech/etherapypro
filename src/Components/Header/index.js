//import liraries
import React, { Component } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    Image,
    SafeAreaView
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import styles from './style'
import Colors from '../../common/Colors'
import { vw } from '../../common/ViewportUnits'
import Fonts from '../../common/Fonts'

// create a component
class Header extends Component {

    constructor() {
        super()
    }

    render() {

        const {
            backgroundColor,
            borderBottomColor,
            onPressLeftIcon,
            backArrow,
            backArrowImg,
            title,
            rightIconName,
            onPressRightIcon,
            onPressRightIcon1,
            imgIconShow,
            rightIconName1,
            rightButtonName,
            rightButtonName1,
            logoOnPres,
            rightIconColor
        } = this.props

        const { headerbtnStyle } = styles

        const main = {
            width: '100%',
            height: 44,
            backgroundColor: backgroundColor == true ? Colors.whiteFour : 'transparent',
            borderBottomWidth: 1,
            borderBottomColor: borderBottomColor == true ? 'rgba(0, 0, 0, 0.3)' : 'transparent'
        }

        const imgIconShowStyle = {
            width: vw(21),
            height: vw(21)
        }

        const button = {
            alignItems: 'center',
            paddingLeft: vw(10),
            height: 44,
            justifyContent: 'center'
        }

        const rightBtnText = {
            fontFamily: Fonts.light,
            fontSize: vw(17),
            lineHeight: vw(22),
            letterSpacing: -0.41,
            color: Colors.appGreen,
            paddingRight: vw(6)
        }

        const headerTitleStyle = {
            alignItems: 'center',
            justifyContent: 'center',
            height: 44,
            paddingHorizontal: vw(100)
        }

        const headerTitleTextStyle = {
            fontFamily: Fonts.light,
            fontSize: vw(17),
            letterSpacing: -0.41,
            color: Colors.black
        }

        return (
            <>
                <SafeAreaView></SafeAreaView>
                <View style={main}>
                    <View style={[headerbtnStyle, { left: vw(5) }]}>
                        <TouchableOpacity style={button} onPress={onPressLeftIcon} disabled={false}>
                            {
                                backArrow == true
                                    ?
                                    (
                                        <Icon
                                            color={Colors.black}
                                            name={backArrowImg}
                                            size={vw(30)}
                                        />
                                    )
                                    :
                                    (null)
                            }
                        </TouchableOpacity>
                    </View>
                    <View style={headerTitleStyle}>
                        <Text style={headerTitleTextStyle} numberOfLines={1}>{title}</Text>
                    </View>
                    <View style={[headerbtnStyle, { right: vw(12) }]}>
                        <TouchableOpacity style={button} onPress={onPressRightIcon}>
                            {
                                rightIconName == ''
                                    ?
                                    rightButtonName == undefined || rightButtonName == null || rightButtonName == ''
                                        ?
                                        (null)
                                        :
                                        (
                                            <Text style={rightBtnText} onPress={logoOnPres}>{rightButtonName}</Text>
                                        )
                                    :
                                    (
                                        imgIconShow === true
                                            ?
                                            (
                                                <Image
                                                    style={imgIconShowStyle}
                                                    source={rightIconName}
                                                    resizeMode='contain'
                                                />
                                            )
                                            :
                                            (
                                                <Icon
                                                    color={rightIconColor == true ? Colors.appGreen : Colors.black}
                                                    name={rightIconName}
                                                    size={30}
                                                />
                                            )
                                    )
                            }
                        </TouchableOpacity>
                        <TouchableOpacity style={button} onPress={onPressRightIcon1}>
                            {
                                rightIconName1 == ''
                                    ?
                                    rightButtonName1 == undefined || rightButtonName1 == null || rightButtonName1 == ''
                                        ?
                                        (null)
                                        :
                                        (
                                            <Text style={rightBtnText} onPress={logoOnPres}>{rightButtonName1}</Text>
                                        )
                                    :
                                    (
                                        imgIconShow === true
                                            ?
                                            (
                                                <Image
                                                    style={imgIconShowStyle}
                                                    source={rightIconName1}
                                                    resizeMode='contain'
                                                />
                                            )
                                            :
                                            (
                                                <Icon
                                                    color={Colors.black}
                                                    name={rightIconName1}
                                                    size={30}
                                                />
                                            )
                                    )
                            }
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        )
    }
}


// make this component available to the app
export default Header