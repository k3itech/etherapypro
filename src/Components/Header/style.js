import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    headerbtnStyle: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        flexDirection: 'row',
        alignItems: 'center',
        zIndex: 9
    }
})

// make this component available to the app
export default styles