//import liraries
import React, { Component } from 'react'
import {
    View,
    TouchableOpacity,
    SafeAreaView,
    Image,
    Text
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import styles from './style'
import Colors from '../../common/Colors'
import { vw } from '../../common/ViewportUnits'
import Fonts from '../../common/Fonts'

// create a component
class HeaderChat extends Component {

    constructor() {
        super()
    }

    render() {
        const {
            onPressLeftIcon,
            onPressRightIcon,
            title,
            isLeftIcon
        } = this.props

        const {
            main,
            leftButtonViewStyle,
            button,
            headerViewStyle,
            rightButtonStyle,
            profileImageSectionMainStyle,
            profileImageSectionStyle,
            profileStyle,
            editStyle,
            headerTextViewStyle
        } = styles

        return (
            <>
                <SafeAreaView></SafeAreaView>
                <View style={main}>
                    <View style={leftButtonViewStyle}>
                        <TouchableOpacity style={{ ...button, backgroundColor: Colors.appGreenLight }} onPress={onPressLeftIcon} disabled={false}>
                            <Icon
                                color={Colors.appGreen}
                                name='ios-arrow-back-sharp'
                                size={vw(30)}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={headerViewStyle}>
                        <View style={profileImageSectionMainStyle}>
                            <View style={profileImageSectionStyle}>
                                <Image
                                    source={require('../../assets/img/calendar.png')}
                                    style={profileStyle}
                                    resizeMode='cover'
                                />
                                <TouchableOpacity style={editStyle} disabled={true}>
                                    <Image
                                        source={require('../../assets/img/supportChat.png')}
                                        style={{
                                            width: vw(10),
                                            height: vw(10)
                                        }}
                                        resizeMode='center'
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={headerTextViewStyle}>
                            <Text style={{ fontFamily: Fonts.bold, fontSize: vw(18), color: 'rgba(0, 0, 0, 1)' }}>Tania Anderson</Text>
                            <Text style={{ fontFamily: Fonts.regular, fontSize: vw(13), color: 'rgba(0, 0, 0, 1)', marginTop: vw(2) }}>Matching agent</Text>
                        </View>
                    </View>
                    <View style={leftButtonViewStyle}>
                        <TouchableOpacity style={rightButtonStyle} onPress={onPressRightIcon} disabled={false}>
                            <Image
                                source={require('../../assets/img/calendar.png')}
                                style={{
                                    width: vw(22),
                                    height: vw(22)
                                }}
                                resizeMode='contain'
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        )
    }
}


// make this component available to the app
export default HeaderChat