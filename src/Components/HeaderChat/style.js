import { StyleSheet } from 'react-native'
import Colors from '../../common/Colors'
import { vw } from '../../common/ViewportUnits'

const styles = StyleSheet.create({
    main: {
        width: '100%',
        height: 80,
        flexDirection: 'row'
    },
    leftButtonViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '20%',
        height: '100%'
    },
    button: {
        alignItems: 'center',
        height: 49,
        width: 49,
        justifyContent: 'center',
        borderRadius: 49 / 2
    },
    headerViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        width: '60%'
    },
    rightButtonStyle:{
        alignItems: 'center',
        height: 49,
        width: 49,
        justifyContent: 'center',
        borderRadius: 49 / 2,
        backgroundColor: Colors.whiteFour,
        shadowColor: Colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: vw(6)
        },
        shadowOpacity: 1,
        elevation: 10,
        shadowRadius: vw(5)
    },
    profileImageSectionMainStyle: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        alignItems: "center",
        justifyContent: "center"
    },
    profileImageSectionStyle: {
        width: vw(56),
        height: vw(56),
        position: "relative"
    },
    profileStyle: {
        width: vw(56),
        height: vw(56),
        borderRadius: vw(56) / 2
    },
    editStyle: {
        width: vw(20),
        height: vw(20),
        borderRadius: vw(10),
        backgroundColor: Colors.appGreen,
        borderWidth: vw(1),
        borderColor: Colors.whiteFour,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: vw(2),
        bottom: vw(-4)
    },
    headerTextViewStyle: {
        width: '70%',
        height: '100%',
        marginLeft: vw(60),
        justifyContent: 'center'
    }
})

// make this component available to the app
export default styles