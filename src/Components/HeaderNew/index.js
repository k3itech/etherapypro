//import liraries
import React, { Component } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    ImageBackground,
    Image
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'

// create a component
class HeaderNew extends Component {

    constructor() {
        super()
    }

    render() {
        const {
            onPressLeftIcon,
            isTitle,
            title,
            isLeftIcon,
            isRightIcon,
            rightIconOrImage,
            onPressRightIcon
        } = this.props

        const main = {
            width: '100%',
            height: 80,
            flexDirection: 'row',
            flex: 1
        }

        const leftButtonViewStyle = {
            alignItems: 'center',
            justifyContent: 'center',
            flex: 0.15,
            height: 80
        }

        const button = {
            alignItems: 'center',
            height: 49,
            width: 49,
            justifyContent: 'center',
            borderRadius: 49 / 2,
            zIndex: 10
        }

        const headerTitleViewStyle = {
            alignItems: 'center',
            justifyContent: 'center',
            height: 80,
            flex: 0.7
        }

        const headerTitleTextStyle = {
            fontFamily: Fonts.bold,
            fontSize: 24,
            color: Colors.black
        }

        return (
            <View style={main}>
                <View style={leftButtonViewStyle}>
                    <TouchableOpacity style={{ ...button, paddingLeft: 0, marginLeft: 16, backgroundColor: isLeftIcon == false ? 'transparent' : Colors.appGreenLight }} onPress={onPressLeftIcon} disabled={false}>
                        {
                            isLeftIcon == true
                                ?
                                <Icon
                                    color={Colors.appGreen}
                                    name='ios-arrow-back-sharp'
                                    size={30}
                                />
                                :
                                null
                        }
                    </TouchableOpacity>
                </View>
                <View style={headerTitleViewStyle}>
                    {
                        isTitle == true
                            ?
                            (
                                <Text style={headerTitleTextStyle} numberOfLines={1}>{title}</Text>
                            )
                            :
                            (
                                <ImageBackground
                                    source={require('../../assets/img/app_logo_green.png')}
                                    resizeMode='contain'
                                    style={{
                                        width: 184,
                                        height: 54
                                    }}
                                />
                            )
                    }
                </View>
                {
                    isRightIcon == true
                        ?
                        <View style={leftButtonViewStyle}>
                            <TouchableOpacity style={{ ...button, marginRight: 16 }} onPress={onPressRightIcon} disabled={false}>
                                {
                                    rightIconOrImage != ''
                                        ?
                                        <Icon
                                            color={Colors.appGreen}
                                            name={rightIconOrImage}
                                            size={30}
                                        />
                                        :
                                        <Image
                                            source={require('../../assets/img/gear.png')}
                                            resizeMode='cover'
                                            style={{
                                                width: 30,
                                                height: 30
                                            }}
                                        />
                                }
                            </TouchableOpacity>
                        </View>
                        :
                        null
                }
            </View>
        )
    }
}


// make this component available to the app
export default HeaderNew