//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native'
import styles from './style'
import { vw } from '../../common/ViewportUnits'
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'
import RegisterButton from '../RegisterButton'

// create a component
class HomeHorizontalListCard extends Component {

    render() {
        const {
            data,
            onPress
        } = this.props;

        const main = {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginRight: vw(16),
            width: vw(240),
            height: vw(195)
        }

        const {
            bgStyle,
            bottomViewStyle,
            btnStyle
        } = styles

        return (
            <View style={main}>
                <TouchableOpacity style={btnStyle} onPress={onPress}>
                    <Image
                        style={bgStyle}
                        source={require('../../assets/img/horizontal_bg.png')}
                        resizeMode={'cover'}
                    />
                    <View style={{ position: 'absolute', top: vw(16), left: vw(16) }}>
                        <RegisterButton
                            bgColor={null}
                            borderColor={null}
                            borderRadius={vw(30) / 2}
                            shadowColor={false}
                            height={vw(30)}
                            width={vw(30)}
                            disabled={true}
                            buttonImageWidth={vw(30)}
                            buttonImageHeight={vw(30)}
                            buttonImage={require('../../assets/img/play-button.png')}
                            borderWidth={0}
                        />
                    </View>
                    <View style={bottomViewStyle}>
                        <View style={{ borderWidth: 0, flex: 1, marginBottom: vw(8) }}>
                            <Text style={{ color: Colors.black, fontFamily: Fonts.bold, fontSize: vw(17) }}>How do I start?</Text>
                        </View>
                        <View style={{ borderWidth: 0, flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ color: Colors.black, fontFamily: Fonts.regular, fontSize: vw(11) }}>Tip 1/6</Text>
                            <Text style={{ color: Colors.black, fontFamily: Fonts.regular, fontSize: vw(11) }}>03:20</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

// make this component available to the app
export default HomeHorizontalListCard