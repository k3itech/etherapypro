import { StyleSheet } from 'react-native'
import { vw } from '../../common/ViewportUnits'
import Colors from '../../common/Colors'

const styles = StyleSheet.create({
    bgStyle: {
        width: '100%',
        height: vw(130)
    },
    btnStyle: {
        flex: 1,
        borderRadius: vw(10), 
        shadowColor: Colors.shadowColor, 
        shadowOffset: { 
            width: 0, 
            height: vw(6)
        }, 
        shadowOpacity: 1, 
        elevation: 10, 
        shadowRadius: vw(5),
        borderWidth: 0,
        width: '100%',
        height: '100%',
        backgroundColor: Colors.whiteFour
    },
    bottomViewStyle: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        borderWidth: 0,
        borderColor: 'red',
        height: vw(65),
        padding: vw(10)
    }
})

// make this component available to the app
export default styles