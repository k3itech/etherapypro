import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Image,
    TextInput
} from 'react-native'
import Colors from '../common/Colors'
import Fonts from '../common/Fonts'
import { vw } from '../common/ViewportUnits'

// create a component
class Input extends Component {

    // render ui
    render() {

        const {
            inputBox,
            imageIconStyle
        } = styles

        const {
            placeholder,
            label,
            required,
            onChangeText,
            isSecure,
            keyboardType,
            value,
            editable,
            maxLength,
            autoCapitalize,
            leftIconImageSource,
            fromRegister
        } = this.props

        const inputInnerStyle = {
            flexDirection: 'row',
            width: '100%',
            height: vw(45),
            alignItems: 'center',
            borderRadius: 5,
            shadowColor: Colors.shadowColor,
            shadowOffset: {
                width: 0,
                height: vw(6)
            },
            shadowOpacity: 1,
            elevation: 10,
            backgroundColor: fromRegister ? 'transparent' : Colors.whiteFour,
            borderWidth: 2,
            borderColor: Colors.whiteFour
        }

        return (
            <View style={inputInnerStyle}>
                <View style={{ flex: 0.02, justifyContent: 'center', alignItems: 'flex-end', marginRight: 8, borderWidth: 0, height: '100%' }} />
                <Image
                    source={leftIconImageSource}
                    resizeMode='center'
                    style={imageIconStyle}
                />
                <TextInput
                    onChangeText={(text) => onChangeText(text)}
                    secureTextEntry={isSecure}
                    style={{ ...inputBox, color: fromRegister ?  'rgba(255, 255, 255, 1)' : Colors.black }}
                    placeholder={placeholder}
                    keyboardType={keyboardType ? keyboardType : 'default'}
                    value={value}
                    multiline={false}
                    editable={editable}
                    maxLength={maxLength}
                    autoCapitalize={autoCapitalize}
                    placeholderTextColor={fromRegister ? 'rgba(255, 255, 255, 0.5)' : Colors.gray}
                />
            </View>
        )
    }
}

// create styles
const styles = StyleSheet.create({
    inputBox: {
        height: 45,
        fontFamily: Fonts.semibold,
        fontSize: 14,
        color: Colors.black,
        flex: 1,
        paddingLeft: 4
    },
    imageIconStyle: {
        width: 30,
        height: 30
    }
})

// make this component available to the app
export default Input