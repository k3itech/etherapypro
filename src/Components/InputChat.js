import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    Platform, 
    Dimensions,
    Image
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Colors from '../common/Colors'
import Fonts from '../common/Fonts'
import { vw } from '../common/ViewportUnits'

// create a component
class InputChat extends Component {

    // render ui
    render() {

        const { cameraIconStyle } = styles

        const {
            placeholder,
            value,
            cameraOnPress,
            onChangeText
        } = this.props

        const inputInnerStyle = {
            marginBottom: 0
        }

        const inputBox = {
            fontFamily: Fonts.regular,
            fontSize: vw(16),
            paddingBottom: 12,
            color: Colors.black,
            paddingLeft: 12,//45,
            borderRadius: 22,
            borderStyle: 'solid',
            borderWidth: 0,
            borderColor: '#dddddd',
            paddingTop: 12,
            // phase 1 - fixed
            //height: 46,
            // fhase 2 - dynamic height till 5 lines
            minHeight: 46,
            maxHeight: 150
        }

        return (
            <View style={inputInnerStyle}>
                {/* <View style={cameraIconStyle}>
                    <TouchableOpacity onPress={cameraOnPress} style={{ borderWidth: 0, width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                        <Image
                            source={require('../assets/img/smile.png')}
                            style={{
                                width: vw(22),
                                height: vw(22)
                            }}
                            resizeMode='contain'
                        />
                    </TouchableOpacity>
                </View> */}
                <TextInput
                    style={inputBox}
                    placeholder={placeholder}
                    value={value}
                    onChangeText={(text) => onChangeText(text)}
                    multiline={true}
                />
            </View>
        )
    }
}

// create styles
const styles = StyleSheet.create({
    cameraIconStyle: {
        width: 40,
        height: 40,
        borderRadius: 20,
        position: 'absolute',
        zIndex: 1,
        top: 6,
        left: 0,
        justifyContent: 'center',
        alignContent: 'center',
        borderWidth: 0
    }
})

// make this component available to the app
export default InputChat