import React, { Component } from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign'
import RNPickerSelect from 'react-native-picker-select'
import Colors from '../common/Colors'
import Fonts from '../common/Fonts'

// create a component
class CustomSelect extends Component {

    // render ui
    render() {
        const pickerSelectStyles = StyleSheet.create({
            inputIOS: {
                height: 45,
                fontFamily: Fonts.semibold,
                fontSize: 14,
                color: Colors.black,
                backgroundColor: Colors.whiteFour,
                borderRadius: 5,
                textAlignVertical: 'center',
                paddingLeft: 12,
                paddingRight: 0,
                shadowColor: Colors.shadowColor,
                shadowOffset: {
                    width: 0,
                    height: 6
                },
                shadowOpacity: 1,
                elevation: 10
            },
            inputAndroid: {
                height: 45,
                fontFamily: Fonts.semibold,
                fontSize: 14,
                color: Colors.black,
                backgroundColor: Colors.whiteFour,
                borderRadius: 5,
                textAlignVertical: 'center',
                paddingLeft: 12,
                paddingRight: 0,
                shadowColor: Colors.shadowColor,
                shadowOffset: {
                    width: 0,
                    height: 6
                },
                shadowOpacity: 1,
                elevation: 10
            }
        })

        const {
            placeholder,
            onValueChange,
            value,
            items,
            disabled
        } = this.props;

        return (
            <View style={{ borderWidth: 0 }}>
                <RNPickerSelect
                    disabled={disabled}
                    placeholder={placeholder}
                    items={items}
                    onValueChange={(value) => onValueChange(value)}
                    style={{
                        ...pickerSelectStyles,
                        iconContainer: {
                            borderWidth: 0,
                            position: 'absolute',
                            right: 16,
                            top: 16
                        }
                    }}
                    value={value}
                    useNativeAndroidPickerStyle={false}
                    textInputProps={{ underlineColor: 'yellow' }}
                    Icon={() => {
                        return (
                            <AntDesign
                                name={'caretdown'}
                                size={12}
                            />
                        )
                    }}
                />
            </View>
        )
    }
}

// make this component available to the app
export default CustomSelect;