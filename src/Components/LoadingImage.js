import React from 'react'
import { View } from 'react-native'
import Image from 'react-native-image-progress'
import ProgressCircle from 'react-native-progress/Circle'
import Colors from '../common/Colors'

// create a component
const LoadingImage = ({
    resizeMode,
    source,
    style
}) => {

    return (
        <View>
            <Image
                resizeMode={resizeMode}
                source={source}
                indicator={ProgressCircle}
                indicatorProps={{
                    size: 30,
                    borderWidth: 0,
                    color: Colors.whiteFour,
                    unfilledColor: Colors.loading_image_unfilled_color
                }}
                style={style}
            />
        </View>
    )
}

// make this component available to the app
export default LoadingImage