import React from "react"
import { StyleSheet } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay'
import Colors from '../common/Colors'

// create a component
const OverlayLoading = ({ visible }) => {
    return (
        <Spinner
            visible={visible}
            textContent={''}
            textStyle={styles.spinnerTextStyle}
            color={Colors.appDarkGreen}
            overlayColor={'#ffffff00'}
            cancelable={false}
        />
    )
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: Colors.appDarkGreen,
        marginTop: -50
    }
})

// make this component available to the app
export default OverlayLoading