//import liraries
import React from 'react'
import { View } from 'react-native'

// create a component
const PageContainer = ({
    children,
    padding,
    bgColor
}) => {
    const main = {
        zIndex: 0,
        backgroundColor: bgColor,
        flex: 1,
        padding: padding
    }
    return (
        <View style={main}>
            {children}
        </View>
    )
}

export default PageContainer