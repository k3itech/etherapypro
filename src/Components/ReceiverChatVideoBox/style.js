import { StyleSheet } from "react-native"
import Colors from "../../common/Colors"
import Fonts from "../../common/Fonts"
import { vw } from "../../common/ViewportUnits"

const styles = StyleSheet.create({
    mychatDirection: {
        flexDirection: 'row-reverse',
        marginBottom: vw(20)
    },
    mychatStyle: {
        flexDirection: 'column',
        width: '100%',
        justifyContent: 'flex-end'
    },
    myChatOuter: {
        borderRadius: vw(20),
        backgroundColor: '#0E9748',
        paddingHorizontal: 14,
        paddingTop: 9,
        paddingBottom: 6,
        flexDirection: 'column',
        borderBottomLeftRadius: 0
    },
    myChatText: {
        fontSize: vw(16),
        fontFamily: Fonts.regular,
        color: Colors.whiteFour
    },
    myTimeStyle: {
        fontFamily: Fonts.regular,
        fontSize: vw(14),
        color: Colors.whiteFour,
        textAlign: 'left',
        paddingTop: 8,
        paddingRight: 5
    },
    messageDocStyle: {
        position: 'relative',
        overflow: 'hidden',
        borderRadius: 16
    }
})

// make this style component available to the app
export default styles