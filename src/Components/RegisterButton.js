//import liraries
import React, { Component } from 'react'
import {
    TouchableOpacity,
    ImageBackground
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { vw } from '../common/ViewportUnits'
import Colors from '../common/Colors'

// create a component
class Button extends Component {

    render() {

        const {
            bgColor,
            width,
            height,
            borderColor,
            borderRadius,
            onPress,
            disabled,
            shadowColor,
            buttonImage,
            buttonImageWidth,
            buttonImageHeight,
            borderWidth
        } = this.props

        var Button = {
            height: height,
            width: width,
            borderRadius: borderRadius,
            borderWidth: borderWidth,
            justifyContent: 'center',
            backgroundColor: bgColor,
            alignItems: 'center',
            borderColor: borderColor,
            shadowColor: shadowColor == false ? 'transparent' : Colors.shadowColor,
            shadowOffset: {
                width: 0,
                height: shadowColor == false ? 0 : vw(6)
            },
            shadowOpacity: 1,
            elevation: shadowColor == false ? 0 : 10
        }

        return (
            <TouchableOpacity style={Button} onPress={onPress} disabled={disabled}>
                {
                    shadowColor == false
                        ?
                        (
                            <ImageBackground
                                source={buttonImage}
                                resizeMode='center'
                                style={{
                                    width: buttonImageWidth,
                                    height: buttonImageHeight
                                }}
                            />
                        )
                        :
                        (
                            <Icon
                                color={Colors.whiteFour}
                                name={'arrow-forward'}
                                size={38}
                            />
                        )
                }
            </TouchableOpacity>
        )
    }
}

// make this component available to the app
export default Button