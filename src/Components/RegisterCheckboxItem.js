//import liraries
import React, { Component } from 'react'
import {
    View,
    TouchableOpacity,
    Text
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { vw } from '../common/ViewportUnits'
import Colors from '../common/Colors'
import Fonts from '../common/Fonts'

// create a component
class RegisterCheckboxItem extends Component {

    constructor() {
        super()
    }

    render() {
        const {
            onPress,
            isSelected,
            name,
            width
        } = this.props
        return (
            <TouchableOpacity style={{ borderWidth: 0, width: width, height: 40, justifyContent: 'flex-start', alignItems: 'center', flexDirection: 'row' }} onPress={onPress}>
                <View style={{ borderWidth: 0, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center' }}>
                        <Icon
                            name={isSelected ? 'checkbox' : 'square'}
                            size={vw(15)}
                            color={Colors.whiteFour}
                        />
                    </View>
                </View>
                <Text style={{ fontSize: 14, fontFamily: Fonts.regular, color: Colors.whiteFour, borderWidth: 0, flex: 1 }} numberOfLines={2}>{name}</Text>
            </TouchableOpacity>
        )
    }
}

// make this component available to the app
export default RegisterCheckboxItem