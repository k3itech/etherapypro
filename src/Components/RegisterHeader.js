//import liraries
import React, { Component } from 'react'
import {
    View,
    ImageBackground
} from 'react-native'
import {
    vw,
    vh
} from '../common/ViewportUnits'

// create a component
class RegisterHeader extends Component {

    constructor() {
        super()
    }

    render() {
        const main = {
            width: '100%',
            height: vh(80),
            justifyContent: 'center',
            alignItems: 'center'
        }
        return (
            <View style={main}>
                <ImageBackground
                    source={require('../assets/img/app_logo_green.png')}
                    resizeMode='contain'
                    style={{ 
                        width: vw(184), 
                        height: vh(54) 
                    }}
                />
            </View>
        )
    }
}

// make this component available to the app
export default RegisterHeader