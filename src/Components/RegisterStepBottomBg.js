import React from 'react'
import {
    StyleSheet,
    ImageBackground,
    View
} from 'react-native'
import { vw } from '../common/ViewportUnits'

// create a component
const RegisterStepBottomBg = () => {
    return (
        <ImageBackground
            style={styles.bgStyle}
            source={require('../assets/img/Path.png')}
            resizeMode='stretch'
        />
    )
}

const styles = StyleSheet.create({
    bgStyle: {
        width: '100%',
        height: vw(120)
    }
})

// make this component available to the app
export default RegisterStepBottomBg