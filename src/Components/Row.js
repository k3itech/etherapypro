//import liraries
import React, { Component } from 'react'
import { View } from 'react-native'

// create a component
class Row extends Component {
    render() {
        const {
            children,
            mHorizontal
        } = this.props
        const main = {
            flexDirection: 'row',
            marginHorizontal: mHorizontal,
            flexWrap: 'wrap'
        }
        return (
            <View style={main}>
                {children}
            </View>
        )
    }
}

// make this component available to the app
export default Row