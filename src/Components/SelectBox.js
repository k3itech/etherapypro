//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import {
    vw,
    vh
} from '../common/ViewportUnits'
import Colors from '../common/Colors'
import Fonts from '../common/Fonts'

// create a component
class SelectBox extends Component {

    constructor() {
        super()
    }

    render() {
        const {
            children,
            width,
            SelectBoxText,
            onPress,
            disabled
        } = this.props

        const main = {
            width: width,
            height: vh(90),
            backgroundColor: Colors.whiteFour,
            borderBottomWidth: 1,
            borderColor: Colors.gray,
            justifyContent: 'flex-end',
            paddingBottom: vh(7)
        }

        const SelectBoxTextStyle = {
            fontFamily: Fonts.medium,
            fontSize: vw(19),
            letterSpacing: 0,
            color: Colors.blue
        }

        const rowStyle = {
            flexDirection: 'row',
            justifyContent: 'space-between'
        }

        return (
            <View style={main}>
                <TouchableOpacity style={rowStyle} onPress={onPress} disabled={disabled}>
                    <Text style={SelectBoxTextStyle}>{SelectBoxText}</Text>
                    <MaterialIcons
                        name='keyboard-arrow-down'
                        size={22}
                    />
                </TouchableOpacity>
                {children}
            </View>
        )
    }
}

// make this component available to the app
export default SelectBox