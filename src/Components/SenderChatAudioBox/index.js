
import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native'
import moment from 'moment'
import Fonts from '../../common/Fonts'
import { vw } from '../../common/ViewportUnits'
import styles from './style'

// create a component
class SenderChatAudioBox extends Component {

    // render ui
    render() {

        const {
            mychatDirection,
            mychatStyle,
            myChatOuter,
            messageDocStyle,
            myTimeStyle
        } = styles

        const { 
            date,
            onPress,
            user_name
        } = this.props
        let date1 = moment(date).format('MMM DD')
        let time1 = moment(date).format('hh:mm A')

        return (
            <TouchableOpacity style={mychatDirection} onPress={onPress}>
                <View style={mychatStyle}>
                    <View style={{ width: '100%', height: 35, flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ color: 'rgba(0, 0, 0, 0.6)', fontSize: vw(14), fontFamily: Fonts.regular, width: '50%' }}>{user_name}</Text>
                        <Text style={{ color: 'rgba(0, 0, 0, 1)', fontSize: vw(16), fontFamily: Fonts.semibold, width: '50%', textAlign: 'right' }}>{date1}</Text>
                    </View>
                    <View style={myChatOuter}>
                        <Image
                            style={messageDocStyle}
                            resizeMode='center'
                            source={require('../../assets/img/mic.png')}
                        />
                        <Text style={myTimeStyle}>{time1}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

// make this component available to the app
export default SenderChatAudioBox