import { StyleSheet } from "react-native"
import Colors from "../../common/Colors"
import Fonts from "../../common/Fonts"
import { vw } from "../../common/ViewportUnits"

const styles = StyleSheet.create({
    mychatDirection: {
        flexDirection: 'row-reverse',
        marginBottom: vw(20)
    },
    mychatStyle: {
        flexDirection: 'column',
        width: '100%',
        justifyContent: 'flex-end'
    },
    myChatOuter: {
        borderRadius: vw(20),
        backgroundColor: '#007EF4',
        paddingHorizontal: 14,
        paddingTop: 9,
        paddingBottom: 6,
        flexDirection: 'column',
        borderBottomRightRadius: 0
    },
    myTimeStyle: {
        fontFamily: Fonts.regular,
        fontSize: vw(14),
        color: Colors.whiteFour,
        textAlign: 'right',
        paddingTop: 8,
        paddingRight: 5
    },
    messageDocStyle: {
        width: 40,
        height: 40,
        alignSelf: 'center',
        marginTop: 10
    }
})

// make this style component available to the app
export default styles