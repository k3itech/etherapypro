
import React, { Component } from 'react'
import {
    View,
    Text
} from 'react-native'
import moment from 'moment'
import Fonts from '../../common/Fonts'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import LoadingImage from '../LoadingImage'
import Functions from '../../Utility/Functions'

// create a component
class SenderChatImageBox extends Component {

    // render ui
    render() {

        const {
            mychatDirection,
            mychatStyle,
            myChatOuter,
            messageDocStyle,
            myTimeStyle
        } = styles

        const {
            message,
            date,
            image_data,
            user_name
        } = this.props

        let data = {
            height: image_data.height,
            width: image_data.width
        }
        let screenheight = Functions.getImageResizeDataShow(data)

        let date1 = moment(date).format('MMM DD')
        let time1 = moment(date).format('hh:mm A')

        return (
            <View style={mychatDirection}>
                <View style={mychatStyle}>
                    <View style={{ width: '100%', height: 35, flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ color: 'rgba(0, 0, 0, 0.6)', fontSize: vw(14), fontFamily: Fonts.regular, width: '50%' }}>{user_name}</Text>
                        <Text style={{ color: 'rgba(0, 0, 0, 1)', fontSize: vw(16), fontFamily: Fonts.semibold, width: '50%', textAlign: 'right' }}>{date1}</Text>
                    </View>
                    <View style={myChatOuter}>
                        <LoadingImage
                            resizeMode='cover'
                            style={{ 
                                ...messageDocStyle, 
                                width: '100%',//screenheight.width, 
                                height: screenheight.height 
                            }}
                            source={{ uri: `${message}` }}
                        />
                        <Text style={myTimeStyle}>{time1}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

// make this component available to the app
export default SenderChatImageBox