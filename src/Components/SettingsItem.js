//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    Switch,
    TouchableWithoutFeedback,
    Platform,
    Image
} from 'react-native'
import { vw } from '../common/ViewportUnits'
import Colors from '../common/Colors'
import Fonts from '../common/Fonts'

// create a component
class SettingsItem extends Component {

    render() {

        const {
            showSwith,
            itemLabel,
            switchValue,
            switchOnChange,
            onPress,
            disabled,
            isHeader,
            itemImage,
            isDEscription,
            description
        } = this.props

        const main = {
            flexDirection: 'row',
            alignItems: 'center',
            minHeight: isHeader ? vw(53) : vw(60),
            borderBottomWidth: 1,
            borderColor: 'rgba(0, 0, 0, .4)',
            paddingHorizontal: 20
        }

        const itemLabelStyle = {
            fontFamily: Fonts.semibold,
            fontSize: vw(16),
            color: isHeader ? 'rgba(0, 0, 0, .6)' : Colors.black,
            textTransform: isHeader ? 'uppercase' : 'none'
        }

        const leftButtonViewStyle = {
            alignItems: 'center',
            borderWidth: 0,
            justifyContent: 'center',
            marginRight: 16,
        }
        
        const rightButtonViewStyle = {
            alignItems: 'center',
            borderWidth: 0,
            justifyContent: 'flex-end',
            right: 16,
            position: 'absolute'
        }

        return (
            <TouchableWithoutFeedback onPress={onPress} disabled={showSwith ? true : false}>
                <View style={main}>
                    {
                        isHeader
                            ?
                            null
                            :
                            (
                                <View style={leftButtonViewStyle}>
                                    <Image
                                        source={itemImage}
                                        resizeMode={'center'}
                                        style={{
                                            width: vw(30),
                                            height: vw(30)
                                        }}
                                    />
                                </View>
                            )
                    }
                    <View style={{ borderWidth: 0, flex: .8 }}>
                        <Text style={{ ...itemLabelStyle, marginTop: isDEscription ? 16 : 0 }}>{itemLabel}</Text>
                        {
                            isDEscription
                                ?
                                <Text style={{ ...itemLabelStyle, fontFamily: Fonts.regular, marginTop: 6, marginBottom: 16 }}>{description}</Text>
                                :
                                null
                        }
                    </View>
                    {
                        showSwith == false
                            ?
                            null
                            :
                            (
                                <View style={rightButtonViewStyle}>
                                    <Switch
                                        value={switchValue}
                                        onValueChange={(value) => {
                                            switchOnChange(value)
                                        }}
                                        thumbColor={switchValue == true ? Colors.appGreen : Platform.OS == 'ios' ? 'rgba(236, 236, 236, 1)' : Colors.gray}
                                        trackColor={{
                                            true: 'rgba(14, 151, 72, .39)',
                                            false: 'rgba(178, 178, 178, 1)'
                                        }}
                                    />
                                </View>
                            )
                    }
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

// make this component available to the app
export default SettingsItem