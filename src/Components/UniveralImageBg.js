import React from 'react'
import {
    StyleSheet,
    ImageBackground,
    View,
    SafeAreaView,
    Text,
    Image
} from 'react-native'
import {
    vh,
    vw
} from '../common/ViewportUnits'
import Fonts from '../common/Fonts'
import Colors from '../common/Colors'

// create a component
const UniveralImageBg = ({
    isLogoShowing,
    isTitleShowing,
    height,
    bgImage
}) => {
    return (
        <>
            <SafeAreaView></SafeAreaView>
            <View style={{ ...styles.main, height: height }}>
                <ImageBackground
                    style={styles.bgStyle}
                    source={bgImage}
                    resizeMode='cover'
                >
                    <View style={styles.iconViewStyle}>
                        {
                            isLogoShowing == true
                                ?
                                <Image
                                    style={styles.iconStyle}
                                    source={require('../assets/img/app_logo.png')}
                                    resizeMode='contain'
                                />
                                :
                                null
                        }
                        {
                            isTitleShowing == true
                                ?
                                <Text style={styles.titleStyle}>{'Confidential Online Counseling'}</Text>
                                :
                                null
                        }
                    </View>
                </ImageBackground>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    main: {
        width: '100%'
    },
    bgStyle: {
        width: '100%',
        height: '100%'
    },
    imgStyle: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        overflow: 'hidden'
    },
    iconViewStyle: {
        width: '100%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        top: vh(8)
    },
    iconStyle: {
        width: vw(205),
        height: vh(67),
        overflow: 'hidden'
    },
    titleStyle: {
        width: '90%',
        textAlign: 'center',
        fontFamily: Fonts.bold,
        fontSize: vw(22),
        color: Colors.whiteFour,
        top: vh(12)
    }
})

// make this component available to the app
export default UniveralImageBg