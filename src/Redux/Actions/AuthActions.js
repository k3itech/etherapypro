import {
    SYNC_ALREADY_LOGIN_USER_DATA,
    USER_LOGOUT_SUCCESS_RESET,
    USER_LOGOUT_REQUESTED,
    USER_LOGOUT_SUCCESS,
    USER_LOGOUT_FAILED,
    USER_LOGOUT_ERROR_RESET,
    HANDLE_NETWORK_CONNECION_STATUS_UPDATE,
    REGISTER_STATIC_DATA_REQUESTED,
    REGISTER_STATIC_DATA_SUCCESS,
    REGISTER_STATIC_DATA_FAILED,
    REGISTER_STATIC_DATA_ERROR_RESET,
    SELECT_REGISTER_STEP1_FEELINGS_DATA,
    SELECT_REGISTER_STEP2_CHALLENGES_DATA,
    SELECT_REGISTER_STEP3_AREALIFE_DATA, 
    SELECT_REGISTER_STEP4_CHANGE_ONE_THING_DATA,
    SELECT_REGISTER_STEP5_SUICIDE_ATTEMPT_DATA,
    SELECT_REGISTER_STEP5_COUNSELLING_DATA,
    SELECT_REGISTER_STEP6_AGE_DATA,
    SELECT_REGISTER_STEP6_COUNTRY_DATA,
    SELECT_REGISTER_STEP6_STATE_DATA,
    SELECT_REGISTER_STEP7_RELATIONSHIP_DATA,
    SELECT_REGISTER_STEP8_GENDER_DATA,
    SELECT_REGISTER_STEP9_SEXUAL_ORIENTATION_DATA,
    SELECT_REGISTER_STEP10_RELIGIOUS_DATA,
    SELECT_REGISTER_STEP10_ILLNESS_PAIN_DATA,
    SELECT_REGISTER_STEP11_MEDICATION_DATA,
    SELECT_REGISTER_STEP11_READY_TO_GET_STARTED_DATA,
    REGISTER_REQUESTED,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
    REGISTER_ERROR_RESET,
    LOGIN_REQUESTED,
    LOGIN_SUCCESS, 
    LOGIN_FAILED,
    LOGIN_ERROR_RESET,
    FORGOT_PASSWORD_REQUESTED,
    FORGOT_PASSWORD_SUCCESS,
    FORGOT_PASSWORD_FAILED,
    FORGOT_PASSWORD_ERROR_RESET,
    CHANGE_PASSWORD_REQUESTED,
    CHANGE_PASSWORD_SUCCESS,
    CHANGE_PASSWORD_FAILED,
    CHANGE_PASSWORD_ERROR_RESET,
    MY_PROFILE_REQUESTED,
    MY_PROFILE_SUCCESS,
    MY_PROFILE_FAILED,
    MY_PROFILE_ERROR_RESET
} from '../Types'
import { Alert } from 'react-native'
import Functions from '../../Utility/Functions'
import * as RootNavigation from '../../Routers/RootNavigation'
import {
    APP_TITLE,
    GLOBAL_ERROR_MSG,
    OK_TITLE
} from '../../Utility/Constants'

export const handleNetworkConnectivityStatus = (status) => (dispatch) => {
    dispatch({
        type: HANDLE_NETWORK_CONNECION_STATUS_UPDATE,
        payload: status
    })
}

export const setAlreadyLoginUserData = (userData) => (dispatch) => {
    dispatch({
        type: SYNC_ALREADY_LOGIN_USER_DATA,
        payload: userData
    })
}

// get register static data API call
export const registrationStaticDataAPI = () => (dispatch) => {
    dispatch({
        type: REGISTER_STATIC_DATA_REQUESTED
    })
    requestRegistrationStaticDataFROMAPI(dispatch)
}

const requestRegistrationStaticDataFROMAPI = (dispatch) => {  
    Functions.getApi(`/data`, {}).then((data) => {
        console.log('register questions data = ', data)
        if (data.status == 200) {
            if (data.data.success == true) {
                dispatch(registrationStaticDataAPISuccess(data.data.data))
            }
            else {
                dispatch(registrationStaticDataAPIDailed(data.data.error ? data.data.error == null ? data.data.message : data.data.error : data.data.message))
            }
        }
        else {
            dispatch(registrationStaticDataAPIDailed(data.data.message))
        }
    })
        .catch((error) => {
            console.log('register questions error = ', error)
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log('error.response.data = ', error.response.data)
                console.log('error.response.status = ', error.response.status)
                if (error.response.status == 505) {
                    if (error.response.data.success == false) {
                        dispatch(registrationStaticDataAPIDailed(undefined))
                        setTimeout(() => {
                            dispatch(forceUpdateAction(error.response.data))
                        }, 100)
                    }
                    else {
                        dispatch(logoutFailed(error.response.data.message))
                    }
                }
                else if (error.response.status == 401) {
                    if (error.response.data.success == false) {
                        dispatch(registrationStaticDataAPIDailed(undefined))
                        dispatch(unAuthorizedUserAction(error.response.data.message))
                    }
                    else {
                        dispatch(registrationStaticDataAPIDailed(error.response.data.message))
                    }
                }
                else {
                    dispatch(registrationStaticDataAPIDailed(error.response.data.message ? error.response.data.message : undefined))
                }
            }
            else {
                // Something happened in setting up the request that triggered an Error
                console.log('error.message', error.message)
                dispatch(registrationStaticDataAPIDailed(error.message ? error.message : GLOBAL_ERROR_MSG))
            }
        })
}

const registrationStaticDataAPISuccess = (data) => (dispatch) => {
    dispatch({
        type: REGISTER_STATIC_DATA_SUCCESS,
        payload: data
    })
}

const registrationStaticDataAPIDailed = (errorMessage) => ({
    type: REGISTER_STATIC_DATA_FAILED,
    payload: errorMessage
})

export const registrationStaticDataAPIErrorReset = () => (dispatch) => {
    dispatch({
        type: REGISTER_STATIC_DATA_ERROR_RESET
    })
}

export const selectRegisterStep1FeelingsData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP1_FEELINGS_DATA,
        payload: data
    })
}

export const selectRegisterStep2ChallengesData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP2_CHALLENGES_DATA,
        payload: data
    })
}

export const selectRegisterStep3AreaLifeData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP3_AREALIFE_DATA,
        payload: data
    })
}

export const selectRegisterStep4ChangeOneThingData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP4_CHANGE_ONE_THING_DATA,
        payload: data
    })
}

export const selectRegisterStep5SuicideAttemptData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP5_SUICIDE_ATTEMPT_DATA,
        payload: data
    })
}

export const selectRegisterStep5CounsellingData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP5_COUNSELLING_DATA,
        payload: data
    })
}

export const selectRegisterStep6AgeData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP6_AGE_DATA,
        payload: data
    })
}

export const selectRegisterStep6CountryData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP6_COUNTRY_DATA,
        payload: data
    })
}

export const selectRegisterStep6StateData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP6_STATE_DATA,
        payload: data
    })
}

export const selectRegisterStep7RelationshipData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP7_RELATIONSHIP_DATA,
        payload: data
    })
}

export const selectRegisterStep8GenderData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP8_GENDER_DATA,
        payload: data
    })
}

export const selectRegisterStep9SexualOrientationData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP9_SEXUAL_ORIENTATION_DATA,
        payload: data
    })
}

export const selectRegisterStep10RelligiosData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP10_RELIGIOUS_DATA,
        payload: data
    })
}

export const selectRegisterStep10IllnessPainData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP10_ILLNESS_PAIN_DATA,
        payload: data
    })
}

export const selectRegisterStep11MedicationData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP11_MEDICATION_DATA,
        payload: data
    })
}

export const selectRegisterStep11GetReadyToStartData = (data) => (dispatch) => {
    dispatch({
        type: SELECT_REGISTER_STEP11_READY_TO_GET_STARTED_DATA,
        payload: data
    })
}

// login API call
export const loginAPI = (data) => (dispatch) => {
    dispatch({
        type: LOGIN_REQUESTED
    })
    requestLoginAPI(dispatch, data)
}

const requestLoginAPI = (dispatch, data) => {  
    Functions.postApi(`/user/login`, data, {}).then((data) => {
        console.log('login data = ', data)
        if (data.status == 200) {
            if (data.data.success == true) {
                dispatch(loginAPISuccess(data.data.data))
            }
            else {
                dispatch(loginAPIFailed(data.data.error ? data.data.error == null ? data.data.message : data.data.error : data.data.message))
            }
        }
        else {
            dispatch(loginAPIFailed(data.data.error))
        }
    })
        .catch((error) => {
            console.log('login error = ', error)
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log('error.response.data = ', error.response.data)
                console.log('error.response.status = ', error.response.status)
                if (error.response.status == 505) {
                    if (error.response.data.success == false) {
                        dispatch(loginAPIFailed(undefined))
                        setTimeout(() => {
                            dispatch(forceUpdateAction(error.response.data))
                        }, 100)
                    }
                    else {
                        dispatch(loginAPIFailed(error.response.data.message))
                    }
                }
                else if (error.response.status == 401) {
                    if (error.response.data.success == false) {
                        dispatch(loginAPIFailed(undefined))
                        dispatch(unAuthorizedUserAction(error.response.data.message))
                    }
                    else {
                        dispatch(loginAPIFailed(error.response.data.message))
                    }
                }
                else {
                    dispatch(loginAPIFailed(error.response.data.message ? error.response.data.message : undefined))
                }
            }
            else {
                // Something happened in setting up the request that triggered an Error
                console.log('error.message', error.message)
                dispatch(loginAPIFailed(error.message ? error.message : GLOBAL_API_ERROR_MESSAGE))
            }
        })
}

const loginAPISuccess = (userData) => (dispatch) => {
    Functions.onSignIn(JSON.stringify(userData))
    dispatch({
        type: LOGIN_SUCCESS,
        payload: userData
    })
}

const loginAPIFailed = (errorMessage) => ({
    type: LOGIN_FAILED,
    payload: errorMessage
})

export const loginAPIErrorReset = () => (dispatch) => {
    dispatch({
        type: LOGIN_ERROR_RESET
    })
}

// Logout API call
export const registerAPI = (data) => (dispatch) => {
    dispatch({
        type: REGISTER_REQUESTED
    })
    requestRegisterAPI(dispatch, data)
}

const requestRegisterAPI = (dispatch, data) => {
    //run API request    
    Functions.postApi(`/user/register`, data, {}).then((data) => {
        console.log('user/register data = ', data)
        if (data.status == 200) {
            if (data.data.success == true) {
                dispatch(registerAPISuccess(data.data.data))
            }
            else {
                dispatch(registerAPIFailed(data.data.error ? data.data.error == null ? data.data.message : data.data.error : data.data.message))
            }
        }
        else {
            dispatch(registerAPIFailed(data.data.message))
        }
    })
        .catch((error) => {
            console.log('user/register error = ', error)
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log('error.response.data = ', error.response.data)
                console.log('error.response.status = ', error.response.status)
                if (error.response.status == 505) {
                    if (error.response.data.success == false) {
                        dispatch(registerAPIFailed(undefined))
                        setTimeout(() => {
                            dispatch(forceUpdateAction(error.response.data))
                        }, 100)
                    }
                    else {
                        dispatch(registerAPIFailed(error.response.data.message))
                    }
                }
                else if (error.response.status == 401) {
                    if (error.response.data.success == false) {
                        dispatch(registerAPIFailed(undefined))
                        dispatch(unAuthorizedUserAction(error.response.data.message))
                    }
                    else {
                        dispatch(registerAPIFailed(error.response.data.message))
                    }
                }
                else {
                    dispatch(registerAPIFailed(error.response.data.message ? error.response.data.message : undefined))
                }
            }
            else {
                // Something happened in setting up the request that triggered an Error
                console.log('error.message', error.message)
                dispatch(registerAPIFailed(error.message ? error.message : GLOBAL_API_ERROR_MESSAGE))
            }
        })
}

const registerAPISuccess = (userData) => (dispatch) => {
    Functions.onSignIn(JSON.stringify(userData))
    dispatch({
        type: REGISTER_SUCCESS,
        payload: userData
    })
}

const registerAPIFailed = (errorMessage) => ({
    type: REGISTER_FAILED,
    payload: errorMessage
})

export const registerAPIErrorReset = () => (dispatch) => {
    dispatch({
        type: REGISTER_ERROR_RESET
    })
}

// forgot password API call
export const forgotPasswordAPI = (data) => (dispatch) => {
    dispatch({
        type: FORGOT_PASSWORD_REQUESTED
    })
    requestForgotPasswordAPI(dispatch, data)
}

const requestForgotPasswordAPI = (dispatch, data) => {
    //run API request    
    Functions.postApi(`/user/register`, data, {}).then((data) => {
        console.log('user/register data = ', data)
        if (data.status == 200) {
            if (data.data.success == true) {
                dispatch(forgotPasswordAPISuccess(data.data.data))
            }
            else {
                dispatch(forgotPasswordAPIAPIFailed(data.data.error ? data.data.error == null ? data.data.message : data.data.error : data.data.message))
            }
        }
        else {
            dispatch(forgotPasswordAPIAPIFailed(data.data.message))
        }
    })
        .catch((error) => {
            console.log('user/register error = ', error)
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log('error.response.data = ', error.response.data)
                console.log('error.response.status = ', error.response.status)
                if (error.response.status == 505) {
                    if (error.response.data.success == false) {
                        dispatch(forgotPasswordAPIAPIFailed(undefined))
                        setTimeout(() => {
                            dispatch(forceUpdateAction(error.response.data))
                        }, 100)
                    }
                    else {
                        dispatch(forgotPasswordAPIAPIFailed(error.response.data.message))
                    }
                }
                else if (error.response.status == 401) {
                    if (error.response.data.success == false) {
                        dispatch(forgotPasswordAPIAPIFailed(undefined))
                        dispatch(unAuthorizedUserAction(error.response.data.message))
                    }
                    else {
                        dispatch(forgotPasswordAPIAPIFailed(error.response.data.message))
                    }
                }
                else {
                    dispatch(forgotPasswordAPIAPIFailed(error.response.data.message ? error.response.data.message : undefined))
                }
            }
            else {
                // Something happened in setting up the request that triggered an Error
                console.log('error.message', error.message)
                dispatch(forgotPasswordAPIAPIFailed(error.message ? error.message : GLOBAL_API_ERROR_MESSAGE))
            }
        })
}

const forgotPasswordAPISuccess = (userData) => (dispatch) => {
    dispatch({
        type: FORGOT_PASSWORD_SUCCESS,
        payload: userData
    })
}

const forgotPasswordAPIAPIFailed = (errorMessage) => ({
    type: FORGOT_PASSWORD_FAILED,
    payload: errorMessage
})

export const forgotPasswordAPIErrorReset = () => (dispatch) => {
    dispatch({
        type: FORGOT_PASSWORD_ERROR_RESET
    })
}

// change password API call
export const changePasswordAPI = (data, token) => (dispatch) => {
    dispatch({
        type: CHANGE_PASSWORD_REQUESTED
    })
    requestChangePasswordAPI(dispatch, data, token)
}

const requestChangePasswordAPI = (dispatch, data, token) => {
    //run API request    
    Functions.putApi(`/user/resetPassword/${token}`, data, {}).then((data) => {
        console.log('user/resetPassword/token data = ', data)
        if (data.status == 200) {
            if (data.data.success == true) {
                dispatch(changePasswordAPISuccess(data.data.message))
            }
            else {
                dispatch(changePasswordAPIAPIFailed(data.data.error ? data.data.error == null ? data.data.message : data.data.error : data.data.message))
            }
        }
        else {
            dispatch(changePasswordAPIAPIFailed(data.data.message))
        }
    })
        .catch((error) => {
            console.log('user/resetPassword/token error = ', error)
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log('error.response.data = ', error.response.data)
                console.log('error.response.status = ', error.response.status)
                if (error.response.status == 505) {
                    if (error.response.data.success == false) {
                        dispatch(changePasswordAPIAPIFailed(undefined))
                        setTimeout(() => {
                            dispatch(forceUpdateAction(error.response.data))
                        }, 100)
                    }
                    else {
                        dispatch(changePasswordAPIAPIFailed(error.response.data.message))
                    }
                }
                else if (error.response.status == 401) {
                    if (error.response.data.success == false) {
                        dispatch(changePasswordAPIAPIFailed(undefined))
                        dispatch(unAuthorizedUserAction(error.response.data.message))
                    }
                    else {
                        dispatch(changePasswordAPIAPIFailed(error.response.data.message))
                    }
                }
                else {
                    dispatch(changePasswordAPIAPIFailed(error.response.data.message ? error.response.data.message : undefined))
                }
            }
            else {
                // Something happened in setting up the request that triggered an Error
                console.log('error.message', error.message)
                dispatch(changePasswordAPIAPIFailed(error.message ? error.message : GLOBAL_API_ERROR_MESSAGE))
            }
        })
}

const changePasswordAPISuccess = (message) => (dispatch) => {
    dispatch({
        type: CHANGE_PASSWORD_SUCCESS,
        payload: message
    })
}

const changePasswordAPIAPIFailed = (errorMessage) => ({
    type: CHANGE_PASSWORD_FAILED,
    payload: errorMessage
})

export const changePasswordAPIErrorReset = () => (dispatch) => {
    dispatch({
        type: CHANGE_PASSWORD_ERROR_RESET
    })
}

// my profile API call
export const myProfileAPI = (token) => (dispatch) => {
    dispatch({
        type: MY_PROFILE_REQUESTED
    })
    requestMyProfileAPI(dispatch, token)
}

const requestMyProfileAPI = (dispatch, token) => {
    //run API request    
    Functions.getApi(`/user}`, {}).then((data) => {
        console.log('user/token data = ', data)
        if (data.status == 200) {
            if (data.data.success == true) {
                dispatch(myProfileAPISuccess(data.data.data))
            }
            else {
                dispatch(myProfileAPIFailed(data.data.error ? data.data.error == null ? data.data.message : data.data.error : data.data.message))
            }
        }
        else {
            dispatch(changePassmyProfileAPIFailedwordAPIAPIFailed(data.data.message))
        }
    })
        .catch((error) => {
            console.log('user/token error = ', error)
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log('error.response.data = ', error.response.data)
                console.log('error.response.status = ', error.response.status)
                if (error.response.status == 505) {
                    if (error.response.data.success == false) {
                        dispatch(myProfileAPIFailed(undefined))
                        setTimeout(() => {
                            dispatch(forceUpdateAction(error.response.data))
                        }, 100)
                    }
                    else {
                        dispatch(myProfileAPIFailed(error.response.data.message))
                    }
                }
                else if (error.response.status == 401) {
                    if (error.response.data.success == false) {
                        dispatch(myProfileAPIFailed(undefined))
                        dispatch(unAuthorizedUserAction(error.response.data.message))
                    }
                    else {
                        dispatch(myProfileAPIFailed(error.response.data.message))
                    }
                }
                else {
                    dispatch(myProfileAPIFailed(error.response.data.message ? error.response.data.message : undefined))
                }
            }
            else {
                // Something happened in setting up the request that triggered an Error
                console.log('error.message', error.message)
                dispatch(myProfileAPIFailed(error.message ? error.message : GLOBAL_API_ERROR_MESSAGE))
            }
        })
}

const myProfileAPISuccess = (userData) => (dispatch) => {
    Functions.onSignIn(JSON.stringify(userData))
    dispatch({
        type: MY_PROFILE_SUCCESS,
        payload: userData
    })
}

const myProfileAPIFailed = (errorMessage) => ({
    type: MY_PROFILE_FAILED,
    payload: errorMessage
})

export const myProfileAPIErrorReset = () => (dispatch) => {
    dispatch({
        type: MY_PROFILE_ERROR_RESET
    })
}

// Logout API call
export const logoutAPI = () => (dispatch) => {
    dispatch({
        type: USER_LOGOUT_REQUESTED
    })
    doLogout(dispatch)
}

const doLogout = (dispatch) => {
    // //run API request    
    // Functions.putApi(`${BASE_URL}/logout`, {}, { 'Authorization': global.user_token }).then((data) => {
    //     console.log('logout data = ', data)
    //     if (data.status == 200) {
    //         if (data.data.success == true) {
    //             dispatch(logoutSuccess())
    //         }
    //         else {
    //             dispatch(logoutFailed(data.data.message))
    //         }
    //     }
    //     else {
    //         dispatch(logoutFailed(data.data.message))
    //     }
    // })
    //     .catch((error) => {
    //         console.log('logout error = ', error)

    //         if (error.response) {
    //             // The request was made and the server responded with a status code
    //             // that falls out of the range of 2xx
    //             console.log('error.response.data = ', error.response.data)
    //             console.log('error.response.status = ', error.response.status)

    //             if (error.response.status == 505) {
    //                 if (error.response.data.success == false) {
    //                     dispatch(logoutFailed(undefined))
    //                     setTimeout(() => {
    //                         dispatch(forceUpdateAction(error.response.data))
    //                     }, 100)
    //                 }
    //                 else {
    //                     dispatch(logoutFailed(error.response.data.message))
    //                 }
    //             }
    //             else if (error.response.status == 401) {
    //                 if (error.response.data.success == false) {
    //                     dispatch(logoutFailed(undefined))
    //                     dispatch(unAuthorizedUserAction(error.response.data.message))
    //                 }
    //                 else {
    //                     dispatch(logoutFailed(error.response.data.message))
    //                 }
    //             }
    //             else {
    //                 dispatch(logoutFailed(error.response.data.message ? error.response.data.message : undefined))
    //             }
    //         }
    //         else {
    //             // Something happened in setting up the request that triggered an Error
    //             console.log('error.message', error.message)
    //             dispatch(logoutFailed(error.message ? error.message : GLOBAL_API_ERROR_MESSAGE))
    //         }
    //     })
}

const logoutSuccess = () => (dispatch) => {
    dispatch({
        type: USER_LOGOUT_SUCCESS
    })
}

const logoutFailed = (errorMessage) => ({
    type: USER_LOGOUT_FAILED,
    payload: errorMessage
})

export const resetLogoutError = () => (dispatch) => {
    dispatch({
        type: USER_LOGOUT_ERROR_RESET
    })
}

// logout success
export const LogoutSuccess = () => (dispatch) => {
    Functions.onSignOut()
    dispatch({
        type: USER_LOGOUT_SUCCESS_RESET
    })
}

export const unAuthorizedUserAction = (message) => (dispatch) => {
    setTimeout(() => {
        console.log('unAuthorizedUserAction message = ', message)
        Alert.alert(
            APP_TITLE,
            message,
            [
                {
                    text: OK_TITLE,
                    onPress: () => {
                        RootNavigation.navigate('Auth')
                        dispatch(LogoutSuccess())
                    }
                }
            ],
            { cancelable: false }
        )
    }, 1000)
}

export const forceUpdateAction = (data) => (dispatch) => {
    console.log('forceUpdateAction data = ', data)
    //RootNavigation.navigate('App', { screen: 'ForceUpdateFlow' })
    // if (data.type == 'error') {
    //   CustomAlert.alert(data.message)
    // }
    // else if (data.type == 'force-update') {
    //   Alert.alert(
    //     APP_TITLE,
    //     data.message,
    //     [
    //       {
    //         text: 'Update Now',
    //         onPress: () => {
    //           Linking.canOpenURL(data.link).then(supported => {
    //             if (supported) {
    //               Linking.openURL(data.link)
    //             }
    //             else {
    //               console.log("Don't know how to open URL: " + data.link)
    //             }
    //           })
    //         }
    //       }
    //     ],
    //     { cancelable: false }
    //   )
    // }
    // else {
    //   Alert.alert(
    //     APP_TITLE,
    //     data.message,
    //     [
    //       {
    //         text: 'Update Now',
    //         onPress: () => {
    //           Linking.canOpenURL(data.link).then(supported => {
    //             if (supported) {
    //               Linking.openURL(data.link)
    //             }
    //             else {
    //               console.log("Don't know how to open URL: " + data.link)
    //             }
    //           })
    //         }
    //       },
    //       {
    //         text: 'Cancel',
    //         onPress: () => {
    //           console.log('Cancel Pressed')
    //           global.app_version = data.version
    //         },
    //         style: 'cancel'
    //       }
    //     ],
    //     { cancelable: false }
    //   )
    // }
}