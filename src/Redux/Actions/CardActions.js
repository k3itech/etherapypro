import {
    ADD_CARD_REQUESTED,
    ADD_CARD_SUCCESS,
    ADD_CARD_FAILED,
    ADD_CARD_ERROR_RESET
} from '../Types'
import Functions from '../../Utility/Functions'
import CustomAlert from '../../Utility/CustomAlert'
import * as RootNavigation from '../../Routers/RootNavigation'
import { APP_TITLE } from '../../Utility/Constants'
import { Alert } from 'react-native'

// add card API call
export const addCardAPI = (data) => (dispatch) => {
    dispatch({
        type: ADD_CARD_REQUESTED
    })
    requestAddCardAPI(dispatch, data)
}

const requestAddCardAPI = (dispatch, data) => {
    Functions.postApi(`/addCard`, data, {}).then((data) => {
        console.log('addCard data = ', data)
        if (data.status == 200) {
            if (data.data.success == true) {
                dispatch(addCardAPISuccess(data.data.message))
            }
            else {
                dispatch(addCardAPIFailed(data.data.error ? data.data.error == null ? data.data.message : data.data.error : data.data.message))
            }
        }
        else {
            dispatch(addCardAPIFailed(data.data.error))
        }
    })
        .catch((error) => {
            console.log('addCard error = ', error)
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log('error.response.data = ', error.response.data)
                console.log('error.response.status = ', error.response.status)
                if (error.response.status == 505) {
                    if (error.response.data.success == false) {
                        dispatch(addCardAPIFailed(undefined))
                        setTimeout(() => {
                            dispatch(forceUpdateAction(error.response.data))
                        }, 100)
                    }
                    else {
                        dispatch(addCardAPIFailed(error.response.data.message))
                    }
                }
                else if (error.response.status == 401) {
                    if (error.response.data.success == false) {
                        dispatch(addCardAPIFailed(undefined))
                        dispatch(unAuthorizedUserAction(error.response.data.message))
                    }
                    else {
                        dispatch(addCardAPIFailed(error.response.data.message))
                    }
                }
                else {
                    dispatch(addCardAPIFailed(error.response.data.message ? error.response.data.message : undefined))
                }
            }
            else {
                // Something happened in setting up the request that triggered an Error
                console.log('error.message', error.message)
                dispatch(addCardAPIFailed(error.message ? error.message : GLOBAL_API_ERROR_MESSAGE))
            }
        })
}

const addCardAPISuccess = (message) => (dispatch) => {
    if (message) {
        setTimeout(() => {
            Alert.alert(
                APP_TITLE,
                message,
                [
                    {
                        text: 'Ok',
                        onPress: () => {
                            RootNavigation.navigate('Home')
                        }
                    }
                ],
                {
                    cancelable: false
                }
            )
        }, 100)
    }
    dispatch({
        type: ADD_CARD_SUCCESS
    })
}

const addCardAPIFailed = (errorMessage) => (dispatch) => {
    if (errorMessage) {
        if (errorMessage.raw) {
            if (errorMessage.raw.message) {
                setTimeout(() => {
                    CustomAlert.alert(errorMessage.raw.message)
                }, 100)
            }
        }
    }
    dispatch({
        type: ADD_CARD_FAILED
    })
}

export const addCardAPIErrorReset = () => (dispatch) => {
    dispatch({
        type: ADD_CARD_ERROR_RESET
    })
}