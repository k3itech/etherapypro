import {
    CONECTED_TO_SOCKET_EVENT,
    CONECTED_TO_SOCKET_EVENT_RESET,
    GLOBAL_SOCKET_CONNECTION_STATUS_UPDATE,
    SAVE_CONNECTED_SOCKET_ID,
    RESET_CONNECTED_SOCKET_ID,
    CHAT_HISTORY_DATA_REQUESTED,
    CHAT_HISTORY_DATA_SUCCESS,
    APPEND_CHAT_HISTORY_DATA,
    SHOW_CHAT_LOADER,
    HIDE_CHAT_LOADER
} from '../Types'

export const globalSocketConnectionStatusUpdate = (value) => (dispatch) => {
    dispatch({
        type: GLOBAL_SOCKET_CONNECTION_STATUS_UPDATE,
        payload: value
    })
}

export const connectedToSocket = () => (dispatch) => {
    dispatch({
        type: CONECTED_TO_SOCKET_EVENT
    })
}

export const connectedToSocketReset = () => (dispatch) => {
    dispatch({
        type: CONECTED_TO_SOCKET_EVENT_RESET
    })
}

export const saveConnectedSocketID = (socket_id) => (dispatch) => {
    dispatch({
        type: SAVE_CONNECTED_SOCKET_ID,
        payload: socket_id
    })
}

export const resetConnectedSocketID = () => (dispatch) => {
    dispatch({
        type: RESET_CONNECTED_SOCKET_ID
    })
}

export const getChatHistoryData = (data) => (dispatch) => {
    dispatch({
        type: CHAT_HISTORY_DATA_REQUESTED
    })

    requestGetChatHistoryData(dispatch, data)
}

const requestGetChatHistoryData = (dispatch, data) => {
    dispatch({
        type: CHAT_HISTORY_DATA_SUCCESS,
        payload: data
    })
}

export const appendChatHistoryData = (data) => (dispatch) => {
    dispatch({
        type: APPEND_CHAT_HISTORY_DATA,
        payload: data
    })
}

export const showChatLoader = () => (dispatch) => {
    dispatch({
        type: SHOW_CHAT_LOADER
    })
}

export const hideChatLoader = () => (dispatch) => {
    dispatch({
        type: HIDE_CHAT_LOADER
    })
}