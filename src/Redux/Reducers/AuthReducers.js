import {
    HANDLE_NETWORK_CONNECION_STATUS_UPDATE,
    SYNC_ALREADY_LOGIN_USER_DATA,
    USER_LOGOUT_REQUESTED,
    USER_LOGOUT_SUCCESS,
    USER_LOGOUT_FAILED,
    USER_LOGOUT_ERROR_RESET,
    REGISTER_STATIC_DATA_REQUESTED,
    REGISTER_STATIC_DATA_SUCCESS,
    REGISTER_STATIC_DATA_FAILED,
    REGISTER_STATIC_DATA_ERROR_RESET,
    SELECT_REGISTER_STEP1_FEELINGS_DATA,
    SELECT_REGISTER_STEP2_CHALLENGES_DATA,
    SELECT_REGISTER_STEP3_AREALIFE_DATA,
    SELECT_REGISTER_STEP4_CHANGE_ONE_THING_DATA,
    SELECT_REGISTER_STEP5_SUICIDE_ATTEMPT_DATA,
    SELECT_REGISTER_STEP5_COUNSELLING_DATA,
    SELECT_REGISTER_STEP6_AGE_DATA,
    SELECT_REGISTER_STEP6_COUNTRY_DATA,
    SELECT_REGISTER_STEP6_STATE_DATA,
    SELECT_REGISTER_STEP7_RELATIONSHIP_DATA,
    SELECT_REGISTER_STEP8_GENDER_DATA,
    SELECT_REGISTER_STEP9_SEXUAL_ORIENTATION_DATA,
    SELECT_REGISTER_STEP10_RELIGIOUS_DATA,
    SELECT_REGISTER_STEP10_ILLNESS_PAIN_DATA,
    SELECT_REGISTER_STEP11_MEDICATION_DATA,
    SELECT_REGISTER_STEP11_READY_TO_GET_STARTED_DATA,
    REGISTER_REQUESTED,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
    REGISTER_ERROR_RESET,
    LOGIN_REQUESTED,
    LOGIN_SUCCESS, 
    LOGIN_FAILED,
    LOGIN_ERROR_RESET,
    FORGOT_PASSWORD_REQUESTED,
    FORGOT_PASSWORD_SUCCESS,
    FORGOT_PASSWORD_FAILED,
    FORGOT_PASSWORD_ERROR_RESET,
    CHANGE_PASSWORD_REQUESTED,
    CHANGE_PASSWORD_SUCCESS,
    CHANGE_PASSWORD_FAILED,
    CHANGE_PASSWORD_ERROR_RESET,
    MY_PROFILE_REQUESTED,
    MY_PROFILE_SUCCESS,
    MY_PROFILE_FAILED,
    MY_PROFILE_ERROR_RESET
} from '../Types'

const initialState = {
    loading: false,
    userData: undefined,
    errorMessageLogout: undefined,
    isNetworkConnected: undefined,
    registrationStaticData: undefined,
    registrationStaticDataErrorMessage: undefined,
    selectedRegisterStep1FeelingsData: '',
    selectedRegisterStep2ChallengesData: '',
    selectedRegisterStep3AreaLifeData: [],
    selectedRegisterStep4ChangeOneThingData: '',
    selectedRegisterStep5SuicideAttemptData: undefined,
    selectedRegisterStep5CounsellingData: undefined,
    selectedRegisterStep6AgeData: '',
    selectedRegisterStep6CountryData: '',
    selectedRegisterStep6StateData: '',
    selectedRegisterStep7RelationshipData: '',
    selectedRegisterStep8GenderData: '',
    selectedRegisterStep9SexualOrientationData: '',
    selectedRegisterStep10ReligiousData: '',
    selectedRegisterStep10IllnessPainData: undefined,
    selectedRegisterStep11MedicationData: undefined,
    selectedRegisterStep11GetReadyToStartData: undefined,
    registerErrorMessage: undefined,
    loginErrorMessage: undefined,
    forgotPasswordErrorMessage: undefined,
    forgotPasswordSuccess: false,
    changePasswordErrorMessage: undefined,
    changePasswordSuccessMessage: undefined,
    myProfileErrorMessage: undefined
}

// make this Reducer available to the app
export default (state = initialState, action) => {

    const {
        type,
        payload
    } = action

    switch (type) {
        
        case HANDLE_NETWORK_CONNECION_STATUS_UPDATE: {
            return {
                ...state,
                isNetworkConnected: payload
            }
        }

        case SYNC_ALREADY_LOGIN_USER_DATA: {
            return {
                ...state,
                loading: false,
                userData: payload
            }
        }

        case USER_LOGOUT_REQUESTED: {
            return {
                ...state,
                loading: true,
                errorMessageLogout: undefined
            }
        }

        case USER_LOGOUT_SUCCESS: {
            return {
                ...state,
                loading: false,
                userData: undefined,
                errorMessageLogout: undefined
            }
        }

        case USER_LOGOUT_FAILED: {
            return {
                ...state,
                loading: false,
                errorMessageLogout: payload
            }
        }

        case USER_LOGOUT_ERROR_RESET: {
            return {
                ...state,
                loading: false,
                errorMessageLogout: undefined
            }
        }

        case REGISTER_STATIC_DATA_REQUESTED: {
            return {
                ...state,
                loading: true,
                registrationStaticDataErrorMessage: undefined,
                registrationStaticData: undefined
            }
        }

        case REGISTER_STATIC_DATA_SUCCESS: {
            return {
                ...state,
                loading: false,
                registrationStaticDataErrorMessage: undefined,
                registrationStaticData: payload
            }
        }

        case REGISTER_STATIC_DATA_FAILED: {
            return {
                ...state,
                loading: false,
                registrationStaticDataErrorMessage: payload,
                registrationStaticData: undefined
            }
        }

        case REGISTER_STATIC_DATA_ERROR_RESET: {
            return {
                ...state,
                loading: false,
                registrationStaticDataErrorMessage: undefined
            }
        }

        case SELECT_REGISTER_STEP1_FEELINGS_DATA: {
            return {
                ...state,
                selectedRegisterStep1FeelingsData: payload
            }
        }

        case SELECT_REGISTER_STEP2_CHALLENGES_DATA: {
            return {
                ...state,
                selectedRegisterStep2ChallengesData: payload
            }
        }

        case SELECT_REGISTER_STEP3_AREALIFE_DATA: {
            return {
                ...state,
                selectedRegisterStep3AreaLifeData: payload
            }
        }

        case SELECT_REGISTER_STEP4_CHANGE_ONE_THING_DATA: {
            return {
                ...state,
                selectedRegisterStep4ChangeOneThingData: payload
            }
        }

        case SELECT_REGISTER_STEP5_SUICIDE_ATTEMPT_DATA: {
            return {
                ...state,
                selectedRegisterStep5SuicideAttemptData: payload
            }
        }

        case SELECT_REGISTER_STEP5_COUNSELLING_DATA: {
            return {
                ...state,
                selectedRegisterStep5CounsellingData: payload
            }
        }

        case SELECT_REGISTER_STEP6_AGE_DATA: {
            return {
                ...state,
                selectedRegisterStep6AgeData: payload
            }
        }

        case SELECT_REGISTER_STEP6_COUNTRY_DATA: {
            return {
                ...state,
                selectedRegisterStep6CountryData: payload
            }
        }

        case SELECT_REGISTER_STEP6_STATE_DATA: {
            return {
                ...state,
                selectedRegisterStep6StateData: payload
            }
        }

        case SELECT_REGISTER_STEP7_RELATIONSHIP_DATA: {
            return {
                ...state,
                selectedRegisterStep7RelationshipData: payload
            }
        }

        case SELECT_REGISTER_STEP8_GENDER_DATA: {
            return {
                ...state,
                selectedRegisterStep8GenderData: payload
            }
        }

        case SELECT_REGISTER_STEP9_SEXUAL_ORIENTATION_DATA: {
            return {
                ...state,
                selectedRegisterStep9SexualOrientationData: payload
            }
        }

        case SELECT_REGISTER_STEP10_RELIGIOUS_DATA: {
            return {
                ...state,
                selectedRegisterStep10ReligiousData: payload
            }
        }

        case SELECT_REGISTER_STEP10_ILLNESS_PAIN_DATA: {
            return {
                ...state,
                selectedRegisterStep10IllnessPainData: payload
            }
        }

        case SELECT_REGISTER_STEP11_MEDICATION_DATA: {
            return {
                ...state,
                selectedRegisterStep11MedicationData: payload
            }
        }

        case SELECT_REGISTER_STEP11_READY_TO_GET_STARTED_DATA: {
            return {
                ...state,
                selectedRegisterStep11GetReadyToStartData: payload
            }
        }

        case REGISTER_REQUESTED: {
            return {
                ...state,
                loading: true,
                registerErrorMessage: undefined,
                userData: undefined
            }
        }

        case REGISTER_SUCCESS: {
            return {
                ...state,
                loading: false,
                registerErrorMessage: undefined,
                userData: payload
            }
        }

        case REGISTER_FAILED: {
            return {
                ...state,
                loading: false,
                registerErrorMessage: payload,
                userData: undefined
            }
        }

        case REGISTER_ERROR_RESET: {
            return {
                ...state,
                loading: false,
                registerErrorMessage: undefined
            }
        }

        case LOGIN_REQUESTED: {
            return {
                ...state,
                loading: true,
                loginErrorMessage: undefined,
                userData: undefined
            }
        }

        case LOGIN_SUCCESS: {
            return {
                ...state,
                loading: false,
                loginErrorMessage: undefined,
                userData: payload
            }
        }

        case LOGIN_FAILED: {
            return {
                ...state,
                loading: false,
                loginErrorMessage: payload,
                userData: undefined
            }
        }

        case LOGIN_ERROR_RESET: {
            return {
                ...state,
                loading: false,
                loginErrorMessage: undefined
            }
        }

        case FORGOT_PASSWORD_REQUESTED: {
            return {
                ...state,
                loading: true,
                forgotPasswordErrorMessage: undefined,
                forgotPasswordSuccess: false
            }
        }

        case FORGOT_PASSWORD_SUCCESS: {
            return {
                ...state,
                loading: false,
                forgotPasswordErrorMessage: undefined,
                forgotPasswordSuccess: true
            }
        }

        case FORGOT_PASSWORD_FAILED: {
            return {
                ...state,
                loading: false,
                forgotPasswordErrorMessage: payload,
                forgotPasswordSuccess: false
            }
        }

        case FORGOT_PASSWORD_ERROR_RESET: {
            return {
                ...state,
                loading: false,
                forgotPasswordErrorMessage: undefined,
                forgotPasswordSuccess: false
            }
        }

        case CHANGE_PASSWORD_REQUESTED: {
            return {
                ...state,
                loading: true,
                changePasswordErrorMessage: undefined,
                changePasswordSuccessMessage: undefined
            }
        }

        case CHANGE_PASSWORD_SUCCESS: {
            return {
                ...state,
                loading: false,
                changePasswordErrorMessage: undefined,
                changePasswordSuccessMessage: payload
            }
        }

        case CHANGE_PASSWORD_FAILED: {
            return {
                ...state,
                loading: false,
                changePasswordErrorMessage: payload,
                changePasswordSuccessMessage: undefined
            }
        }

        case CHANGE_PASSWORD_ERROR_RESET: {
            return {
                ...state,
                loading: false,
                changePasswordErrorMessage: undefined,
                changePasswordSuccessMessage: undefined
            }
        }

        case MY_PROFILE_REQUESTED: {
            return {
                ...state,
                loading: true,
                myProfileErrorMessage: undefined
            }
        }

        case MY_PROFILE_SUCCESS: {
            return {
                ...state,
                loading: false,
                myProfileErrorMessage: undefined,
                userData: payload
            }
        }

        case MY_PROFILE_FAILED: {
            return {
                ...state,
                loading: false,
                myProfileErrorMessage: payload
            }
        }

        case MY_PROFILE_ERROR_RESET: {
            return {
                ...state,
                loading: false,
                myProfileErrorMessage: undefined
            }
        }

        default: {
            return {
                ...state
            }
        }
    }
}