import {
    ADD_CARD_REQUESTED,
    ADD_CARD_SUCCESS,
    ADD_CARD_FAILED,
    ADD_CARD_ERROR_RESET
} from '../Types'

const initialState = {
    loading: false,
    addCardErrorMessage: undefined,
    addCardSuccess: false
}

// make this Reducer available to the app
export default (state = initialState, action) => {

    const {
        type,
        payload
    } = action

    switch (type) {

        case ADD_CARD_REQUESTED: {
            return {
                ...state,
                loading: true,
                addCardErrorMessage: undefined,
                addCardSuccess: false
            }
        }

        case ADD_CARD_SUCCESS: {
            return {
                ...state,
                loading: false,
                addCardSuccess: true,
                addCardErrorMessage: undefined
            }
        }

        case ADD_CARD_FAILED: {
            return {
                ...state,
                loading: false,
                addCardErrorMessage: payload,
                addCardSuccess: false
            }
        }

        case ADD_CARD_ERROR_RESET: {
            return {
                ...state,
                loading: false,
                addCardErrorMessage: undefined,
                addCardSuccess: false
            }
        }

        default: {
            return {
                ...state
            }
        }
    }
}