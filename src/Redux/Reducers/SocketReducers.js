import {
    CONECTED_TO_SOCKET_EVENT,
    CONECTED_TO_SOCKET_EVENT_RESET,
    GLOBAL_SOCKET_CONNECTION_STATUS_UPDATE,
    SAVE_CONNECTED_SOCKET_ID,
    RESET_CONNECTED_SOCKET_ID,
    CHAT_HISTORY_DATA_REQUESTED,
    CHAT_HISTORY_DATA_SUCCESS,
    APPEND_CHAT_HISTORY_DATA,
    SHOW_CHAT_LOADER,
    HIDE_CHAT_LOADER
} from '../Types'

const initialState = {
    loading: false,
    connectedToSockedEvent: false,
    globalSocketConnectionStatus: 0,
    connectedSocketID: undefined,
    chatHistoryData: []
}

// make this Reducer available to the app
export default (state = initialState, action) => {

    const {
        type,
        payload
    } = action 

    switch (type) {

        case GLOBAL_SOCKET_CONNECTION_STATUS_UPDATE: {
            return {
                ...state,
                globalSocketConnectionStatus: payload
            }
        }

        case CONECTED_TO_SOCKET_EVENT: {
            return {
                ...state,
                connectedToSockedEvent: true
            }
        }

        case CONECTED_TO_SOCKET_EVENT_RESET: {
            return {
                ...state,
                connectedToSockedEvent: false
            }
        }

        case SAVE_CONNECTED_SOCKET_ID: {
            return {
                ...state,
                connectedSocketID: payload
            }
        }

        case RESET_CONNECTED_SOCKET_ID: {
            return {
                ...state,
                connectedSocketID: undefined
            }
        }

        case CHAT_HISTORY_DATA_REQUESTED: {
            return {
                ...state,
                // chatHistoryData: []
            }
        }

        case CHAT_HISTORY_DATA_SUCCESS: {
            return {
                ...state,
                chatHistoryData: payload
            }
        }

        case APPEND_CHAT_HISTORY_DATA: {
            return {
                ...state,
                chatHistoryData: [payload, ...state.chatHistoryData]
            }
        }

        case SHOW_CHAT_LOADER: {
            return {
                ...state,
                loading: true
            }
        }

        case HIDE_CHAT_LOADER: {
            return {
                ...state,
                loading: false
            }
        }

        default: {
            return {
                ...state
            }
        }
    }
}