import { combineReducers } from 'redux'

import { USER_LOGOUT_SUCCESS_RESET } from '../Types'

import AuthReducers from './AuthReducers'
import SocketReducers from './SocketReducers'
import CardReducers from './CardReducers'

// make these Combine Reducers(All) available to the app
const appReducer = combineReducers({
    auth: AuthReducers,
    socket: SocketReducers,
    card: CardReducers
})

const rootReducer = (state, action) => {
    if (action.type === USER_LOGOUT_SUCCESS_RESET) {
        state = undefined
    }
    return appReducer(state, action)
}

export default rootReducer