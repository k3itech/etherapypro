export const SYNC_ALREADY_LOGIN_USER_DATA = 'SYNC_ALREADY_LOGIN_USER_DATA'

export const HANDLE_NETWORK_CONNECION_STATUS_UPDATE = 'HANDLE_NETWORK_CONNECION_STATUS_UPDATE'

export const GLOBAL_SOCKET_CONNECTION_STATUS_UPDATE = 'GLOBAL_SOCKET_CONNECTION_STATUS_UPDATE'
export const CONECTED_TO_SOCKET_EVENT = 'CONECTED_TO_SOCKET_EVENT'
export const CONECTED_TO_SOCKET_EVENT_RESET = 'CONECTED_TO_SOCKET_EVENT_RESET'

export const USER_LOGOUT_REQUESTED = 'USER_LOGOUT_REQUESTED'
export const USER_LOGOUT_SUCCESS = 'USER_LOGOUT_SUCCESS'
export const USER_LOGOUT_FAILED = 'USER_LOGOUT_FAILED'
export const USER_LOGOUT_ERROR_RESET = 'USER_LOGOUT_ERROR_RESET'
export const USER_LOGOUT_SUCCESS_RESET = 'USER_LOGOUT_SUCCESS_RESET'

export const REGISTER_STATIC_DATA_REQUESTED = 'REGISTER_STATIC_DATA_REQUESTED'
export const REGISTER_STATIC_DATA_SUCCESS = 'REGISTER_STATIC_DATA_SUCCESS'
export const REGISTER_STATIC_DATA_FAILED = ''
export const REGISTER_STATIC_DATA_ERROR_RESET = 'REGISTER_STATIC_DATA_ERROR_RESET'

export const SELECT_REGISTER_STEP1_FEELINGS_DATA = 'SELECT_REGISTER_STEP1_FEELINGS_DATA'
export const SELECT_REGISTER_STEP2_CHALLENGES_DATA = 'SELECT_REGISTER_STEP2_CHALLENGES_DATA'
export const SELECT_REGISTER_STEP3_AREALIFE_DATA = 'SELECT_REGISTER_STEP3_AREALIFE_DATA'
export const SELECT_REGISTER_STEP4_CHANGE_ONE_THING_DATA = 'SELECT_REGISTER_STEP4_CHANGE_ONE_THING_DATA'
export const SELECT_REGISTER_STEP5_SUICIDE_ATTEMPT_DATA = 'SELECT_REGISTER_STEP5_SUICIDE_ATTEMPT_DATA'
export const SELECT_REGISTER_STEP5_COUNSELLING_DATA = 'SELECT_REGISTER_STEP5_COUNSELLING_DATA'
export const SELECT_REGISTER_STEP6_AGE_DATA = 'SELECT_REGISTER_STEP6_AGE_DATA'
export const SELECT_REGISTER_STEP6_COUNTRY_DATA = 'SELECT_REGISTER_STEP6_COUNTRY_DATA'
export const SELECT_REGISTER_STEP6_STATE_DATA = 'SELECT_REGISTER_STEP6_STATE_DATA'
export const SELECT_REGISTER_STEP7_RELATIONSHIP_DATA = 'SELECT_REGISTER_STEP7_RELATIONSHIP_DATA'
export const SELECT_REGISTER_STEP8_GENDER_DATA = 'SELECT_REGISTER_STEP8_GENDER_DATA'
export const SELECT_REGISTER_STEP9_SEXUAL_ORIENTATION_DATA = 'SELECT_REGISTER_STEP9_SEXUAL_ORIENTATION_DATA'
export const SELECT_REGISTER_STEP10_RELIGIOUS_DATA = 'SELECT_REGISTER_STEP10_RELIGIOUS_DATA'
export const SELECT_REGISTER_STEP10_ILLNESS_PAIN_DATA = 'SELECT_REGISTER_STEP10_ILLNESS_PAIN_DATA'
export const SELECT_REGISTER_STEP11_MEDICATION_DATA = 'SELECT_REGISTER_STEP11_MEDICATION_DATA'
export const SELECT_REGISTER_STEP11_READY_TO_GET_STARTED_DATA = 'SELECT_REGISTER_STEP11_READY_TO_GET_STARTED_DATA'

export const REGISTER_REQUESTED = 'REGISTER_REQUESTED'
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAILED = 'REGISTER_FAILED'
export const REGISTER_ERROR_RESET = 'REGISTER_ERROR_RESET'

export const LOGIN_REQUESTED = 'LOGIN_REQUESTED'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILED = 'LOGIN_FAILED'
export const LOGIN_ERROR_RESET = 'LOGIN_ERROR_RESET'

// FORGOT PASSWORD
export const FORGOT_PASSWORD_REQUESTED = 'FORGOT_PASSWORD_REQUESTED'
export const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS'
export const FORGOT_PASSWORD_FAILED = 'FORGOT_PASSWORD_FAILED'
export const FORGOT_PASSWORD_ERROR_RESET = 'FORGOT_PASSWORD_ERROR_RESET'

// change password
export const CHANGE_PASSWORD_REQUESTED = 'CHANGE_PASSWORD_REQUESTED'
export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS'
export const CHANGE_PASSWORD_FAILED = 'CHANGE_PASSWORD_FAILED'
export const CHANGE_PASSWORD_ERROR_RESET = 'CHANGE_PASSWORD_ERROR_RESET'

// my profile
export const MY_PROFILE_REQUESTED = 'MY_PROFILE_REQUESTED'
export const MY_PROFILE_SUCCESS = 'MY_PROFILE_SUCCESS'
export const MY_PROFILE_FAILED = 'MY_PROFILE_FAILED'
export const MY_PROFILE_ERROR_RESET = 'MY_PROFILE_ERROR_RESET'

// socket types
export const SAVE_CONNECTED_SOCKET_ID = 'SAVE_CONNECTED_SOCKET_ID'
export const RESET_CONNECTED_SOCKET_ID = 'RESET_CONNECTED_SOCKET_ID'
export const CHAT_HISTORY_DATA_REQUESTED = 'CHAT_HISTORY_DATA_REQUESTED'
export const CHAT_HISTORY_DATA_SUCCESS = 'CHAT_HISTORY_DATA_SUCCESS'
export const APPEND_CHAT_HISTORY_DATA = 'APPEND_CHAT_HISTORY_DATA'
export const SHOW_CHAT_LOADER = 'SHOW_CHAT_LOADER'
export const HIDE_CHAT_LOADER = 'HIDE_CHAT_LOADER'

// ADD CARD
export const ADD_CARD_REQUESTED = 'ADD_CARD_REQUESTED'
export const ADD_CARD_SUCCESS = 'ADD_CARD_SUCCESS'
export const ADD_CARD_FAILED = 'ADD_CARD_FAILED'
export const ADD_CARD_ERROR_RESET = 'ADD_CARD_ERROR_RESET'