import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { navigationRef, isReadyRef } from './RootNavigation'
import SwitchNavigator from './SwitchNavigator'

const Stack = createStackNavigator()

export default class AppRoute extends Component {
    render() {
        return (
            <NavigationContainer
                ref={navigationRef}
                onReady={() => {
                    isReadyRef.current == true
                }}
            >
                <Stack.Navigator
                    screenOptions={{
                        headerShown: false
                    }}
                >
                    <Stack.Screen
                        name='SwitchNavigator'
                        component={SwitchNavigator}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}