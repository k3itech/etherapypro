import React, { Component } from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import Home from '../Screens/Home'
import StarredMessages from '../Screens/StarredMessages'
import Settings from '../Screens/Settings'
import ManageSubscription from '../Screens/ManageSubscription'
import AgentProfile from '../Screens/AgentProfile'
import AgentBookSession from '../Screens/AgentBookSession'
import ClinicalProcess from '../Screens/ClinicalProcess'
import WelcomeBack from '../Screens/WelcomeBack'
import ChangePassword from '../Screens/ChangePassword'
import ChatDetail from '../Screens/ChatDetail'
import AudioPlayerContainer from '../Screens/AudioPlayerContainer'
import AudioRecorderContainer from '../Screens/AudioRecorderContainer'
import AccountDetails from '../Screens/AccountDetails'
import AddCardDetails from '../Screens/AddCardDetails'

import store from '../Utility/ReduxStore'
import ContactUs from '../Screens/ContactUs'

const RootStack = createStackNavigator()

class AppNavigator extends Component {

    render() {
        return (
            <RootStack.Navigator
                screenOptions={{
                    headerShown: false
                }}
                initialRouteName='Home'
            >
                <RootStack.Screen
                    name='Home'
                    component={Home}
                />
                <RootStack.Screen
                    name='StarredMessages'
                    component={StarredMessages}
                />
                <RootStack.Screen
                    name='Settings'
                    component={Settings}
                />
                <RootStack.Screen
                    name='ManageSubscription'
                    component={ManageSubscription}
                />
                <RootStack.Screen
                    name='AgentProfile'
                    component={AgentProfile}
                />
                <RootStack.Screen
                    name='ContactUs'
                    component={ContactUs}
                />
                <RootStack.Screen
                    name='AgentBookSession'
                    component={AgentBookSession}
                />
                <RootStack.Screen
                    name='ClinicalProcess'
                    component={ClinicalProcess}
                />
                <RootStack.Screen
                    name='WelcomeBack'
                    component={WelcomeBack}
                />
                <RootStack.Screen
                    name='ChangePassword'
                    component={ChangePassword}
                />
                <RootStack.Screen
                    name='ChatDetail'
                    component={ChatDetail}
                />
                <RootStack.Screen
                    name='AudioPlayer'
                    component={AudioPlayerContainer}
                />
                <RootStack.Screen
                    name='AudioRecorder'
                    component={AudioRecorderContainer}
                />
                <RootStack.Screen
                    name='AccountDetails'
                    component={AccountDetails}
                />
                <RootStack.Screen
                    name='AddCardDetails'
                    component={AddCardDetails}
                />
            </RootStack.Navigator>
        )
    }
}

export default AppNavigator