import React, { Component } from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import store from '../Utility/ReduxStore'

import Login from '../Screens/Login'
import Welcome from '../Screens/Welcome'
import GetStarted from '../Screens/GetStarted'
import RegisterStep1Feeling from '../Screens/RegisterStep1Feeling'
import RegisterStep2Challenge from '../Screens/RegisterStep2Challenge'
import RegisterStep3AreaLife from '../Screens/RegisterStep3AreaLife'
import RegisterStep4ChangeOneThing from '../Screens/RegisterStep4ChangeOneThing'
import RegisterStep5SuicideCounselling from '../Screens/RegisterStep5SuicideCounselling'
import RegisterStep6AgeCountryState from '../Screens/RegisterStep6AgeCountryState'
import RegisterStep7Relationhip from '../Screens/RegisterStep7Relationhip'
import RegisterStep8Gender from '../Screens/RegisterStep8Gender'
import RegisterStep9SexualOrientation from '../Screens/RegisterStep9SexualOrientation'
import RegisterStep10ReligiousIllness from '../Screens/RegisterStep10ReligiousIllness'
import RegisterStep11GetStarted from '../Screens/RegisterStep11GetStarted'
import RegisterStep12Form from '../Screens/RegisterStep12Form'
import RegisterStep13 from '../Screens/RegisterStep13'
import ForgotPassword from '../Screens/ForgotPassword'

const Stack = createStackNavigator()

export default class AuthNavigator extends Component {
    render() {
        return (
            <Stack.Navigator
                screenOptions={{
                    headerShown: false
                }}
                initialRouteName={'Welcome'}
            >
                <Stack.Screen
                    name='Welcome'
                    component={Welcome}
                />
                <Stack.Screen
                    name='GetStarted'
                    component={GetStarted}
                />
                <Stack.Screen
                    name='Login'
                    component={Login}
                />
                <Stack.Screen
                    name='RegisterStep1Feeling'
                    component={RegisterStep1Feeling}
                />
                <Stack.Screen
                    name='RegisterStep2Challenge'
                    component={RegisterStep2Challenge}
                />
                <Stack.Screen
                    name='RegisterStep3AreaLife'
                    component={RegisterStep3AreaLife}
                />
                <Stack.Screen
                    name='RegisterStep4ChangeOneThing'
                    component={RegisterStep4ChangeOneThing}
                />
                <Stack.Screen
                    name='RegisterStep5SuicideCounselling'
                    component={RegisterStep5SuicideCounselling}
                />
                <Stack.Screen
                    name='RegisterStep6AgeCountryState'
                    component={RegisterStep6AgeCountryState}
                />
                <Stack.Screen
                    name='RegisterStep7Relationhip'
                    component={RegisterStep7Relationhip}
                />
                <Stack.Screen
                    name='RegisterStep8Gender'
                    component={RegisterStep8Gender}
                />
                <Stack.Screen
                    name='RegisterStep9SexualOrientation'
                    component={RegisterStep9SexualOrientation}
                />
                <Stack.Screen
                    name='RegisterStep10ReligiousIllness'
                    component={RegisterStep10ReligiousIllness}
                />
                <Stack.Screen
                    name='RegisterStep11GetStarted'
                    component={RegisterStep11GetStarted}
                />
                <Stack.Screen
                    name='RegisterStep12Form'
                    component={RegisterStep12Form}
                />
                <Stack.Screen
                    name='RegisterStep13'
                    component={RegisterStep13}
                />
                <Stack.Screen
                    name='ForgotPassword'
                    component={ForgotPassword}
                />
            </Stack.Navigator>
        )
    }
}