import React, { Component } from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import AppNavigator from './AppNavigator'
import AuthNavigator from './AuthNavigator'
import Loading from '../Screens/Loading'

const Stack = createStackNavigator()

export default class SwitchNavigator extends Component {
    render() {
        return (
            <Stack.Navigator
                screenOptions={{
                    headerShown: false
                }}
            >
                <Stack.Screen
                    name='Loading'
                    component={Loading}
                />
                <Stack.Screen
                    name='Auth'
                    component={AuthNavigator}
                />
                <Stack.Screen
                    name='App'
                    component={AppNavigator}
                />
            </Stack.Navigator>
        )
    }
}