//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    Image,
    TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import PageContainer from "../../Components/PageContainer"
import styles from './style'
import HeaderNew from '../../Components/HeaderNew'
import Functions from '../../Utility/Functions'
import OverlayLoading from '../../Components/OverlayLoading'
import {
    myProfileAPI,
    myProfileAPIErrorReset
} from '../../Redux/Actions'
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class AccountDetails extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    static getDerivedStateFromProps(props, state) {
        // Any time the current user changes,
        // Reset any parts of state that are tied to that user.

        if (props.myProfileErrorMessage != undefined) {
            setTimeout(() => {
                CustomAlert.alert(props.myProfileErrorMessage)
                props.myProfileAPIErrorReset()
            }, 100)
        }

        return null
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.props.myProfileAPI(this.props.userData.token)
        })
    }

    componentWillUnmount() {
        this._unsubscribe()
    }

    render() {
        const {
            main,
            bgStyle,
            mainViewStyle
        } = styles
        return (
            <>
                <View style={bgStyle}>
                    <SafeAreaView></SafeAreaView>
                    <View style={{ height: 80 }}>
                        <HeaderNew
                            isTitle={true}
                            title={'Account details'}
                            isLeftIcon={true}
                            onPressLeftIcon={() => {
                                this.props.navigation.goBack()
                            }}
                        />
                    </View>
                    <OverlayLoading
                        visible={this.props.loading}
                    />
                    <View style={main}>
                        <PageContainer padding={vw(0)}>
                            <View style={mainViewStyle}>
                                <View style={{ borderWidth: 0, flex: 0.55, justifyContent: 'center' }}>
                                    <Text style={{ color: Colors.black, fontFamily: Fonts.semibold, fontSize: vw(16) }}>Nickname</Text>
                                    <Text style={{ color: 'rgba(0, 0, 0, 0.6)', fontFamily: Fonts.semibold, fontSize: vw(12), marginTop: vw(2) }}>Atul</Text>
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'flex-end', borderWidth: 0, flex: 0.45 }}>
                                    <Text style={{ color: Colors.appGreen, fontFamily: Fonts.semibold, fontSize: vw(16) }}>Change nickname</Text><Text style={{ color: Colors.appGreen, fontFamily: Fonts.semibold, fontSize: vw(16) }}></Text>
                                </View>
                            </View>
                            <View style={mainViewStyle}>
                                <View style={{ borderWidth: 0, flex: 0.55, justifyContent: 'center' }}>
                                    <Text style={{ color: Colors.black, fontFamily: Fonts.semibold, fontSize: vw(16) }}>Email</Text>
                                    <Text style={{ color: 'rgba(0, 0, 0, 0.6)', fontFamily: Fonts.semibold, fontSize: vw(12), marginTop: vw(2) }}>Atul@gmail.com</Text>
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'flex-end', borderWidth: 0, flex: 0.45 }}>
                                    <Text style={{ color: Colors.appGreen, fontFamily: Fonts.semibold, fontSize: vw(16) }}>Change email</Text><Text style={{ color: Colors.appGreen, fontFamily: Fonts.semibold, fontSize: vw(16) }}></Text>
                                </View>
                            </View>
                            <View style={mainViewStyle}>
                                <View style={{ borderWidth: 0, flex: 0.55, justifyContent: 'center' }}>
                                    <Text style={{ color: Colors.black, fontFamily: Fonts.semibold, fontSize: vw(16) }}>Password</Text>
                                    <Text style={{ color: 'rgba(0, 0, 0, 0.6)', fontFamily: Fonts.semibold, fontSize: vw(12), marginTop: vw(2) }}>********</Text>
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'flex-end', borderWidth: 0, flex: 0.45 }}>
                                    <Text style={{ color: Colors.appGreen, fontFamily: Fonts.semibold, fontSize: vw(16) }}>Change password</Text><Text style={{ color: Colors.appGreen, fontFamily: Fonts.semibold, fontSize: vw(16) }}></Text>
                                </View>
                            </View>
                            <View style={{ borderBottomWidth: 0.5, borderBottomColor: 'rgba(0, 0, 0, 0.4)', marginTop: vw(16), marginBottom: vw(4) }}></View>
                            <View style={mainViewStyle}>
                                <View style={{ borderWidth: 0, flex: 0.55, justifyContent: 'center' }}>
                                    <Text style={{ color: Colors.black, fontFamily: Fonts.semibold, fontSize: vw(16) }}>Payment</Text>
                                    <View style={{ borderWidth: 0, flex: 1, justifyContent: 'center', flexDirection: 'row', height: vw(16) }}>
                                        <Image
                                            source={require('../../assets/img/support.png')}
                                            resizeMode='contain'
                                            style={{ 
                                                width: vw(16),
                                                height: vw(16),
                                                marginRight: vw(4),
                                                marginTop: vw(2)
                                            }}
                                        />
                                        <Text style={{ color: 'rgba(0, 0, 0, 0.6)', fontFamily: Fonts.semibold, fontSize: vw(12), marginTop: vw(2) }}>No payment details available</Text>
                                    </View>
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'flex-end', borderWidth: 0, flex: 0.45 }}>
                                    <Text style={{ color: Colors.appGreen, fontFamily: Fonts.semibold, fontSize: vw(16) }}
                                    onPress={()=>{this.props.navigation.navigate('AddCardDetails')}}
                                    >Add card</Text><Text style={{ color: Colors.appGreen, fontFamily: Fonts.semibold, fontSize: vw(16) }}></Text>
                                </View>
                            </View>
                        </PageContainer>
                    </View>
                </View>
            </>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    userData: state.auth.userData,
    loading: state.auth.loading,
    myProfileErrorMessage: state.auth.myProfileErrorMessage
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    myProfileAPI: (token) => {
        dispatch(myProfileAPI(token))
    },
    myProfileAPIErrorReset: () => {
        dispatch(myProfileAPIErrorReset())
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AccountDetails)