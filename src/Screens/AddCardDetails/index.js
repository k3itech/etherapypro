//import liraries
import React, { Component } from 'react'
import {
    View,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import CardInput from '../../Components/CardInput'
import HeaderNew from '../../Components/HeaderNew'
import Button from '../../Components/Button'
import Colors from '../../common/Colors'
import OverlayLoading from '../../Components/OverlayLoading'
import {
    addCardAPI,
    addCardAPIErrorReset
} from '../../Redux/Actions'
import Validation from '../../Utility/Validation'
import Functions from '../../Utility/Functions'

// create a component
class AddCardDetails extends Component {

    constructor(props) {
        super(props)

        this.state = {
            cardHolderName: '',
            email: '',
            cardNumber: '',
            expiry: '',
            cvv: ''
        }
    }

    static getDerivedStateFromProps(props, state) {
        // Any time the current user changes,
        // Reset any parts of state that are tied to that user.
        return null
    }

    checkoutOnPress = () => {
        let validate = Validation.addCardDetailsFormValidation(this.state)
        if (validate.error == false) {
            Functions.isNetworkConnected().then(isNetwork => {
                if (isNetwork) {
                    var expiryDate = this.state.expiry != null ? String(this.state.expiry).trim() : this.state.expiry
                    
                    let enteredMonth = expiryDate.slice(NaN, 2)
                    let enteredYear = expiryDate.slice(2)

                    let data = {
                        name: this.state.cardHolderName,
                        address: {
                            line1: 'Ram nagar',
                            postal_code: '302019',
                            city: 'Jaipur',
                            state: 'Rajasthan',
                            country: 'India'
                        },
                        email: this.state.email,
                        cardNumber: this.state.cardNumber,
                        expMonth: enteredMonth,
                        expYear: enteredYear,
                        cvc: this.state.cvv
                    }
                    this.props.addCardAPI(data)
                }
            })
        }
    }

    render() {
        const {
            main,
            innerPageContainer
        } = styles
        return (
            <View style={main}>
                <SafeAreaView></SafeAreaView>
                <OverlayLoading
                    visible={this.props.loading}
                />
                <View style={{ height: 80 }}>
                    <HeaderNew
                        onPressLeftIcon={() => {
                            this.props.navigation.goBack()
                        }}
                        isTitle={true}
                        title={'Card Details'}
                        isLeftIcon={true}
                    />
                </View>
                <View style={innerPageContainer}>
                    <View style={{ marginHorizontal: vw(20) }}>
                        <View style={{ marginBottom: 40 }} />
                        <CardInput
                            editable={true}
                            autoCapitalize={'none'}
                            placeholder='Card Holder Name'
                            label={'Name on Card*'}
                            onChangeText={(text) => {
                                this.setState({
                                    cardHolderName: text
                                })
                            }}
                            value={this.state.cardHolderName}
                        />
                        <View style={{ marginBottom: vw(12) }} />
                        <CardInput
                            editable={true}
                            autoCapitalize={'none'}
                            placeholder='Email'
                            label={'Email*'}
                            onChangeText={(text) => {
                                this.setState({
                                    email: text
                                })
                            }}
                            value={this.state.email}
                            keyboardType={'email-address'}
                        />
                        <View style={{ marginBottom: vw(12) }} />
                        <CardInput
                            editable={true}
                            autoCapitalize={'none'}
                            placeholder='Card number'
                            label={'Card Number*'}
                            onChangeText={(text) => {
                                this.setState({
                                    cardNumber: text
                                })
                            }}
                            value={this.state.cardNumber}
                            maxLength={16}
                            keyboardType={'number-pad'}
                        />
                        <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', marginTop: vw(12) }}>
                            <View style={{ width: '70%' }}>
                                <CardInput
                                    editable={true}
                                    autoCapitalize={'none'}
                                    placeholder='Month / Year'
                                    label={'Expiry *'}
                                    onChangeText={(text) => {
                                        this.setState({
                                            expiry: text
                                        })
                                    }}
                                    value={this.state.expiry}
                                    maxLength={6}
                                    keyboardType={'number-pad'}
                                />
                            </View>
                            <View style={{ width: '20%' }}>
                                <CardInput
                                    editable={true}
                                    autoCapitalize={'none'}
                                    label={'CVV*'}
                                    placeholder='CVV'
                                    onChangeText={(text) => {
                                        this.setState({
                                            cvv: text
                                        })
                                    }}
                                    value={this.state.cvv}
                                    maxLength={3}
                                    keyboardType={'number-pad'}
                                />
                            </View>
                        </View>
                        <View style={{ height: vw(20) }} />
                        <Button
                            text={'Checkout'}
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            textColor={Colors.whiteFour}
                            fontFamily={Fonts.semibold}
                            fontSize={vw(16)}
                            height={vw(45)}
                            borderRadius={vw(5)}
                            onPress={this.checkoutOnPress}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.card.loading,
    userData: state.auth.userData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    addCardAPI: (data, token) => {
        dispatch(addCardAPI(data, token))
    },
    addCardAPIErrorReset: () => {
        dispatch(addCardAPIErrorReset())
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddCardDetails)