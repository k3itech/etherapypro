//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import RegisterButton from '../../Components/RegisterButton'
import HeaderNew from '../../Components/HeaderNew'
import star_green from '../../assets/img/star_green.png'
import agentSupport from '../../assets/img/agentSupport.png'
import film from '../../assets/img/film.png'
import Button from '../../Components/Button'
import Colors from '../../common/Colors'
import { TouchableOpacity } from 'react-native-gesture-handler'

// create a component
class AgentBookSession extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        const {
            main,
            innerPageContainer,
            noVideoSession
        } = styles
        return (
            <View style={main}>
                <SafeAreaView></SafeAreaView>
                <View style={{ height: 80 }}>
                    <HeaderNew
                        onPressLeftIcon={() => {
                            this.props.navigation.goBack()
                        }}
                        isTitle={false}
                        title={''}
                        isLeftIcon={true}
                    />
                </View>
                <View style={innerPageContainer}>
                    <View style={{ marginHorizontal: 0 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                            <RegisterButton
                                bgColor={null}
                                borderColor={'#2197F1'}
                                borderRadius={vw(111) / 2}
                                shadowColor={false}
                                height={vw(111)}
                                width={vw(111)}
                                disabled={true}
                                buttonImageWidth={vw(110)}
                                buttonImageHeight={vw(110)}
                                buttonImage={''}
                                borderWidth={3}
                            />
                        </View>
                        <Text style={{ fontSize: vw(22), fontFamily: Fonts.bold, textAlign: 'center', marginTop: vw(2), marginBottom: vw(6) }}>{`Tania Anderson`}</Text>
                        <View style={{ borderWidth: 0, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ backgroundColor: 'rgba(14, 151, 72, .5)', justifyContent: 'center', alignItems: 'center', marginRight: vw(6), width: vw(22), height: vw(22), borderRadius: vw(22) / 2 }}>
                                <Image
                                    source={agentSupport}
                                    resizeMode={'contain'}
                                    style={{}}
                                />
                            </View>
                            <Text style={{ fontSize: vw(16), fontFamily: Fonts.regular }}>{`Matching agent`}</Text>
                        </View>
                        <View style={{ marginTop: vw(16), marginHorizontal: vw(20) }}>
                            <Text style={{ fontSize: vw(22), fontFamily: Fonts.bold, marginTop: vw(16), marginBottom: vw(16) }}>{`Live video sessions`}</Text>
                            <View style={noVideoSession}>
                                <View style={{ borderWidth: 0, width: vw(100), justifyContent: 'center', alignItems: 'center' }}>
                                    <RegisterButton
                                        bgColor={'#2197F1'}
                                        borderColor={'#2197F1'}
                                        borderRadius={vw(56) / 2}
                                        shadowColor={false}
                                        height={vw(56)}
                                        width={vw(56)}
                                        disabled={true}
                                        buttonImageWidth={vw(110)}
                                        buttonImageHeight={vw(110)}
                                        buttonImage={film}
                                        borderWidth={3}
                                    />
                                </View>
                                <View style={{ borderWidth: 0, flex: 1, height: '100%', alignItems: 'center', justifyContent: 'center', marginRight: 16 }}>
                                    <Text style={{ fontSize: vw(18), fontFamily: Fonts.bold }} numberOfLines={0}>{`You have no live video sessions scheduled`}</Text>
                                </View>
                                <View style={{ borderWidth: 0, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255, 255, 255, 0.5)', position: 'absolute', top: 0, bottom: 0, left: 0, right: 0 }}></View>
                            </View>
                            <Button
                                text={'Book a session'}
                                bgColor={'rgba(14, 151, 72, 0.5)'}
                                borderColor={'rgba(14, 151, 72, 0.5)'}
                                disabled={false}
                                textColor={Colors.whiteFour}
                                fontFamily={Fonts.semibold}
                                fontSize={vw(16)}
                                height={vw(45)}
                                borderRadius={vw(5)}
                                onPress={() => this.props.navigation.navigate('ChatDetail')}
                            />
                        </View>
                        <View>
                            <Text style={{ fontSize: vw(22), fontFamily: Fonts.bold, marginTop: vw(26), marginBottom: vw(6), marginLeft: vw(20) }}>{`Tools`}</Text>
                            <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#CECECE', height: vw(44), paddingLeft: vw(20) }} onPress={() => this.props.navigation.navigate('StarredMessages')}>
                                <Image
                                    source={star_green}
                                    resizeMode={'contain'}
                                    style={{ marginRight: vw(12), borderWidth: 0 }}
                                />
                                <Text style={{ fontSize: vw(16), fontFamily: Fonts.semibold, flex: 1 }}>{`Starred messages`}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AgentBookSession)