import { StyleSheet } from 'react-native'
import Colors from '../../common/Colors'
import { vw } from '../../common/ViewportUnits'

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    innerPageContainer: {
        borderWidth: 0,
        borderColor: 'red'
    },
    noVideoSession: {
        backgroundColor: Colors.whiteFour, 
        shadowColor: Colors.shadowColor, 
        shadowOffset: { 
            width: 0, 
            height: vw(6)
        }, 
        shadowOpacity: 1, 
        elevation: 10, 
        shadowRadius: vw(5), 
        borderRadius: vw(5),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: vw(100),
        marginBottom: vw(16)
    }
})

// make this component available to the app
export default styles