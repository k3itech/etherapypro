//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import RegisterButton from '../../Components/RegisterButton'
import HeaderNew from '../../Components/HeaderNew'
import licence from '../../assets/img/licence.png'
import agentSupport from '../../assets/img/agentSupport.png'

// create a component
class AgentProfile extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        const {
            main,
            innerPageContainer
        } = styles
        return (
            <View style={main}>
                <SafeAreaView></SafeAreaView>
                <View style={{ height: 80 }}>
                    <HeaderNew
                        onPressLeftIcon={() => { 
                            this.props.navigation.goBack() 
                        }}
                        isTitle={true}
                        title={''}
                        isLeftIcon={true}
                    />
                </View>
                <View style={innerPageContainer}>
                    <View style={{ marginHorizontal: 20 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                            <RegisterButton
                                bgColor={null}
                                borderColor={'#2197F1'}
                                borderRadius={vw(111) / 2}
                                shadowColor={false}
                                height={vw(111)}
                                width={vw(111)}
                                disabled={true}
                                buttonImageWidth={vw(110)}
                                buttonImageHeight={vw(110)}
                                buttonImage={''}
                                borderWidth={3}
                            />
                        </View>
                        <Text style={{ fontSize: vw(22), fontFamily: Fonts.bold, textAlign: 'center', marginTop: vw(2), marginBottom: vw(6) }}>{`Tania Anderson`}</Text>
                        <View style={{ borderWidth: 0, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ backgroundColor: 'rgba(14, 151, 72, .5)', justifyContent: 'center', alignItems: 'center', marginRight: vw(6), width: vw(22), height: vw(22), borderRadius: vw(22) / 2 }}>
                                <Image
                                    source={agentSupport}
                                    resizeMode={'contain'}
                                    style={{}}
                                />
                            </View>
                            <Text style={{ fontSize: vw(16), fontFamily: Fonts.regular }}>{`Matching agent`}</Text>
                        </View>
                        <Text style={{ fontSize: vw(16), fontFamily: Fonts.semibold, textAlign: 'center', marginTop: vw(12), color: 'rgba(0, 0, 0, 0.7)' }}>{'Lorem ipsum dolor sit amet, labore\nconsectetur adipiscing elit, sed do et\neiusmod tempo ncididunt ut\nlabore dolore. Lorem ipsum dolor sit\namet, labore consectetur adipiscing\nelit, sed do et eiusmod tempo\nncididunt ut labore dolore.'}</Text>
                        <View style={{ borderTopWidth: 1, marginTop: vw(75), borderTopColor: 'rgba(0, 0, 0, 0.4)' }}>
                            <Text style={{ fontSize: vw(22), fontFamily: Fonts.bold, marginTop: vw(16), marginBottom: vw(16), textTransform: 'uppercase' }}>{`licenses`}</Text>
                            <View style={{ borderWidth: 0, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Image
                                    source={licence}
                                    resizeMode={'contain'}
                                    style={{ marginRight: vw(12), borderWidth: 0 }}
                                />
                                <Text style={{ fontSize: vw(16), fontFamily: Fonts.regular, flex: 1 }}>{`Licensed Marriage and Family Therapist LMFT 42039 CA`}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AgentProfile)