import React, { Component } from 'react'
import {
    View,
    Text,
    Platform,
    StyleSheet,
    TouchableOpacity,
    SafeAreaView
} from 'react-native'
import AudioRecorderPlayer from 'react-native-audio-recorder-player'
import { connect } from 'react-redux'
import HeaderNew from '../../Components/HeaderNew'
import Colors from '../../common/Colors'
import OverlayLoading from '../../Components/OverlayLoading'
import {
    ratio,
    screenWidth
} from '../../Utility/styles'
import Button from '../AudioRecorderContainer/Button'
import Fonts from '../../common/Fonts'

const DISABLED_COLOR = 'rgba(255, 255, 255, 0.4)'
const ENABLED_COLOR = 'rgba(255, 255, 255, 1.0)'

audioRecorderPlayer: AudioRecorderPlayer

class AudioPlayerContainer extends Component {

    constructor(props) {
        super(props)

        this.audioRecorderPlayer = new AudioRecorderPlayer()
        this.audioRecorderPlayer.setSubscriptionDuration(0.1)

        this.state = {
            currentPositionSec: 0,
            currentDurationSec: this.props.route.params.durationSeconds,
            playTime: '00:00:00',
            duration: this.audioRecorderPlayer.mmssss(Math.floor(this.props.route.params.durationSeconds)),
            isPlaying: false,
            isPaused: false,
            audioUrl: this.props.route.params.audioUrl,
            visible: false
        }
    }

    //onStatusPress = (e: any) => {
    onStatusPress = (e) => {
        const touchX = e.nativeEvent.locationX

        const playWidth = (this.state.currentPositionSec / this.state.currentDurationSec) * (screenWidth - 56 * ratio)

        const currentPosition = Math.round(this.state.currentPositionSec)

        if (playWidth && playWidth < touchX) {
            const addSecs = Math.round(currentPosition + 3000)
            this.audioRecorderPlayer.seekToPlayer(addSecs)
        }
        else {
            const subSecs = Math.round(currentPosition - 3000)
            this.audioRecorderPlayer.seekToPlayer(subSecs)
        }
    }

    onStartPlay = async () => {
        console.log('this.state.audioUrl = ', this.state.audioUrl)

        if (this.state.visible == false) {
            this.setState({
                visible: true
            })
        }
        this.audioRecorderPlayer.startPlayer(this.state.audioUrl).then(() => {
            console.log('play')
            if (this.state.visible == true) {
                this.setState({
                    visible: false
                })
            }
        }).catch(error => {
            console.log('error = ', error)
            if (this.state.visible == true) {
                this.setState({
                    visible: false
                })
            }
            return
        })

        this.audioRecorderPlayer.setVolume(1.0)

        this.audioRecorderPlayer.addPlayBackListener((e) => {
            this.setState({
                currentPositionSec: e.current_position,
                currentDurationSec: e.duration,
                playTime: this.audioRecorderPlayer.mmssss(
                    Math.floor(e.current_position)
                ),
                duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
                isPlaying: true
            })
            if (e.current_position === e.duration) {
                console.log('finished')
                this.audioRecorderPlayer.stopPlayer()
                this.audioRecorderPlayer.removePlayBackListener()
                this.setState({
                    isPaused: false,
                    isPlaying: false
                })
            }
        })
    }

    onPausePlay = async () => {
        await this.audioRecorderPlayer.pausePlayer()
        this.setState({
            isPlaying: !this.state.isPlaying,
            isPaused: !this.state.isPaused
        })
    }

    onStopPlay = async () => {
        console.log('onStopPlay')
        this.audioRecorderPlayer.stopPlayer()
        this.audioRecorderPlayer.removePlayBackListener()
        this.setState({
            isPaused: false,
            isPlaying: false
        })
    }

    render() {
        let playWidth = (this.state.currentPositionSec / this.state.currentDurationSec) * (screenWidth - 56 * ratio)

        if (!playWidth) {
            playWidth = 0
        }

        return (
            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }} >
                <OverlayLoading
                    visible={this.state.visible}
                    color={Colors.whiteFour}
                />
                <View style={styles.main}>
                    <SafeAreaView></SafeAreaView>
                    <View style={{ height: 80 }}>
                        <HeaderNew
                            onPressLeftIcon={() => {
                                this.props.navigation.goBack()
                            }}
                            isTitle={true}
                            title={'Audio Player'}
                            isLeftIcon={true}
                        />
                    </View>
                    <View style={styles.container}>
                        <Text style={styles.titleTxt}>{'Audio Player'}</Text>
                        <View style={styles.viewPlayer}>
                            <TouchableOpacity
                                style={styles.viewBarWrapper}
                                onPress={this.onStatusPress}
                                disabled={true}
                            >
                                <View style={styles.viewBar}>
                                    <View style={[styles.viewBarPlay, { width: playWidth }]} />
                                </View>
                            </TouchableOpacity>
                            <Text style={styles.txtCounter}>
                                {this.state.playTime} / {this.state.duration}
                            </Text>
                            <View style={styles.playBtnWrapper}>
                                <Button
                                    style={{
                                        ...styles.btn,
                                        borderColor: this.state.isPlaying ? DISABLED_COLOR : ENABLED_COLOR
                                    }}
                                    onPress={this.onStartPlay}
                                    textStyle={{ ...styles.txt, color: this.state.isPlaying ? DISABLED_COLOR : ENABLED_COLOR }}
                                    disabled={this.state.isPlaying ? true : false}
                                >
                                    {'PLAY'}
                                </Button>
                                <Button
                                    style={[
                                        styles.btn, {
                                            marginLeft: 12 * ratio,
                                            borderColor: this.state.playTime == '00:00:00' ? DISABLED_COLOR : this.state.isPlaying ? ENABLED_COLOR : DISABLED_COLOR
                                        }
                                    ]}
                                    onPress={this.onPausePlay}
                                    textStyle={{
                                        ...styles.txt,
                                        color: this.state.playTime == '00:00:00' ? DISABLED_COLOR : this.state.isPlaying ? ENABLED_COLOR : DISABLED_COLOR
                                    }}
                                    disabled={this.state.playTime == '00:00:00' ? true : this.state.isPlaying ? false : true}
                                >
                                    {'PAUSE'}
                                </Button>
                                <Button
                                    style={[
                                        styles.btn, {
                                            marginLeft: 12 * ratio,
                                            borderColor: this.state.isPlaying ? ENABLED_COLOR : DISABLED_COLOR
                                        }
                                    ]}
                                    onPress={this.onStopPlay}
                                    textStyle={{
                                        ...styles.txt,
                                        color: this.state.isPlaying ? ENABLED_COLOR : this.state.isPaused ? ENABLED_COLOR : DISABLED_COLOR
                                    }}
                                    disabled={this.state.isPlaying ? false : this.state.isPaused ? false : true}
                                >
                                    {'STOP'}
                                </Button>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#455A64',
        flexDirection: 'column',
        alignItems: 'center'
    },
    main: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    content: {
        alignItems: 'center'
    },
    titleTxt: {
        marginTop: 100 * ratio,
        color: 'white',
        fontSize: 28 * ratio
    },
    viewRecorder: {
        marginTop: 40 * ratio,
        width: '100%',
        alignItems: 'center'
    },
    recordBtnWrapper: {
        flexDirection: 'row'
    },
    viewPlayer: {
        marginTop: 60 * ratio,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    viewBarWrapper: {
        marginTop: 28 * ratio,
        marginHorizontal: 28 * ratio,
        alignSelf: 'stretch'
    },
    viewBar: {
        backgroundColor: '#ccc',
        height: 4 * ratio,
        alignSelf: 'stretch'
    },
    viewBarPlay: {
        backgroundColor: 'white',
        height: 4 * ratio,
        width: 0
    },
    playStatusTxt: {
        marginTop: 8 * ratio,
        color: '#ccc'
    },
    playBtnWrapper: {
        flexDirection: 'row',
        marginTop: 40 * ratio
    },
    btn: {
        borderWidth: 1 * ratio
    },
    txt: {
        fontSize: 14 * ratio,
        marginHorizontal: 8 * ratio,
        marginVertical: 4 * ratio
    },
    txtRecordCounter: {
        marginTop: 32 * ratio,
        color: 'white',
        fontSize: 20 * ratio,
        textAlignVertical: 'center',
        fontWeight: '200',
        fontFamily: Fonts.regular,
        letterSpacing: 3
    },
    txtCounter: {
        marginTop: 12 * ratio,
        color: 'white',
        fontSize: 20 * ratio,
        textAlignVertical: 'center',
        fontWeight: '200',
        fontFamily: Fonts.regular,
        letterSpacing: 3
    }
});

// get state/props from redux state(Reducers)
const mapStateToProps = state => {
    return {
        userData: state.auth.userData
    }
}

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AudioPlayerContainer)