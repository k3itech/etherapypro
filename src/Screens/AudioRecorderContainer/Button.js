import React, { Component } from 'react'
import {
  Image,
  Text,
  TouchableOpacity,
  View
} from 'react-native'

class Button extends Component {
  render() {
    return (
      <TouchableOpacity
        activeOpacity={this.props.activeOpacity}
        onPress={this.props.onPress}
        disabled={this.props.disabled}
      >
        <View style={this.props.style}>
          {
            this.props.imgLeftSrc
              ?
              (
                <Image
                  style={this.props.imgLeftStyle}
                  source={this.props.imgLeftSrc}
                />
              )
              :
              null
          }
          <Text style={this.props.textStyle}>{this.props.children}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

export default Button