import React, { Component } from 'react'
import {
    View,
    Text,
    PermissionsAndroid,
    Platform,
    StyleSheet,
    TouchableOpacity,
    SafeAreaView
} from 'react-native'
import AudioRecorderPlayer, {
    AVEncoderAudioQualityIOSType,
    AVEncodingOption,
    AudioEncoderAndroidType,
    AudioSourceAndroidType
} from 'react-native-audio-recorder-player'
import RNFetchBlob from 'rn-fetch-blob'
import { connect } from 'react-redux'
import HeaderNew from '../../Components/HeaderNew'
import {
    ratio,
    screenWidth
} from '../../Utility/styles'
import Button from './Button'
import Fonts from '../../common/Fonts'
import { AUDIO_MESSAGE_TYPE } from '../../Utility/Constants'

const DISABLED_COLOR = 'rgba(255, 255, 255, 0.4)'
const ENABLED_COLOR = 'rgba(255, 255, 255, 1.0)'

audioRecorderPlayer: AudioRecorderPlayer

class AudioRecorderContainer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            recordSecs: 0,
            recordTime: '00:00:00',
            currentPositionSec: 0,
            currentDurationSec: 0,
            playTime: '00:00:00',
            duration: '00:00:00',
            isRecording: false,
            isPlaying: false,
            isPaused: false,
            audioUrl: undefined,
            audioName: undefined,
            audioPath: undefined
        }

        this.audioRecorderPlayer = new AudioRecorderPlayer()
        this.audioRecorderPlayer.setSubscriptionDuration(0.1)
    }

    //onStatusPress = (e: any) => {
    onStatusPress = (e) => {
        const touchX = e.nativeEvent.locationX

        const playWidth = (this.state.currentPositionSec / this.state.currentDurationSec) * (screenWidth - 56 * ratio)

        const currentPosition = Math.round(this.state.currentPositionSec)

        if (playWidth && playWidth < touchX) {
            const addSecs = Math.round(currentPosition + 3000)
            this.audioRecorderPlayer.seekToPlayer(addSecs)
        }
        else {
            const subSecs = Math.round(currentPosition - 3000)
            this.audioRecorderPlayer.seekToPlayer(subSecs)
        }
    }

    onStartRecord = async () => {
        if (Platform.OS == 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'Permissions for write access',
                        message: 'Give permission to your storage to write a file',
                        buttonPositive: 'ok'
                    }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log('You can use the storage')
                }
                else {
                    console.log('permission denied')
                    return
                }
            }
            catch (err) {
                console.warn(err)
                return
            }
        }
        if (Platform.OS == 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                    {
                        title: 'Permissions for read access',
                        message: 'Give permission to your storage to read a file',
                        buttonPositive: 'ok'
                    }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log('You can read the storage')
                }
                else {
                    console.log('permission denied')
                    return
                }
            }
            catch (err) {
                console.warn(err)
                return
            }
        }
        if (Platform.OS == 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                    {
                        title: 'Permissions for write access',
                        message: 'Give permission to your storage to write a file',
                        buttonPositive: 'ok'
                    }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log('You can use the camera')
                }
                else {
                    console.log('permission denied')
                    return
                }
            }
            catch (err) {
                console.warn(err)
                return
            }
        }
        this.setState({
            recordSecs: 0,
            isRecording: false
        })
        let timeStamp = Math.floor(Date.now() / 1000)
        let path = ''
        let name = ''
        if (Platform.OS == 'ios') {
            path = `hello_${timeStamp}.m4a`
            name = `${timeStamp}.m4a`
        }
        else {
            let dirs = RNFetchBlob.fs.dirs
            console.log('dirs = ', dirs)
            path = `${dirs.DownloadDir}/hello_${timeStamp}.mp4`
            name = `${timeStamp}.mp4`
        }
        this.setState({
            audioName: name,
            audioPath: path
        })
        const audioSet = {
            AudioEncoderAndroid: AudioEncoderAndroidType.AAC,
            AudioSourceAndroid: AudioSourceAndroidType.MIC,
            AVEncoderAudioQualityKeyIOS: AVEncoderAudioQualityIOSType.high,
            AVNumberOfChannelsKeyIOS: 2,
            AVFormatIDKeyIOS: AVEncodingOption.aac,
        }
        const uri = await this.audioRecorderPlayer.startRecorder(path, false, audioSet);
        this.audioRecorderPlayer.addRecordBackListener((e) => {
            this.setState({
                recordSecs: e.current_position,
                recordTime: this.audioRecorderPlayer.mmssss(
                    Math.floor(e.current_position),
                ),
                isRecording: true
            })
        })
        console.log(`uri onStartRecord : ${uri}`)
        this.setState({
            audioUrl: uri
        })
    }

    onStopRecord = async () => {
        this.setState({
            isRecording: false
        })
        const result = await this.audioRecorderPlayer.stopRecorder()
        this.audioRecorderPlayer.removeRecordBackListener()
        console.log('onStopRecord result = ', result)
    };

    onStartPlay = async () => {
        await this.audioRecorderPlayer.startPlayer(this.state.audioPath)
        this.audioRecorderPlayer.setVolume(1.0)
        this.audioRecorderPlayer.addPlayBackListener((e) => {
            this.setState({
                currentPositionSec: e.current_position,
                currentDurationSec: e.duration,
                playTime: this.audioRecorderPlayer.mmssss(
                    Math.floor(e.current_position)
                ),
                duration: this.audioRecorderPlayer.mmssss(Math.floor(e.duration)),
                isPlaying: true
            })
            if (e.current_position === e.duration) {
                console.log('finished')
                this.audioRecorderPlayer.stopPlayer()
                this.audioRecorderPlayer.removePlayBackListener()
                this.setState({
                    isPaused: false,
                    isPlaying: false
                })
            }
        })
    }

    onPausePlay = async () => {
        await this.audioRecorderPlayer.pausePlayer()
        this.setState({
            isPlaying: !this.state.isPlaying,
            isPaused: !this.state.isPaused
        })
    }

    onStopPlay = async () => {
        console.log('onStopPlay')
        this.audioRecorderPlayer.stopPlayer()
        this.audioRecorderPlayer.removePlayBackListener()
        this.setState({
            isPaused: false,
            isPlaying: false
        })
        console.log('currentPositionSec = ', this.state.currentPositionSec)
        console.log('currentDurationSec = ', this.state.currentDurationSec)
    }

    onUpload = async () => {
        this.audioRecorderPlayer.stopPlayer()
        this.audioRecorderPlayer.removePlayBackListener()
        this.setState({
            isPaused: false,
            isPlaying: false
        })
        this.uploadAudioOnServer()
    }

    uploadAudioOnServer = () => {
        let audioName = this.state.audioName

        console.log('audioName = ', audioName)
        console.log('this.state.recordSecs = ', this.state.recordSecs)
        console.log('this.state.recordTime = ', this.state.recordTime)
        console.log('this.state.audioUrl = ', this.state.audioUrl)

        let chatData = {
            message: this.state.audioUrl,
            message_type: AUDIO_MESSAGE_TYPE,
            date: new Date(),
            counsellor_id: '5f7e9c3022b3035778405266',
            counsellorname: "damonmatt",
            createdAt: new Date(),
            id: '5f7e9c3022b3035778405266',
            joinId: 'r12467544ut4',
            time: new Date(),
            updatedAt: new Date(),
            user_id: '5f8db366624d4f308dd10850',
            username: 'sp28',
            _id: new Date(),
            meta_data: {
                message_length: `${this.state.recordSecs}`
            }
        }

        this.props.route.params.getBackFromAudioRecorder({
            data: chatData
        })

        this.props.navigation.goBack()
    }

    render() {

        let playWidth = (this.state.currentPositionSec / this.state.currentDurationSec) * (screenWidth - 56 * ratio)

        if (!playWidth) {
            playWidth = 0
        }

        return (
            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }} >
                <View style={styles.main}>
                    <SafeAreaView></SafeAreaView>
                    <View style={{ height: 80 }}>
                        <HeaderNew
                            onPressLeftIcon={() => {
                                this.props.navigation.goBack()
                            }}
                            isTitle={true}
                            title={'Audio Recorder'}
                            isLeftIcon={true}
                        />
                    </View>
                    <View style={styles.container}>
                        <Text style={styles.titleTxt}>{'Audio Recorder Player'}</Text>
                        <Text style={styles.txtRecordCounter}>{this.state.recordTime}</Text>
                        <View style={styles.viewRecorder}>
                            <View style={styles.recordBtnWrapper}>
                                <Button
                                    style={{
                                        ...styles.btn,
                                        borderColor: this.state.isRecording ? DISABLED_COLOR : this.state.isPlaying ? DISABLED_COLOR : ENABLED_COLOR
                                    }}
                                    onPress={this.onStartRecord}
                                    textStyle={{
                                        ...styles.txt,
                                        color: this.state.isRecording ? DISABLED_COLOR : this.state.isPlaying ? DISABLED_COLOR : ENABLED_COLOR
                                    }}
                                    disabled={this.state.isRecording ? true : this.state.isPlaying ? true : false}
                                >
                                    {'RECORD'}
                                </Button>
                                <Button
                                    style={[
                                        styles.btn, {
                                            marginLeft: 12 * ratio,
                                            borderColor: this.state.isRecording ? ENABLED_COLOR : DISABLED_COLOR
                                        },
                                    ]}
                                    onPress={this.onStopRecord}
                                    disabled={this.state.isRecording ? false : true}
                                    textStyle={{
                                        ...styles.txt,
                                        color: this.state.isRecording ? ENABLED_COLOR : DISABLED_COLOR
                                    }}
                                >
                                    {'STOP'}
                                </Button>
                            </View>
                        </View>
                        <View style={styles.viewPlayer}>
                            <TouchableOpacity
                                style={styles.viewBarWrapper}
                                onPress={this.onStatusPress}
                                disabled={true}
                            >
                                <View style={styles.viewBar}>
                                    <View style={[styles.viewBarPlay, { width: playWidth }]} />
                                </View>
                            </TouchableOpacity>
                            <Text style={styles.txtCounter}>
                                {this.state.playTime} / {this.state.duration}
                            </Text>
                            <View style={styles.playBtnWrapper}>
                                <Button
                                    style={{
                                        ...styles.btn,
                                        borderColor: this.state.recordTime == '00:00:00' ? DISABLED_COLOR : this.state.isRecording ? DISABLED_COLOR : this.state.isPlaying ? DISABLED_COLOR : ENABLED_COLOR
                                    }}
                                    onPress={this.onStartPlay}
                                    textStyle={{
                                        ...styles.txt,
                                        color: this.state.recordTime == '00:00:00' ? DISABLED_COLOR : this.state.isRecording ? DISABLED_COLOR : this.state.isPlaying ? DISABLED_COLOR : ENABLED_COLOR
                                    }}
                                    disabled={this.state.recordTime == '00:00:00' ? true : this.state.isRecording ? true : this.state.isPlaying ? true : false}
                                >
                                    {'PLAY'}
                                </Button>
                                <Button
                                    style={[
                                        styles.btn, {
                                            marginLeft: 12 * ratio,
                                            borderColor: this.state.recordTime == '00:00:00' ? DISABLED_COLOR : this.state.isRecording ? DISABLED_COLOR : this.state.playTime == '00:00:00' ? DISABLED_COLOR : this.state.isPlaying ? ENABLED_COLOR : DISABLED_COLOR
                                        }
                                    ]}
                                    onPress={this.onPausePlay}
                                    textStyle={{
                                        ...styles.txt,
                                        color: this.state.recordTime == '00:00:00' ? DISABLED_COLOR : this.state.isRecording ? DISABLED_COLOR : this.state.playTime == '00:00:00' ? DISABLED_COLOR : this.state.isPlaying ? ENABLED_COLOR : DISABLED_COLOR
                                    }}
                                    disabled={this.state.recordTime == '00:00:00' ? true : this.state.isRecording ? true : this.state.playTime == '00:00:00' ? true : this.state.isPlaying ? false : true}
                                >
                                    {'PAUSE'}
                                </Button>
                                <Button
                                    style={[
                                        styles.btn, {
                                            marginLeft: 12 * ratio,
                                            borderColor: this.state.recordTime == '00:00:00' ? DISABLED_COLOR : this.state.isRecording ? DISABLED_COLOR : this.state.isPlaying ? ENABLED_COLOR : DISABLED_COLOR
                                        }
                                    ]}
                                    onPress={this.onStopPlay}
                                    textStyle={{
                                        ...styles.txt,
                                        color: this.state.recordTime == '00:00:00' ? DISABLED_COLOR : this.state.isRecording ? DISABLED_COLOR : this.state.isPlaying ? ENABLED_COLOR : this.state.isPaused ? ENABLED_COLOR : DISABLED_COLOR
                                    }}
                                    disabled={this.state.recordTime == '00:00:00' ? true : this.state.isRecording ? true : this.state.isPlaying ? false : this.state.isPaused ? false : true}
                                >
                                    {'STOP'}
                                </Button>
                            </View>
                        </View>
                        <Button
                            style={{
                                ...styles.btn,
                                borderColor: this.state.recordTime == '00:00:00' ? DISABLED_COLOR : this.state.isRecording ? DISABLED_COLOR : ENABLED_COLOR, marginTop: 40
                            }}
                            onPress={this.onUpload}
                            textStyle={{
                                ...styles.txt,
                                color: this.state.recordTime == '00:00:00' ? DISABLED_COLOR : this.state.isRecording ? DISABLED_COLOR : ENABLED_COLOR
                            }}
                            disabled={this.state.recordTime == '00:00:00' ? true : this.state.isRecording ? true : false}
                        >
                            {'Upload'}
                        </Button>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#455A64',
        flexDirection: 'column',
        alignItems: 'center'
    },
    main: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    content: {
        alignItems: 'center'
    },
    titleTxt: {
        marginTop: 100 * ratio,
        color: 'white',
        fontSize: 28 * ratio
    },
    viewRecorder: {
        marginTop: 40 * ratio,
        width: '100%',
        alignItems: 'center'
    },
    recordBtnWrapper: {
        flexDirection: 'row'
    },
    viewPlayer: {
        marginTop: 60 * ratio,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    viewBarWrapper: {
        marginTop: 28 * ratio,
        marginHorizontal: 28 * ratio,
        alignSelf: 'stretch'
    },
    viewBar: {
        backgroundColor: '#ccc',
        height: 4 * ratio,
        alignSelf: 'stretch'
    },
    viewBarPlay: {
        backgroundColor: 'white',
        height: 4 * ratio,
        width: 0
    },
    playStatusTxt: {
        marginTop: 8 * ratio,
        color: '#ccc'
    },
    playBtnWrapper: {
        flexDirection: 'row',
        marginTop: 40 * ratio
    },
    btn: {
        borderWidth: 1 * ratio
    },
    txt: {
        fontSize: 14 * ratio,
        marginHorizontal: 8 * ratio,
        marginVertical: 4 * ratio
    },
    txtRecordCounter: {
        marginTop: 32 * ratio,
        color: 'white',
        fontSize: 20 * ratio,
        textAlignVertical: 'center',
        fontWeight: '200',
        fontFamily: Fonts.regular,
        letterSpacing: 3
    },
    txtCounter: {
        marginTop: 12 * ratio,
        color: 'white',
        fontSize: 20 * ratio,
        textAlignVertical: 'center',
        fontWeight: '200',
        fontFamily: Fonts.regular,
        letterSpacing: 3
    }
})

// get state/props from redux state(Reducers)
const mapStateToProps = state => {
    return {

    }
}

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AudioRecorderContainer)