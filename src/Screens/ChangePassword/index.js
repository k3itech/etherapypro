//import liraries
import React, { Component } from 'react'
import {
    View,
    Alert,
    Image,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import CardInput from '../../Components/CardInput'
import HeaderNew from '../../Components/HeaderNew'
import Button from '../../Components/Button'
import Colors from '../../common/Colors'
import OverlayLoading from '../../Components/OverlayLoading'
import {
    changePasswordAPI,
    changePasswordAPIErrorReset,
    LogoutSuccess
} from '../../Redux/Actions'
import Validation from '../../Utility/Validation'
import Functions from '../../Utility/Functions'
import { APP_TITLE } from '../../Utility/Constants'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class ChangePassword extends Component {

    constructor(props) {
        super(props)

        this.state = {
            currentPassword: '',
            newPassword: '',
            confirmPassword: ''
        }
    }

    static getDerivedStateFromProps(props, state) {
        // Any time the current user changes,
        // Reset any parts of state that are tied to that user.

        if (props.changePasswordErrorMessage != undefined) {
            setTimeout(() => {
                CustomAlert.alert(props.changePasswordErrorMessage)
                props.changePasswordAPIErrorReset()
            }, 100)
        }

        if (props.changePasswordSuccessMessage != undefined) {
            setTimeout(() => {
                Alert.alert(
                    APP_TITLE,
                    props.changePasswordSuccessMessage,
                    [
                        {
                            text: 'Ok',
                            onPress: () => {
                                props.changePasswordAPIErrorReset()
                                props.LogoutSuccess()
                                props.navigation.popToTop()
                                props.navigation.navigate('Auth', { screen: 'Welcome' })
                            }
                        }
                    ],
                    {
                        cancelable: false
                    }
                )
            }, 100)
        }

        return null
    }

    submitOnPress = () => {
        let validate = Validation.changePasswordFormValidation(this.state)
        if (validate.error == false) {
            Functions.isNetworkConnected().then(isNetwork => {
                if (isNetwork) {
                    let data = {
                        currentPassword: this.state.currentPassword,
                        newPassword: this.state.newPassword,
                        confirmPassword: this.state.confirmPassword
                    }
                    this.props.changePasswordAPI(data, this.props.userData.token)
                }
            })
        }
    }

    render() {
        const {
            main,
            innerPageContainer,
            mainViewStyle
        } = styles
        return (
            <View style={main}>
                <SafeAreaView></SafeAreaView>
                <OverlayLoading
                    visible={this.props.loading}
                />
                <View style={{ height: 80 }}>
                    <HeaderNew
                        onPressLeftIcon={() => { 
                            this.props.navigation.goBack() 
                        }}
                        isTitle={true}
                        title={'Change Password'}
                        isLeftIcon={true}
                    />
                </View>
                <View style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen, position: 'absolute', height: vw(327), top: 0, borderWidth: 0, width: '100%' }} />
                    <View style={mainViewStyle}>
                        <View style={{ height: vw(29) }} />
                        <CardInput
                            editable={true}
                            autoCapitalize={'none'}
                            placeholder='Current Password'
                            label={'Current Password*'}
                            onChangeText={(text) => {
                                this.setState({
                                    currentPassword: text
                                })
                            }}
                            isSecure={true}
                            isWhiteBg={true}
                            value={this.state.currentPassword}
                        />
                        <View style={{ height: vw(29) }} />
                        <CardInput
                            editable={true}
                            autoCapitalize={'none'}
                            placeholder='New Password'
                            label={'New Password*'}
                            onChangeText={(text) => {
                                this.setState({
                                    newPassword: text
                                })
                            }}
                            isWhiteBg={true}
                            isSecure={true}
                            value={this.state.newPassword}
                        />
                        <View style={{ height: vw(29) }} />
                        <CardInput
                            editable={true}
                            autoCapitalize={'none'}
                            placeholder='Confirm Password'
                            label={'Confirm Password*'}
                            onChangeText={(text) => {
                                this.setState({
                                    confirmPassword: text
                                })
                            }}
                            isWhiteBg={true}
                            isSecure={true}
                            value={this.state.confirmPassword}
                        />
                        <View style={{ height: vw(29) }} />
                        <Button
                            text={'Submit'}
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            textColor={Colors.black}
                            fontSize={vw(16)}
                            fontFamily={Fonts.semibold}
                            height={vw(45)}
                            borderRadius={vw(5)}
                            onPress={this.submitOnPress}
                        />
                        <View style={{ height: vw(29) }} />
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData,
    changePasswordErrorMessage: state.auth.changePasswordErrorMessage,
    changePasswordSuccessMessage: state.auth.changePasswordSuccessMessage
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    changePasswordAPI: (data, token) => {
        dispatch(changePasswordAPI(data, token))
    },
    changePasswordAPIErrorReset: () => {
        dispatch(changePasswordAPIErrorReset())
    },
    LogoutSuccess: () => {
        dispatch(LogoutSuccess())
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangePassword)