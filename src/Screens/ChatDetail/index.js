//import liraries
import React, { Component } from 'react'
import {
    View,
    FlatList,
    KeyboardAvoidingView,
    Platform,
    Text,
    SafeAreaView,
    TouchableOpacity,
    Image
} from 'react-native'
import { connect } from 'react-redux'
import ImageResizer from 'react-native-image-resizer'
import ActionSheet from 'react-native-actionsheet'
import Icon3 from 'react-native-vector-icons/Ionicons'
import styles from './style'
import HeaderChat from '../../Components/HeaderChat'
import OverlayLoading from '../../Components/OverlayLoading'
import SenderChatTextBox from '../../Components/SenderChatTextBox'
import SenderChatImageBox from '../../Components/SenderChatImageBox'
import ReceiverChatTextBox from '../../Components/ReceiverChatTextBox'
import ReceiverChatImageBox from '../../Components/ReceiverChatImageBox'
import SenderChatAudioBox from '../../Components/SenderChatAudioBox'
import ReceiverChatAudioBox from '../../Components/ReceiverChatAudioBox'
import SenderChatVideoBox from '../../Components/SenderChatVideoBox'
import ReceiverChatVideoBox from '../../Components/ReceiverChatVideoBox'
import { vw } from '../../common/ViewportUnits'
import Fonts from '../../common/Fonts'
import InputChat from '../../Components/InputChat'
import Colors from '../../common/Colors'
import Functions from '../../Utility/Functions'
import {
    TEXT_MESSAGE_TYPE,
    IMAGE_MESSAGE_TYPE,
    AUDIO_MESSAGE_TYPE,
    VIDEO_MESSAGE_TYPE
} from '../../Utility/Constants'
import { sendMessageEmit } from '../../Utility/Socket'
import {
    appendChatHistoryData,
    showChatLoader,
    hideChatLoader
} from '../../Redux/Actions'

// create a component
class ChatDetail extends Component {

    constructor(props) {
        super(props)

        this.state = {
            chatText: '',
            chatHistory: [],
            isSender: true
        }
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            Functions.disableKeyboardManager()
        })
    }

    componentWillUnmount() {
        this._unsubscribe()
    }

    renderItem = ({ item }) => {
        let isSender = false
        let user_name = ''
        if (item.id == this.props.userData.userDetails._id) {
            isSender = true
            user_name = item.username
        }
        else {
            isSender = false
            user_name = item.counsellorname
        }
        if (isSender) {
            if (item.message_type == IMAGE_MESSAGE_TYPE) {
                return (
                    <SenderChatImageBox
                        message={item.message}
                        image_data={item.meta_data}
                        date={item.createdAt}
                        user_name={user_name}
                    />
                )
            }
            else if (item.message_type == VIDEO_MESSAGE_TYPE) {
                return (
                    <SenderChatVideoBox
                        message={item.message}
                        date={item.createdAt}
                        user_name={user_name}
                    />
                )
            }
            else if (item.message_type == AUDIO_MESSAGE_TYPE) {
                return (
                    <SenderChatAudioBox
                        date={item.createdAt}
                        onPress={() => this.audioMessageOnPress(item)}
                        user_name={user_name}
                    />
                )
            }
            else {
                return (
                    <SenderChatTextBox
                        message={item.message}
                        date={item.createdAt}
                        user_name={user_name}
                    />
                )
            }
        }
        else {
            if (item.message_type == IMAGE_MESSAGE_TYPE) {
                return (
                    <ReceiverChatImageBox
                        message={item.message}
                        image_data={item.meta_data}
                        date={item.createdAt}
                        user_name={user_name}
                    />
                )
            }
            else if (item.message_type == VIDEO_MESSAGE_TYPE) {
                return (
                    <ReceiverChatVideoBox
                        message={item.message}
                        date={item.createdAt}
                        user_name={user_name}
                    />
                )
            }
            else if (item.message_type == AUDIO_MESSAGE_TYPE) {
                return (
                    <ReceiverChatAudioBox
                        date={item.createdAt}
                        onPress={() => this.audioMessageOnPress(item)}
                        user_name={user_name}
                    />
                )
            }
            else {
                return (
                    <ReceiverChatTextBox
                        message={item.message}
                        date={item.createdAt}
                        user_name={user_name}
                    />
                )
            }
        }
    }

    audioMessageOnPress = (item) => {
        console.log('item = ', item);
        this.props.navigation.navigate('AudioPlayer', {
            durationSeconds: item.meta_data.message_length,
            audioUrl: item.message
        })
    }

    micIconOnPress = () => {
        this.props.navigation.navigate('AudioRecorder', {
            getBackFromAudioRecorder: this.getBackFromAudioRecorder.bind(this),
            isSender: this.state.isSender
        })
    }

    getBackFromAudioRecorder = (data) => {
        this.setState({
            chatText: ''
        }, () => {
            console.log('here call back');
            this.props.appendChatHistoryData(data.data)
        })
    }

    smileOnPress = () => {

    }

    imageOPress = (index) => {
        if (index == 0) {
            Functions.imagePickerCamera('photo').then(({ isPicked, data }) => {
                if (isPicked) {
                    this.props.showChatLoader()
                    let rotation = 0
                    if (data.originalRotation === 90) {
                        rotation = 90
                    }
                    else if (data.originalRotation === 270) {
                        rotation = -90
                    }
                    let dataWidthHeight = Functions.getImageResizeHeight(data)
                    ImageResizer.createResizedImage(data.uri, (dataWidthHeight.width) * 2, (dataWidthHeight.height) * 2, "PNG", 70, rotation, null).then((response) => {
                        let chatData = {
                            message: response.uri,
                            message_type: IMAGE_MESSAGE_TYPE,
                            date: new Date(),
                            meta_data: {
                                width: dataWidthHeight.width * 2,
                                height: dataWidthHeight.height * 2
                            },
                            counsellor_id: '5f7e9c3022b3035778405266',
                            counsellorname: "damonmatt",
                            createdAt: new Date(),
                            id: '5f7e9c3022b3035778405266',
                            joinId: 'r12467544ut4',
                            time: new Date(),
                            updatedAt: new Date(),
                            user_id: '5f8db366624d4f308dd10850',
                            username: 'sp28',
                            _id: new Date()
                        }
                        this.setState({
                            chatText: ''
                        }, () => {
                            this.props.appendChatHistoryData(chatData)
                            this.props.showChatLoader()
                        })
                    }).catch((err) => {
                        console.log('err = ', err)
                        this.props.showChatLoader()
                    })
                }
            })
        }
        else if (index == 1) {
            Functions.imagePickerLibrary('photo').then(({ isPicked, data }) => {
                if (isPicked) {
                    this.props.showChatLoader()
                    let dataWidthHeight = Functions.getImageResizeHeight(data)
                    ImageResizer.createResizedImage(data.uri, (dataWidthHeight.width) * 2, (dataWidthHeight.height) * 2, "PNG", 70, 0, null).then((response) => {
                        let chatData = {
                            message: response.uri,
                            message_type: IMAGE_MESSAGE_TYPE,
                            date: new Date(),
                            meta_data: {
                                width: dataWidthHeight.width * 2,
                                height: dataWidthHeight.height * 2
                            },
                            counsellor_id: '5f7e9c3022b3035778405266',
                            counsellorname: "damonmatt",
                            createdAt: new Date(),
                            id: '5f7e9c3022b3035778405266',
                            joinId: 'r12467544ut4',
                            time: new Date(),
                            updatedAt: new Date(),
                            user_id: '5f8db366624d4f308dd10850',
                            username: 'sp28',
                            _id: new Date()
                        }
                        this.setState({
                            chatText: ''
                        }, () => {
                            this.props.appendChatHistoryData(chatData)
                            this.props.hideChatLoader()
                        })
                    }).catch((err) => {
                        console.log('err = ', err)
                        this.props.hideChatLoader()
                    })
                }
            })
        }
        else if (index == 2) {
            Functions.imagePickerCamera('video').then(({ isPicked, data }) => {
                if (isPicked) {
                    this.props.showChatLoader()
                    let chatData = {
                        message: data.uri,
                        message_type: VIDEO_MESSAGE_TYPE,
                        date: new Date(),
                        counsellor_id: '5f7e9c3022b3035778405266',
                        counsellorname: "damonmatt",
                        createdAt: new Date(),
                        id: '5f7e9c3022b3035778405266',
                        joinId: 'r12467544ut4',
                        time: new Date(),
                        updatedAt: new Date(),
                        user_id: '5f8db366624d4f308dd10850',
                        username: 'sp28',
                        _id: new Date()
                    }
                    this.setState({
                        chatText: ''
                    }, () => {
                        this.props.appendChatHistoryData(chatData)
                        this.props.hideChatLoader()
                    })
                }
            })
        }
        else if (index == 3) {
            Functions.imagePickerLibrary('video').then(({ isPicked, data }) => {
                if (isPicked) {
                    this.props.showChatLoader()
                    let chatData = {
                        message: data.uri,
                        message_type: VIDEO_MESSAGE_TYPE,
                        date: new Date(),
                        counsellor_id: '5f7e9c3022b3035778405266',
                        counsellorname: "damonmatt",
                        createdAt: new Date(),
                        id: '5f7e9c3022b3035778405266',
                        joinId: 'r12467544ut4',
                        time: new Date(),
                        updatedAt: new Date(),
                        user_id: '5f8db366624d4f308dd10850',
                        username: 'sp28',
                        _id: new Date()
                    }
                    this.setState({
                        chatText: ''
                    }, () => {
                        this.props.appendChatHistoryData(chatData)
                        this.props.hideChatLoader()
                    })
                }
            })
        }
    }

    cameraOnPress = () => {
        this.ActionSheet.show()
    }

    sendOnPress = () => {
        let chatText = this.state.chatText != null ? String(this.state.chatText).trim() : this.state.chatText
        Functions.isNetworkConnected().then(isNetwork => {
            if (isNetwork) {
                if (chatText != '') {
                    sendMessageEmit('5f7e9c3022b3035778405266', 'damonmatt', chatText, TEXT_MESSAGE_TYPE)
                    this.setState({
                        chatText: ''
                    })
                }
                else {
                    this.setState({
                        chatText: ''
                    })
                }
            }
            else {
                this.setState({
                    chatText: ''
                })
            }
        })
    }

    renderChatBottomBar = () => {
        return (
            this.state.chatText == ''
                ?
                (
                    <View style={styles.chatBottomBar}>
                        <View style={styles.rowStyle}>
                            <View style={styles.colSixtyStyle}>
                                <InputChat
                                    placeholder={'Type your message'}
                                    cameraOnPress={this.smileOnPress}
                                    onChangeText={(text) => {
                                        this.setState({
                                            chatText: text
                                        })
                                    }}
                                    value={this.state.chatText}
                                />
                            </View>
                            <View style={styles.colFourtyStyle}>
                                <View style={styles.iconsBlockStyle}>
                                    <TouchableOpacity onPress={this.cameraOnPress} disabled={false} style={{ backgroundColor: '#007EF4', borderRadius: 22 }}>
                                        <Image
                                            style={styles.micStyle}
                                            resizeMode='center'
                                            source={require('../../assets/img/camera.png')}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={this.micIconOnPress} disabled={false} style={{ backgroundColor: '#007EF4', borderRadius: 22 }}>
                                        <Image
                                            style={styles.micStyle}
                                            resizeMode='center'
                                            source={require('../../assets/img/mic.png')}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                )
                :
                (
                    <View style={styles.chatBottomBar}>
                        <View style={styles.rowStyle}>
                            <View style={styles.colNintyFiveStyle}>
                                <InputChat
                                    placeholder={'Type your message'}
                                    cameraOnPress={this.smileOnPress}
                                    onChangeText={(text) => {
                                        this.setState({
                                            chatText: text
                                        })
                                    }}
                                    value={this.state.chatText}
                                />
                            </View>
                            <View style={styles.colFifteenStyle}>
                                <View style={styles.iconsBlockStyle}>
                                    <TouchableOpacity onPress={this.sendOnPress} style={{ ...styles.micStyle, backgroundColor: '#007EF4', borderRadius: 22, justifyContent: 'center', alignItems: 'center' }} disabled={false}>
                                        <Icon3
                                            name='ios-send'
                                            color={Colors.whiteFour}
                                            size={vw(22)}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                )
        )
    }

    render() {
        const {
            main,
            container
        } = styles
        return (
            <KeyboardAvoidingView style={container} behavior={Platform.OS == 'ios' ? 'padding' : null}>
                <OverlayLoading
                    visible={this.props.loading}
                />
                <View style={main}>
                    <HeaderChat
                        onPressLeftIcon={() => {
                            Functions.enableKeyboardManager()
                            this.props.navigation.goBack()
                        }}
                        onPressRightIcon={() => {

                        }}
                    />
                    <ActionSheet
                        ref={o => this.ActionSheet = o}
                        title={'Select Media'}
                        options={['Take Photo', 'Choose Photo from Lbrary', 'Record Video', 'Choose Video From Library', 'Cancel']}
                        cancelButtonIndex={4}
                        onPress={(index) => {
                            this.imageOPress(index)
                        }}
                    />
                    <View style={{ flex: 1, marginTop: vw(16), paddingTop: 0, marginHorizontal: vw(16) }}>
                        <FlatList
                            style={{
                                borderWidth: 0
                            }}
                            ListFooterComponent={
                                <View style={{ width: '100%', flexDirection: 'column', alignItems: 'center' }}>
                                    <Text style={{ color: 'rgba(0, 0, 0, 0.7)', fontSize: vw(22), fontFamily: Fonts.bold, textAlign: 'center', marginTop: vw(8), marginBottom: vw(12) }}>{`Welcome to your\nconfidential EtherapyPro`}</Text>
                                    <Text style={{ color: 'rgba(0, 0, 0, 0.6)', fontSize: vw(16), fontFamily: Fonts.semibold, marginBottom: vw(20), textAlign: 'center' }} numberOfLines={2}>{`Lorem ipsum dolor sit amet, labore\nconsectetur adipiscing elit, sed do et eiusmod\ntempo ncididunt ut labore dolore. Lorem ipsum dolor sit amet, labore consectetur adipiscing elit, sed do et eiusmod tempo ncididunt ut labore dolore.`}</Text>
                                </View>
                            }
                            data={this.props.chatHistoryData}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => `${item._id}`}
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            scrollEnabled
                            inverted
                        />
                        {this.renderChatBottomBar()}
                    </View>
                    <SafeAreaView />
                </View>
            </KeyboardAvoidingView >
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.socket.loading,
    userData: state.auth.userData,
    chatHistoryData: state.socket.chatHistoryData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    appendChatHistoryData: (data) => {
        dispatch(appendChatHistoryData(data))
    },
    showChatLoader: () => {
        dispatch(showChatLoader())
    },
    hideChatLoader: () => {
        dispatch(hideChatLoader())
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChatDetail)