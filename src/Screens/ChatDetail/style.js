import { StyleSheet } from 'react-native'
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'
import { vw } from '../../common/ViewportUnits'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    main: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    chatBottomBar: {
        backgroundColor: Colors.whiteFour,
        shadowColor: Colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: vw(6)
        },
        shadowOpacity: 1,
        elevation: 10,
        shadowRadius: vw(5),
        paddingVertical: 7.5,
        paddingHorizontal: 15,
        borderWidth: 0,
        borderRadius: 44,
        marginBottom: vw(8),
        marginTop: vw(8)
    },
    rowStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center'
    },
    colSixtyStyle: {
        width: '75%',
        paddingLeft: 10
    },
    colFourtyStyle: {
        width: '35%',
        alignContent: 'center',
        justifyContent: 'space-between'
    },
    colNintyFiveStyle: {
        width: '90%',
        paddingLeft: 10
    },
    colFifteenStyle: {
        width: '20%',
        alignContent: 'center',
        justifyContent: 'space-between'
    },
    iconsBlockStyle: {
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    micStyle: {
        width: 44,
        height: 44
    },
    sendStyle: {
        width: 22,
        height: 22
    },
    createGroupStyle: {
        position: 'relative',
        height: 50,
        flexDirection: 'row',
        width: '100%',
        paddingVertical: 12,
        borderBottomWidth: 1,
        borderBottomColor: Colors.darkGrey1,
        backgroundColor: Colors.orange
    },
    saveBtnStyle: {
        position: 'absolute',
        right: 5,
        top: 5,
        paddingHorizontal: 10,
        paddingVertical: 10,
        fontFamily: Fonts.semibold,
        fontSize: 15,
        letterSpacing: -0.25,
        color: Colors.darkGrey,
        textAlign: 'right'
    },
    createLabelStyle: {
        width: '70%',
        position: 'relative',
        paddingLeft: 60
    },
    groupIconStyle: {
        position: 'absolute',
        left: 15,
        height: 26,
        paddingHorizontal: 0,
        paddingRight: 10,
        top: 0
    },
    groupTextStyle: {
        paddingTop: 2.5,
        color: Colors.whiteFour,
        fontFamily: Fonts.bold
    }
})

// make this component available to the app
export default styles