//import liraries
import React, { Component } from 'react'
import {
    View,
    Image,
    SafeAreaView,
    Text,
    ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import styles from './style'
import HeaderNew from '../../Components/HeaderNew'
import Fonts from '../../common/Fonts'
import { vh, vw } from '../../common/ViewportUnits'
import hand from '../../assets/img/hand.png'
import Colors from '../../common/Colors'
import Button from '../../Components/Button'
import short_term from '../../assets/img/short-term.png'
import short_term_image from '../../assets/img/short-term-image.png'
import long_term_image from '../../assets/img/long-term-image.png'
import symptom_tracker from '../../assets/img/symptom-tractor.png'
import symptom_tractor_image from '../../assets/img/symptom-tractor-image.png'

// create a component
class ClinicalProcess extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        const {
            main,
            leftButtonViewStyle,
            subsctibeViewStyle
        } = styles
        return (
            <>
                <SafeAreaView></SafeAreaView>
                <View style={{ height: 80 }}>
                    <HeaderNew
                        onPressLeftIcon={() => {
                            this.props.navigation.goBack()
                        }}
                        isTitle={true}
                        title={'Clinical process'}
                        isLeftIcon={true}
                    />
                </View>
                <View style={main}>
                    <ScrollView style={{ borderWidth: 0, marginHorizontal: 20 }} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                        <Text style={{ fontFamily: Fonts.bold, fontSize: vw(22), color: Colors.black, textAlign: 'left', marginTop: vw(40), marginBottom: vw(14) }}>{`Symptom tracker`}</Text>
                        <View style={{
                            ...subsctibeViewStyle,
                            backgroundColor: '#2197F1'
                        }}>
                            <View style={{ height: vw(60), alignItems: 'center', justifyContent: 'center', width: '100%', borderWidth: 0 }}>
                                <View style={{ ...leftButtonViewStyle, backgroundColor: '#0884E2' }}>
                                    <Image
                                        source={symptom_tracker}
                                        resizeMode={'center'}
                                        style={{
                                            width: vw(19),
                                            height: vw(24),
                                        }}
                                    />
                                </View>
                            </View>
                            <Text style={{ fontFamily: Fonts.semibold, fontSize: vw(16), color: Colors.whiteFour, textAlign: 'center' }}>{`Lorem ipsum dolor sit amet, labore\nconsectetur adipiscing elit, sed do et\neiusmod tempo ncididunt ut\nlabore dolore.`}</Text>
                        </View>
                        <Text style={{ fontFamily: Fonts.bold, fontSize: vw(22), color: Colors.black, textAlign: 'left', marginTop: vw(40), marginBottom: vw(14) }}>{`Long-term goals`}</Text>
                        <View style={{
                            ...subsctibeViewStyle,
                            backgroundColor: '#0E9748'
                        }}>
                            <View style={{ height: vw(60), alignItems: 'center', justifyContent: 'center', width: '100%', borderWidth: 0 }}>
                                <View style={{ ...leftButtonViewStyle, backgroundColor: '#0B853F' }}>
                                    <Image
                                        source={short_term}
                                        resizeMode={'center'}
                                        style={{
                                            width: vw(29),
                                            height: vw(29),
                                        }}
                                    />
                                </View>
                            </View>
                            <Text style={{ fontFamily: Fonts.semibold, fontSize: vw(16), color: Colors.whiteFour, textAlign: 'center' }}>{`Lorem ipsum dolor sit amet, labore\nconsectetur adipiscing elit, sed do et\neiusmod tempo ncididunt ut\nlabore dolore.`}</Text>
                        </View>
                        <Text style={{ fontFamily: Fonts.bold, fontSize: vw(22), color: Colors.black, textAlign: 'left', marginTop: vw(40), marginBottom: vw(14) }}>{`Short-term objectives`}</Text>
                        <View style={{
                            ...subsctibeViewStyle,
                            backgroundColor: '#323232'
                        }}>
                            <View style={{ height: vw(60), alignItems: 'center', justifyContent: 'center', width: '100%', borderWidth: 0 }}>
                                <View style={{ ...leftButtonViewStyle, backgroundColor: '#242424' }}>
                                    <Image
                                        source={symptom_tracker}
                                        resizeMode={'center'}
                                        style={{
                                            width: vw(29),
                                            height: vw(29),
                                        }}
                                    />
                                </View>
                            </View>
                            <Text style={{ fontFamily: Fonts.semibold, fontSize: vw(16), color: Colors.whiteFour, textAlign: 'center' }}>{`Lorem ipsum dolor sit amet, labore\nconsectetur adipiscing elit, sed do et\neiusmod tempo ncididunt ut\nlabore dolore.`}</Text>
                        </View>
                        <SafeAreaView></SafeAreaView>
                    </ScrollView>
                </View>
            </>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ClinicalProcess)