import { StyleSheet } from 'react-native'
import Colors from '../../common/Colors'
import { vw } from '../../common/ViewportUnits'

const styles = StyleSheet.create({
    main: {
        flex: 1,
        borderWidth: 0,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    leftButtonViewStyle: {
        alignItems: 'center',
        borderWidth: 0,
        justifyContent: 'center',
        height: vw(43),
        width: vw(43),
        shadowColor: Colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: vw(6)
        },
        shadowOpacity: 1,
        elevation: 10,
        shadowRadius: vw(5),
        borderRadius: vw(43) / 2
    },
    subsctibeViewStyle: {
        shadowColor: Colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: vw(6)
        },
        shadowOpacity: 1,
        elevation: 10,
        shadowRadius: vw(5),
        borderRadius: vw(5),
        flexDirection: 'column',
        height: vw(160)
    }
})

// make this component available to the app
export default styles