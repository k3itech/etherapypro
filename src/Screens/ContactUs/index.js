//import liraries
import React, { Component } from 'react'
import {
    View,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import CardInput from '../../Components/CardInput'
import HeaderNew from '../../Components/HeaderNew'
import Button from '../../Components/Button'
import Colors from '../../common/Colors'

// create a component
class ContactUs extends Component {

    constructor(props) {
        super(props)

        this.state = {
            name: '',
            email: '',
            message: ''
        }
    }

    render() {
        const {
            main,
            innerPageContainer,
            mainViewStyle
        } = styles
        return (
            <View style={main}>
                <SafeAreaView></SafeAreaView>
                <View style={{ height: 80 }}>
                    <HeaderNew
                        onPressLeftIcon={() => {
                            this.props.navigation.goBack()
                        }}
                        isTitle={true}
                        title={'Contact us'}
                        isLeftIcon={true}
                    />
                </View>
                <View style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen, position: 'absolute', height: vw(327), top: 0, borderWidth: 0, width: '100%' }} />
                    <View style={mainViewStyle}>
                        <View style={{ height: vw(29) }} />
                        <CardInput
                            editable={true}
                            autoCapitalize={'none'}
                            placeholder=''
                            label={'Name*'}
                            onChangeText={(text) => {
                                this.setState({
                                    name: text
                                })
                            }}
                            isWhiteBg={true}
                            value={this.state.name}
                        />
                        <View style={{ height: vw(29) }} />
                        <CardInput
                            editable={true}
                            autoCapitalize={'none'}
                            placeholder=''
                            label={'E Mail*'}
                            onChangeText={(text) => {
                                this.setState({
                                    email: text
                                })
                            }}
                            isWhiteBg={true}
                            value={this.state.email}
                        />
                        <View style={{ height: vw(29) }} />
                        <CardInput
                            editable={true}
                            autoCapitalize={'none'}
                            placeholder=''
                            label={'Message*'}
                            onChangeText={(text) => {
                                this.setState({
                                    message: text
                                })
                            }}
                            isWhiteBg={true}
                            multiline={true}
                            value={this.state.message}
                        />
                        <View style={{ height: vw(29) }} />
                        <Button
                            text={'Send'}
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            textColor={Colors.black}
                            fontSize={vw(16)}
                            fontFamily={Fonts.semibold}
                            height={vw(45)}
                            borderRadius={vw(5)}
                            onPress={() => { }}
                        />
                        <View style={{ height: vw(29) }} />
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContactUs)