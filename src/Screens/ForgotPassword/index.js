//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView, 
    ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import {
    vw,
    vh
} from '../../common/ViewportUnits'
import PageContainer from '../../Components/PageContainer'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import Button from '../../Components/Button'
import Input from '../../Components/Input'
import {
    forgotPasswordAPI,
    forgotPasswordAPIErrorReset
} from '../../Redux/Actions'
import mailImage from '../../assets/img/mail.png'
import passwordImage from '../../assets/img/password.png'
import Functions from '../../Utility/Functions'
import OverlayLoading from '../../Components/OverlayLoading'
import UniveralImageBg from '../../Components/UniveralImageBg'
import Validation from '../../Utility/Validation'
import login_bg from '../../assets/img/login_bg.png'

// create a component
class ForgotPassword extends Component {

    constructor(props) {
        super(props)

        this.state = {
            name: '',
            email: '',
            password: '',
            confirmPassword: ''
        }
    }

    static getDerivedStateFromProps(props, state) {
        // Any time the current user changes,
        // Reset any parts of state that are tied to that user.

        // if (props.forgotPasswordErrorMessage != undefined) {
        //     setTimeout(() => {
        //         CustomAlert.alert(props.forgotPasswordErrorMessage)
        //         props.forgotPasswordAPIErrorReset()
        //     }, 100)
        // }

        return null
    }

    submitButtonTapped = async () => {
        let validate = Validation.forgotPasswordFormValidation(this.state)
        if (validate.error == false) {
            Functions.isNetworkConnected().then(isNetwork => {
                if (isNetwork) {
                    // let data = {
                    //     username: this.state.username
                    // }
                    // //this.props.forgotPasswordAPI(data)
                }
            })
        }
    }

    render() {
        const {
            main,
            innerPageContainer,
            bottomTextStyle
        } = styles
        return (
            <>
                <OverlayLoading
                    visible={this.props.loading}
                />
                <UniveralImageBg
                    isLogoShowing={true}
                    isTitleShowing={false}
                    height={'100%'}
                    bgImage={login_bg}
                />
                <ScrollView style={main}>
                    <PageContainer padding={vw(0)}>
                        <View style={innerPageContainer}>
                            <Text style={{ textAlign: 'center', fontSize: vw(18), fontFamily: Fonts.bold }}>{'Forgot Password'}</Text>
                            <View style={{ height: vh(12), borderWidth: 0 }} />
                            <Input
                                editable={true}
                                leftIconImageSource={mailImage}
                                autoCapitalize={'none'}
                                placeholder='Your Name'
                                isSecure={false}
                                onChangeText={(text) => {
                                    this.setState({
                                        name: text
                                    })
                                }}
                                keyboardType='default'
                                value={this.state.name}
                            />
                            <View style={{ height: vh(12), borderWidth: 0 }} />
                            <Input
                                editable={true}
                                leftIconImageSource={mailImage}
                                autoCapitalize={'none'}
                                placeholder='Your Email'
                                isSecure={false}
                                onChangeText={(text) => {
                                    this.setState({
                                        email: text
                                    })
                                }}
                                keyboardType='email-address'
                                value={this.state.email}
                            />
                            <View style={{ height: vh(12), borderWidth: 0 }} />
                            <Input
                                editable={true}
                                leftIconImageSource={passwordImage}
                                autoCapitalize={'none'}
                                placeholder='Password'
                                isSecure={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        password: text
                                    })
                                }}
                                keyboardType='default'
                                value={this.state.password}
                            />
                            <View style={{ height: vh(12), borderWidth: 0 }} />
                            <Input
                                editable={true}
                                leftIconImageSource={passwordImage}
                                autoCapitalize={'none'}
                                placeholder='Confirm Password'
                                isSecure={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        confirmPassword: text
                                    })
                                }}
                                keyboardType='default'
                                value={this.state.confirmPassword}
                            />
                            <View style={{ height: vw(40), borderWidth: 0 }} />
                            <Button
                                text={'Reset Password'}
                                bgColor={Colors.appGreen}
                                borderColor={Colors.appGreen}
                                textColor={Colors.whiteFour}
                                fontFamily={Fonts.semibold}
                                fontSize={vw(16)}
                                height={vw(45)}
                                borderRadius={vw(5)}
                                onPress={this.submitButtonTapped}
                            />
                            <View style={{ height: vw(24) }} />
                            <SafeAreaView></SafeAreaView>
                        </View>
                    </PageContainer>
                </ScrollView>
            </>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData,
    forgotPasswordErrorMessage: state.auth.forgotPasswordErrorMessage,
    forgotPasswordSuccess: state.auth.forgotPasswordSuccess
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    forgotPasswordAPI: (data) => {
        dispatch(forgotPasswordAPI(data))
    },
    forgotPasswordAPIErrorReset: () => {
        dispatch(forgotPasswordAPIErrorReset())
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ForgotPassword)