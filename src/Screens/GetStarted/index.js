//import liraries
import React, { Component } from "react"
import {
    View,
    Text,
    SafeAreaView
} from "react-native"
import { connect } from 'react-redux'
import AppIntroSlider from 'react-native-app-intro-slider'
import {
    vw,
    vh
} from "../../common/ViewportUnits"
import PageContainer from "../../Components/PageContainer"
import styles from "./style"
import Colors from "../../common/Colors"
import Button from "../../Components/Button"
import UniveralImageBg from "../../Components/UniveralImageBg"
import Fonts from "../../common/Fonts"

const slides = [
    {
        key: 'one',
        title: 'Title 1',
        text: 'Description.\nSay something cool',
        image: require('../../assets/img/welcome_bg.png'),
        backgroundColor: '#59b2ab'
    },
    {
        key: 'two',
        title: 'Title 2',
        text: 'Other cool stuff',
        image: require('../../assets/img/welcome_bg.png'),
        backgroundColor: '#febe29'
    },
    {
        key: 'three',
        title: 'Rocket guy',
        text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
        image: require('../../assets/img/welcome_bg.png'),
        backgroundColor: '#22bcb5'
    }
]

// create a component
class GetStarted extends Component {

    constructor() {
        super()

        this.state = {
            selectedIndex: 0
        }
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {

        })
    }

    componentWillUnmount() {
        this._unsubscribe()
    }

    _renderItem = ({ item }) => {
        return (
            <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                <UniveralImageBg
                    isLogoShowing={true}
                    isTitleShowing={false}
                    height={vw(700)}
                    bgImage={item.image}
                />
            </View>
        )
    }

    _onDone = () => {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
    }

    render() {
        const {
            main,
            innerPageContainer,
            dotStyle,
            activeDotStyle,
            bottomView,
            bottomTextStyle
        } = styles
        return (
            <>
                <View style={main}>
                    <PageContainer padding={vw(0)}>
                        <View style={innerPageContainer}>
                            <AppIntroSlider
                                showNextButton={false}
                                showDoneButton={false}
                                renderItem={this._renderItem}
                                data={slides}
                                onDone={this._onDone}
                                dotStyle={dotStyle}
                                activeDotStyle={activeDotStyle}
                                onSlideChange={(index, lastIndex) => {
                                    this.setState({
                                        selectedIndex: index
                                    })
                                }}
                            />
                        </View>
                        <View style={bottomView}>
                            <Text style={{ textAlign: 'center', fontFamily: Fonts.semibold, fontSize: vw(22), color: 'rgba(0, 0, 0, 1.0)' }}>{this.state.selectedIndex == 0 ? 'Invest in yourself' : this.state.selectedIndex == 1 ? 'Tool to maximize your potential' : 'No appointments required'}</Text>
                            <View style={{ height: vw(20) }} />
                            <Text style={{ textAlign: 'center', fontFamily: Fonts.semibold, fontSize: 16, color: 'rgba(0, 0, 0, 0.7)' }}>{'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo ncididunt ut labore et dolore'}</Text>
                            {/* <View style={{ height: vw(16) }} />
                            <Button
                                text={'Get Started'}
                                bgColor={Colors.appGreen}
                                borderColor={Colors.appGreen}
                                textColor={Colors.whiteFour}
                                fontSize={vw(16)}
                                fontFamily={Fonts.semibold}
                                height={vw(45)}
                                borderRadius={vw(5)}
                                onPress={() => { }}
                            /> */}
                            <View style={{ height: vw(16) }} />
                            <Text style={bottomTextStyle}>{'Already signed up? '}<Text style={{ color: Colors.blue, textDecorationLine: 'underline', fontFamily: Fonts.bold, fontSize: vw(15) }} onPress={() => this.props.navigation.navigate('Login')}>{'Login'}</Text></Text>
                        </View>
                        <SafeAreaView></SafeAreaView>
                    </PageContainer>
                </View>
            </>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GetStarted)