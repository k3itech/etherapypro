import { StyleSheet } from 'react-native'
import {
    vw,
    vh
} from '../../common/ViewportUnits'
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: 'white'
    },
    bgStyle: {
        flex: 1,
        width: '100%'
    },
    logoStyle: {
        height: vh(92),
        marginTop: vh(25),
        borderWidth: 0
    },
    innerPageContainer: {
        justifyContent: 'flex-start',
        height: vh(450)
    },
    headingTextStyle: {
        fontFamily: Fonts.light,
        fontSize: vw(34),
        textAlign: 'center',
        letterSpacing: 0,
        color: Colors.blue,
        marginTop: vh(23)
    },
    subheadingTextStyle: {
        fontFamily: Fonts.light,
        fontSize: vw(15),
        textAlign: 'center',
        letterSpacing: 0,
        color: Colors.blue1,
        marginTop: vh(4)
    },
    btnSection: {
        marginTop: vh(59)
    },
    dotStyle: {
        width: vw(14),
        height: vw(14),
        borderRadius: vw(14) / 2,
        borderWidth: 0.5,
        backgroundColor: Colors.whiteFour,
        shadowColor: Colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: vh(8)
        },
        borderColor: Colors.shadowColor,
        elevation: 1,
        shadowOpacity: 0.3
    },
    activeDotStyle: {
        width: vw(14),
        height: vw(14),
        borderRadius: vw(14) / 2,
        backgroundColor: Colors.appDarkGreen,
        shadowColor: Colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: vh(8)
        },
        shadowOpacity: 0.3
    },
    bottomView: {
        paddingHorizontal: vw(32),
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'white'
    },
    bottomTextStyle: {
        textAlign: 'center',
        width: '100%',
        color: Colors.black,
        fontSize: vw(15),
        fontFamily: Fonts.regular
    }
})

// make this component available to the app
export default styles