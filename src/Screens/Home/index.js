//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground,
    SafeAreaView,
    ScrollView,
    FlatList
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import PageContainer from "../../Components/PageContainer"
import styles from './style'
import HeaderNew from '../../Components/HeaderNew'
import Functions from '../../Utility/Functions'
import OverlayLoading from '../../Components/OverlayLoading'
import { myProfileAPI } from '../../Redux/Actions'
import RegisterButton from '../../Components/RegisterButton'
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'
import HomeHorizontalListCard from '../../Components/HomeHorizontalListCard'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { joinChat } from '../../Utility/Socket'

// create a component
class Home extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    static getDerivedStateFromProps(props, state) {
        // Any time the current user changes,
        // Reset any parts of state that are tied to that user.

        // if (props.errorMessageMyClientsList != undefined) {
        //     setTimeout(() => {
        //         CustomAlert.alert(props.errorMessageMyClientsList)
        //         props.myClientsListErrorReset()
        //     }, 100)
        // }

        return null
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            //this.props.myProfileAPI(this.props.userData.token)
        })
    }

    componentWillUnmount() {
        this._unsubscribe()
    }

    getMyAgentsListFromAPI = (page) => {
        Functions.isNetworkConnected().then(isNetwork => {
            if (isNetwork) {
                // this.props.myClientsListApi(page, PAGINATION_LIMT, this.props.userData.token)
            }
        })
    }

    renderItem = (item) => {
        return (
            <HomeHorizontalListCard 
            />
        )
    }

    render() {
        const {
            main,
            bgStyle,
            roomTextStyle,
            matchingAgentViewStyle,
            flatStyle,
            flatMainStyle,
            therapyToolsViewStyle
        } = styles
        return (
            <>
                <View style={bgStyle}>
                    <SafeAreaView></SafeAreaView>
                    <ImageBackground
                        source={require('../../assets/img/homebg.png')}
                        resizeMode={'cover'}
                        style={{
                            width: '100%',
                            height: '100%',
                            flex: 1
                        }}
                    >
                        <View style={{ height: 80 }}>
                            <HeaderNew
                                isTitle={false}
                                title={''}
                                isLeftIcon={false}
                                isRightIcon={true}
                                onPressRightIcon={() => {
                                    this.props.navigation.navigate('Settings')
                                }}
                                rightIconOrImage={''}
                            />
                        </View>
                        <OverlayLoading
                            visible={this.props.loading}
                        />
                        <View style={{ ...main, borderWidth: 0, borderColor: 'red' }}>
                            <PageContainer padding={vw(0)}>
                                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                                    <Text style={roomTextStyle}>Rooms</Text>
                                    <TouchableOpacity 
                                        style={matchingAgentViewStyle} 
                                        onPress={() => {
                                            joinChat()
                                            this.props.navigation.navigate('ChatDetail')
                                        }} 
                                        disabled={false}
                                    >
                                        <View style={{ justifyContent: 'center', alignItems: 'center', borderWidth: 0, flex: 0.3 }}>
                                            <RegisterButton
                                                bgColor={Colors.whiteFour}
                                                borderColor={null}
                                                borderRadius={vw(72) / 2}
                                                shadowColor={false}
                                                height={vw(72)}
                                                width={vw(72)}
                                                disabled={true}
                                                buttonImageWidth={vw(72)}
                                                buttonImageHeight={vw(72)}
                                                buttonImage={require('../../assets/img/horizontal_bg.png')}
                                                borderWidth={0}
                                            />
                                        </View>
                                        <View style={{ borderWidth: 0, flex: 0.7 }}>
                                            <Text style={{ color: Colors.whiteFour, fontFamily: Fonts.bold, fontSize: vw(17), marginTop: vw(12) }}>Tania Anderson</Text>
                                            <Text style={{ color: Colors.whiteFour, fontFamily: Fonts.regular, fontSize: vw(13), marginTop: vw(0) }}>Matching agent</Text>
                                            <Text style={{ color: Colors.whiteFour, fontFamily: Fonts.semibold, fontSize: vw(13), marginTop: vw(14) }}>Hi, Welcome to ethrapypro</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <View style={{ marginTop: vw(24) }}>
                                        <FlatList
                                            horizontal
                                            contentContainerStyle={flatStyle}
                                            style={flatMainStyle}
                                            data={[1, 2, 3, 4, 5]}
                                            renderItem={this.renderItem}
                                            keyExtractor={(item) => `${item}`}
                                            showsHorizontalScrollIndicator={false}
                                            showsVerticalScrollIndicator={false}
                                        />
                                    </View>
                                    <Text style={{ color: Colors.black, fontFamily: Fonts.bold, fontSize: vw(22), marginTop: vw(16), marginLeft: vw(32) }}>Therapy tools</Text>
                                    <TouchableOpacity style={therapyToolsViewStyle} onPress={() => this.props.navigation.navigate('AgentBookSession')}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', borderWidth: 0, flex: 0.2 }}>
                                            <RegisterButton
                                                bgColor={'#2197F1'}
                                                borderColor={'#2197F1'}
                                                borderRadius={vw(56) / 2}
                                                shadowColor={false}
                                                height={vw(56)}
                                                width={vw(56)}
                                                disabled={true}
                                                buttonImageWidth={vw(30)}
                                                buttonImageHeight={vw(30)}
                                                buttonImage={require('../../assets/img/film.png')}
                                                borderWidth={1}
                                            />
                                        </View>
                                        <View style={{ borderWidth: 0, flex: 0.8, justifyContent: 'center', paddingLeft: vw(12) }}>
                                            <Text style={{ color: Colors.black, fontFamily: Fonts.bold, fontSize: vw(18) }}>Live video sessions</Text>
                                            <Text style={{ color: 'rgba(0, 0, 0, 0.7)', fontFamily: Fonts.semibold, fontSize: vw(12), marginTop: vw(0) }}>No sessions scheduled</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={therapyToolsViewStyle} onPress={() => this.props.navigation.navigate('ClinicalProcess')}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', borderWidth: 0, flex: 0.2 }}>
                                            <RegisterButton
                                                bgColor={'#0E9748'}
                                                borderColor={'#0E9748'}
                                                borderRadius={vw(56) / 2}
                                                shadowColor={false}
                                                height={vw(56)}
                                                width={vw(56)}
                                                disabled={true}
                                                buttonImageWidth={vw(30)}
                                                buttonImageHeight={vw(30)}
                                                buttonImage={require('../../assets/img/growth.png')}
                                                borderWidth={1}
                                            />
                                        </View>
                                        <View style={{ borderWidth: 0, flex: 0.8, justifyContent: 'center', paddingLeft: vw(12) }}>
                                            <Text style={{ color: Colors.black, fontFamily: Fonts.bold, fontSize: vw(18) }}>Clinical progress</Text>
                                            <Text style={{ color: 'rgba(0, 0, 0, 0.7)', fontFamily: Fonts.semibold, fontSize: vw(12), marginTop: vw(0) }}>Track symptoms, set goals</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={therapyToolsViewStyle} onPress={() => this.props.navigation.navigate('ContactUs')}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', borderWidth: 0, flex: 0.2 }}>
                                            <RegisterButton
                                                bgColor={'#2B2B2B'}
                                                borderColor={'#2B2B2B'}
                                                borderRadius={vw(56) / 2}
                                                shadowColor={false}
                                                height={vw(56)}
                                                width={vw(56)}
                                                disabled={true}
                                                buttonImageWidth={vw(30)}
                                                buttonImageHeight={vw(30)}
                                                buttonImage={require('../../assets/img/film.png')}
                                                borderWidth={1}
                                            />
                                        </View>
                                        <View style={{ borderWidth: 0, flex: 0.8, justifyContent: 'center', paddingLeft: vw(12) }}>
                                            <Text style={{ color: Colors.black, fontFamily: Fonts.bold, fontSize: vw(18) }}>Contact Us</Text>
                                            <Text style={{ color: 'rgba(0, 0, 0, 0.7)', fontFamily: Fonts.semibold, fontSize: vw(12), marginTop: vw(0) }}>customer support</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <SafeAreaView></SafeAreaView>
                                </ScrollView>
                            </PageContainer>
                        </View>
                    </ImageBackground>
                </View>
            </>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    userData: state.auth.userData,
    loading: state.auth.loading
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    myProfileAPI: (token) => {
        dispatch(myProfileAPI(token))
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)