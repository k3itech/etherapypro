import { StyleSheet } from 'react-native'
import {
    vw,
    vh
} from '../../common/ViewportUnits'
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    bgStyle: {
        flex: 1,
        width: '100%',
        backgroundColor: Colors.whiteFour
    },
    innerPageContainer: {

    },
    flatStyle: {
        height: '100%'
    },
    listHeaderStyle: {
        height: vh(32),
        borderRadius: vw(8),
        backgroundColor: '#eeeeef',
        flexDirection: 'row',
        padding: vw(2)
    },
    roomTextStyle: {
        marginLeft: vw(16),
        marginTop: vw(35),
        color: Colors.whiteFour,
        fontFamily: Fonts.bold,
        fontSize: vw(27)
    },
    matchingAgentViewStyle: {
        backgroundColor: '#037835',
        borderRadius: vw(30), 
        shadowColor: Colors.shadowColor, 
        shadowOffset: { 
            width: 0, 
            height: vw(6)
        }, 
        shadowOpacity: 1, 
        elevation: 10, 
        shadowRadius: vw(5),
        marginTop: vw(9),
        marginLeft: vw(16),
        height: vw(100),
        flexDirection: 'row',
        width: '80%',
        justifyContent: 'center'
    },
    flatStyle: {
        paddingLeft: vw(16)
    },
    flatMainStyle: {
        paddingBottom: vh(29),
        marginBottom: vh(15)
    },
    therapyToolsViewStyle: {
        marginTop: vw(9),
        marginLeft: vw(32),
        height: vw(70),
        flexDirection: 'row',
        justifyContent: 'center'
    }
})

// make this component available to the app
export default styles