import React, { Component } from 'react'
import {
    View,
    ActivityIndicator
} from 'react-native'
import Colors from '../../common/Colors'

// create a component
class Loading extends Component {

    // render ui
    render() {
        const { size } = this.props
        return (
            <View style={styles.loaderStyle}>
                <ActivityIndicator
                    size={size || 'large'}
                    color={Colors.black}
                />
            </View>
        )
    }
}

const styles = {
    loaderStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
}

// make this component available to the app
export default Loading