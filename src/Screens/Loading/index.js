import React, { Component } from 'react'
import { Platform } from 'react-native'
import { connect } from 'react-redux'
import SplashScreen from 'react-native-splash-screen'
import Loading from './Loading'
import Functions from '../../Utility/Functions'
import { setAlreadyLoginUserData } from '../../Redux/Actions'
import Firebase from '../../Utility/Firebase'
import { connectSocket } from '../../Utility/Socket'

// create a component
class LoadingContainer extends Component {

    async componentDidMount() {

        if (Platform.OS == 'android') {
            //SplashScreen.hide()
        }

        //let firebase = new Firebase()
        //firebase.checkPermission()
        //firebase.createNotificationListeners()

        global.app_version = '1.0.0'
        global.backgroundUnixTime = 0
        global.received_push_notification_id = undefined

        // check user is signed in or not
        Functions.isSignedIn().then((result) => {
            console.log('result = ', result)
            if (!result || result == null) {
                this.props.navigation.navigate('Auth')
                // this.props.navigation.navigate('App')
            }
            else {
                if (result == {}) {
                    console.log('here if result = ', result);
                }
                else {
                    console.log('here else result = ', result);
                    this.props.setAlreadyLoginUserData(JSON.parse(result))
                    Functions.connectToSocketMethod()
                    this.props.navigation.navigate('App', { screen: 'Home'} )
                }
            }
        })
    }

    // render ui
    render() {
        return (
            <Loading />
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    userData: state.auth.userData,
    loading: state.auth.loading
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    setAlreadyLoginUserData: (data) => {
        dispatch(setAlreadyLoginUserData(data))
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoadingContainer)