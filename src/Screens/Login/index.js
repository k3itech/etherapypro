//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView, Alert
} from 'react-native'
import { connect } from 'react-redux'
import {
    vw,
    vh
} from '../../common/ViewportUnits'
import PageContainer from '../../Components/PageContainer'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import Button from '../../Components/Button'
import Input from '../../Components/Input'
import {
    loginAPIErrorReset,
    loginAPI
} from '../../Redux/Actions'
import mailImage from '../../assets/img/mail.png'
import passwordImage from '../../assets/img/password.png'
import Functions from '../../Utility/Functions'
import OverlayLoading from '../../Components/OverlayLoading'
import UniveralImageBg from '../../Components/UniveralImageBg'
import Checkbox from '../../Components/Checkbox'
import { APP_TITLE } from '../../Utility/Constants'
import Validation from '../../Utility/Validation'
import login_bg from '../../assets/img/login_bg.png'
import {
    connectSocket,
    joinEmit
} from '../../Utility/Socket';

// create a component
class Login extends Component {

    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            alertShowing: 0,
            isRememberPassword: false
        }
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            Functions.getRememberUserData().then(data => {
                console.log('data = ', data)
                if (!data || data == null) {
                    this.setState({
                        username: '',
                        isRememberPassword: false
                    })
                }
                else {
                    this.setState({
                        username: data,
                        isRememberPassword: true
                    })
                }
            })
        })
        // Functions.connectToSocketMethod()
    }

    componentWillUnmount() {
        this._unsubscribe()
    }

    static getDerivedStateFromProps(props, state) {
        // Any time the current user changes,
        // Reset any parts of state that are tied to that user.

        if (props.userData != undefined) {
            if (state.isRememberPassword == true) {
                Functions.rememberUser(state.username)
            }
            else {
                Functions.deleteUserRememberData()
            }
            props.navigation.popToTop()
            props.navigation.navigate('App', { screen: 'Home' })
            props.loginAPIErrorReset()
        }

        return null
    }

    componentDidUpdate(preProps) {
        if (this.props.loginErrorMessage != undefined) {
            if (this.state.alertShowing == 0) {
                this.setState({
                    alertShowing: 1
                })
            }
            setTimeout(() => {
                if (this.props.loginErrorMessage != undefined) {
                    Alert.alert(
                        APP_TITLE,
                        this.props.loginErrorMessage,
                        [
                            {
                                text: 'Ok',
                                onPress: () => {
                                    if (this.state.alertShowing == 1) {
                                        this.setState({
                                            alertShowing: 0
                                        })
                                    }
                                },
                                style: 'cancel'
                            }
                        ],
                        { cancelable: false }
                    )
                }
                this.props.loginAPIErrorReset()
            }, 1000)
        }
    }

    loginButtonTapped = async () => {
        let validate = Validation.signInFormValidation(this.state)
        if (validate.error == false) {
            Functions.isNetworkConnected().then(isNetwork => {
                if (isNetwork) {
                    let data = {
                        username: this.state.username,
                        password: this.state.password
                    }
                    this.props.loginAPI(data)
                }
            })
        }
    }
    render() {

        const {
            main,
            innerPageContainer,
            bottomTextStyle
        } = styles
        return (
            <>
                <OverlayLoading
                    visible={this.props.loading}
                />
                <UniveralImageBg
                    isLogoShowing={true}
                    isTitleShowing={false}
                    height={'100%'}
                    bgImage={login_bg}
                />
                <View style={main}>
                    <PageContainer padding={vw(0)}>
                        <View style={innerPageContainer}>
                            <Text style={{ textAlign: 'center', fontSize: vw(18), fontFamily: Fonts.bold }}>{'Please login your account'}</Text>
                            <View style={{ height: vh(12), borderWidth: 0 }} />
                            <Input
                                editable={true}
                                leftIconImageSource={mailImage}
                                autoCapitalize={'none'}
                                placeholder='Username'
                                isSecure={false}
                                onChangeText={(text) => {
                                    this.setState({
                                        username: text
                                    })
                                }}
                                keyboardType='default'
                                value={this.state.username}
                            />
                            <View style={{ height: vh(8), borderWidth: 0 }} />
                            <Input
                                editable={true}
                                leftIconImageSource={passwordImage}
                                autoCapitalize={'none'}
                                placeholder='Password'
                                isSecure={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        password: text
                                    })
                                }}
                                value={this.state.password}
                            />
                            <View style={{ height: vh(8), borderWidth: 0 }} />
                            <View style={{ flexDirection: 'row', width: '100%' }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', borderWidth: 0, width: '50%' }} >
                                    <Checkbox
                                        checked={this.state.isRememberPassword}
                                        onPress={() => {
                                            this.setState({
                                                isRememberPassword: !this.state.isRememberPassword
                                            })
                                        }}
                                    />
                                    <Text style={{ fontSize: vw(15), fontFamily: Fonts.regular, color: Colors.black, paddingLeft: 2 }}>{'Remember Me'}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', borderWidth: 0, width: '50%' }} >
                                    <Text style={{ fontSize: vw(15), fontFamily: Fonts.bold, color: Colors.blue, textDecorationStyle: 'solid', textDecorationLine: 'underline' }} onPress={() => this.props.navigation.navigate('ForgotPassword')}>{'Forgot Password?'}</Text>
                                </View>
                            </View>
                            <View style={{ height: vw(20), borderWidth: 0 }} />
                            <Button
                                text={'Login'}
                                bgColor={Colors.appGreen}
                                borderColor={Colors.appGreen}
                                textColor={Colors.whiteFour}
                                fontFamily={Fonts.semibold}
                                fontSize={vw(16)}
                                height={vw(45)}
                                borderRadius={vw(5)}
                                onPress={this.loginButtonTapped}
                            />
                            <View style={{ height: vw(8) }} />
                            <Text style={bottomTextStyle}>{"Don't have account? "}<Text style={{ color: Colors.blue, textDecorationLine: 'underline', fontFamily: Fonts.bold, fontSize: vw(15) }} onPress={() => this.props.navigation.navigate('RegisterStep1Feeling')}>{'Register Now'}</Text></Text>
                            <SafeAreaView></SafeAreaView>
                        </View>
                    </PageContainer>
                </View>
            </>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData,
    loginErrorMessage: state.auth.loginErrorMessage
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    loginAPI: (data) => {
        dispatch(loginAPI(data))
    },
    loginAPIErrorReset: () => {
        dispatch(loginAPIErrorReset())
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)