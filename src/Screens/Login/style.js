import { StyleSheet } from 'react-native'
import { vw } from '../../common/ViewportUnits'
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'

const styles = StyleSheet.create({
    main: {
        flex: 1,
        borderWidth: 0,
        position: 'absolute',
        width: '100%',
        bottom: 0
    },
    innerPageContainer: {
        justifyContent: 'flex-end',
        borderColor: 'blue',
        borderWidth: 0,
        marginHorizontal: vw(30)
    },
    bottomTextStyle: {
        textAlign: 'center',
        width: '100%',
        color: Colors.black,
        fontSize: vw(15),
        fontFamily: Fonts.regular
    }
})

// make this component available to the app
export default styles