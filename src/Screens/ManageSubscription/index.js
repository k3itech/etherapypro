//import liraries
import React, { Component } from 'react'
import {
    View,
    Image,
    SafeAreaView,
    Text
} from 'react-native'
import { connect } from 'react-redux'
import styles from './style'
import HeaderNew from '../../Components/HeaderNew'
import Fonts from '../../common/Fonts'
import { vh, vw } from '../../common/ViewportUnits'
import hand from '../../assets/img/hand.png'
import Colors from '../../common/Colors'
import Button from '../../Components/Button'

// create a component
class ManageSubscription extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        const {
            main,
            leftButtonViewStyle,
            subsctibeViewStyle
        } = styles
        return (
            <>
                <SafeAreaView></SafeAreaView>
                <View style={{ height: 80 }}>
                    <HeaderNew
                        onPressLeftIcon={() => { 
                            this.props.navigation.goBack() 
                        }}
                        isTitle={true}
                        title={'Manage subscription'}
                        isLeftIcon={true}
                    />
                </View>
                <View style={main}>
                    <View style={{ borderWidth: 0, marginHorizontal: 20 }}>
                        <Text style={{ marginTop: 30, fontFamily: Fonts.bold, fontSize: vw(22) }}>{'Messaging Therapy - Free'}</Text>
                        <Text style={{ marginTop: 2, fontFamily: Fonts.semibold, fontSize: vw(18) }}>{'Tania Anderson'}</Text>
                        <Text style={{ marginTop: 20, fontFamily: Fonts.semibold, fontSize: vw(16), color: 'rgba(0, 0, 0, 0.7)' }}>{'Live video session credits (LVS)'}</Text>
                        <Text style={{ marginTop: 2, fontFamily: Fonts.semibold, fontSize: vw(18), color: 'rgba(0, 0, 0, 0.7)' }}>{'----'}</Text>
                        <Text style={{ marginTop: 30, fontFamily: Fonts.semibold, fontSize: vw(16), color: 'rgba(0, 0, 0, 0.7)' }}>{'Billing cycle'}</Text>
                        <Text style={{ marginTop: 2, fontFamily: Fonts.semibold, fontSize: vw(18), color: 'rgba(0, 0, 0, 0.7)' }}>{'----'}</Text>
                        <Text style={{ marginTop: 30, fontFamily: Fonts.semibold, fontSize: vw(16), color: 'rgba(0, 0, 0, 0.7)' }}>{'Next renewal'}</Text>
                        <Text style={{ marginTop: 2, fontFamily: Fonts.semibold, fontSize: vw(18), color: 'rgba(0, 0, 0, 0.7)' }}>{'----'}</Text>
                        <View style={subsctibeViewStyle}>
                            <View style={leftButtonViewStyle}>
                                <View style={{ backgroundColor: '#0A7CE6', width: vw(33), height: vw(17), borderRadius: 5, position: 'absolute', top: vw(8) }} />
                                <Image
                                    source={hand}
                                    resizeMode={'center'}
                                    style={{
                                        width: vw(20),
                                        height: vw(25),
                                        borderWidth: 0
                                    }}
                                />
                            </View>
                            <View>
                                <Text style={{ marginTop: 22, fontSize: vw(22), fontFamily: Fonts.bold }}>{'Subscribe now'}</Text>
                                <Text style={{ fontFamily: Fonts.semibold, marginTop: 11, marginBottom: vw(100), fontSize: vw(16), color: 'rgba(0, 0, 0, 0.7)' }}>{'Start improving your life today\nbye selecting a subscription\nthat fits your needs.'}</Text>
                            </View>
                            <View style={{ borderWidth: 0, position: 'absolute', bottom: vw(30), width: '90%' }}>
                                <Button
                                    text={'Subscribe'}
                                    bgColor={Colors.appGreen}
                                    borderColor={Colors.appGreen}
                                    textColor={Colors.whiteFour}
                                    fontFamily={Fonts.semibold}
                                    fontSize={vw(16)}
                                    height={vw(45)}
                                    borderRadius={vw(5)}
                                    onPress={this.loginButtonTapped}
                                />
                            </View>
                        </View>
                    </View>
                    <SafeAreaView></SafeAreaView>
                </View>
            </>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ManageSubscription)