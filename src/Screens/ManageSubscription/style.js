import { StyleSheet } from 'react-native'
import Colors from '../../common/Colors'
import { vw } from '../../common/ViewportUnits'

const styles = StyleSheet.create({
    main: {
        flex: 1,
        borderWidth: 0,
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    leftButtonViewStyle: {
        alignItems: 'center',
        borderWidth: 0,
        justifyContent: 'center',
        marginRight: 16,
        backgroundColor: Colors.whiteFour,
        height: vw(56),
        width: vw(56),
        marginTop: vw(50),
        shadowColor: Colors.shadowColor, 
        shadowOffset: { 
            width: 0, 
            height: vw(6) 
        }, 
        shadowOpacity: 1, 
        elevation: 10, 
        shadowRadius: vw(5), 
        borderRadius: vw(56) / 2
    },
    subsctibeViewStyle: {
        backgroundColor: '#E0F8EA', 
        marginTop: 40, 
        shadowColor: Colors.shadowColor, 
        shadowOffset: { 
            width: 0, 
            height: vw(6)
        }, 
        shadowOpacity: 1, 
        elevation: 10, 
        shadowRadius: vw(5), 
        borderRadius: vw(5),
        flexDirection: 'row',
        justifyContent: 'center'
    }
})

// make this component available to the app
export default styles