//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    FlatList,
    ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import RegisterButton from '../../Components/RegisterButton'
import RegisterCheckboxItem from '../../Components/RegisterCheckboxItem'
import RegisterStepBottomBg from '../../Components/RegisterStepBottomBg'
import {
    selectRegisterStep10RelligiosData,
    selectRegisterStep10IllnessPainData
} from '../../Redux/Actions'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class RegisterStep10ReligiousIllness extends Component {

    constructor(props) {
        super(props)
    }

    renderItem = ({ item }) => {
        return (
            <RegisterCheckboxItem
                name={item}
                width={'50%'}
                isSelected={this.rowSelected(item)}
                onPress={() => this.selectItem(item)}
            />
        )
    }

    rowSelected = (item) => {
        if (this.props.selectedRegisterStep10ReligiousData == item) {
            return true
        }
        else {
            return false
        }
    }

    selectItem = (item) => {
        this.props.selectRegisterStep10RelligiosData(item)
    }

    goToStep11 = () => {
        if (this.props.selectedRegisterStep10ReligiousData == undefined || this.props.selectedRegisterStep10ReligiousData == '' || this.props.selectedRegisterStep10ReligiousData == null) {
            CustomAlert.alert('Please select your religious status')
        }
        else if (this.props.selectedRegisterStep10IllnessPainData == undefined) {
            CustomAlert.alert('Please select your chronic pain or illness status')
        }
        else {
            this.props.navigation.navigate('RegisterStep11GetStarted')
        }
    }

    render() {
        const {
            textStyle,
            innerPageContainer
        } = styles
        return (
            <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                <SafeAreaView></SafeAreaView>
                <RegisterHeader />
                <ScrollView style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: vw(30), borderWidth: 0 }}>
                            <Text style={{ ...textStyle, marginTop: 25 }}>{'Do you consider yourself a\nreligious or spiritual person?'}</Text>
                        </View>
                        <FlatList
                            style={{ borderWidth: 0, marginHorizontal: 5, marginTop: 20, marginBottom: 20 }}
                            data={this.props.registrationStaticData ? this.props.registrationStaticData.religousspitual : []}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => `${index}`}
                            numColumns={2}
                            extraData={this.state}
                        />
                        <Text style={{ ...textStyle, marginTop: 0 }}>{"Are you having any chronic\npain or illness?"}</Text>
                        <View style={{ flexDirection: 'row', marginHorizontal: vw(120), marginTop: 20, marginBottom: 34, justifyContent: 'space-between' }}>
                            <RegisterCheckboxItem
                                name={'Yes'}
                                isSelected={this.props.selectedRegisterStep10IllnessPainData == undefined ? false : this.props.selectedRegisterStep10IllnessPainData == true ? true : false}
                                onPress={() => {
                                    this.props.selectRegisterStep10IllnessPainData(true)
                                }}
                                width={'50%'}
                            />
                            <RegisterCheckboxItem
                                name={'No'}
                                isSelected={this.props.selectedRegisterStep10IllnessPainData == undefined ? false : this.props.selectedRegisterStep10IllnessPainData == true ? false : true}
                                onPress={() => {
                                    this.props.selectRegisterStep10IllnessPainData(false)
                                }}
                                width={'50%'}
                            />
                        </View>
                    </View>
                    <RegisterStepBottomBg />
                    <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 80, marginTop: -50 }}>
                        <RegisterButton
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            height={vw(87)}
                            width={vw(87)}
                            borderRadius={vw(87) / 2}
                            onPress={this.goToStep11}
                        />
                    </View>
                    <View style={{ borderWidth: 0, height: 80, backgroundColor: Colors.whiteFour }} />
                </ScrollView>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    registrationStaticData: state.auth.registrationStaticData,
    selectedRegisterStep10IllnessPainData: state.auth.selectedRegisterStep10IllnessPainData,
    selectedRegisterStep10ReligiousData: state.auth.selectedRegisterStep10ReligiousData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    selectRegisterStep10RelligiosData: (data) => {
        dispatch(selectRegisterStep10RelligiosData(data))
    },
    selectRegisterStep10IllnessPainData: (data) => {
        dispatch(selectRegisterStep10IllnessPainData(data))
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep10ReligiousIllness)