//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import RegisterButton from '../../Components/RegisterButton'
import RegisterCheckboxItem from '../../Components/RegisterCheckboxItem'
import RegisterStepBottomBg from '../../Components/RegisterStepBottomBg'
import {
    selectRegisterStep11MedicationData,
    selectRegisterStep11GetReadyToStartData
} from '../../Redux/Actions'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class RegisterStep11GetStarted extends Component {

    constructor(props) {
        super(props)
    }

    goToStep12 = () => {
        if (this.props.selectedRegisterStep11MedicationData == undefined) {
            CustomAlert.alert('Please select medication')
        }
        else if (this.props.selectedRegisterStep11GetReadyToStartData == undefined) {
            CustomAlert.alert('Please select Get Started value')
        }
        else {
            this.props.navigation.navigate('RegisterStep12Form')
        }
    }

    render() {
        const {
            textStyle,
            innerPageContainer
        } = styles
        return (
            <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                <SafeAreaView></SafeAreaView>
                <RegisterHeader />
                <View style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: vw(40), borderWidth: 0 }}>
                            <Text style={{ ...textStyle, marginTop: 25 }}>{'Are you currently taking\nany medication?'}</Text>
                            <View style={{ flexDirection: 'row', marginHorizontal: vw(90), marginTop: 30, marginBottom: 34, justifyContent: 'space-between' }}>
                                <RegisterCheckboxItem
                                    name={'Yes'}
                                    isSelected={this.props.selectedRegisterStep11MedicationData == undefined ? false : this.props.selectedRegisterStep11MedicationData == true ? true : false}
                                    onPress={() => {
                                        this.props.selectRegisterStep11MedicationData(true)
                                    }}
                                    width={'50%'}
                                />
                                <RegisterCheckboxItem
                                    name={'No'}
                                    isSelected={this.props.selectedRegisterStep11MedicationData == undefined ? false : this.props.selectedRegisterStep11MedicationData == true ? false : true}
                                    onPress={() => {
                                        this.props.selectRegisterStep11MedicationData(false)
                                    }}
                                    width={'50%'}
                                />
                            </View>
                            <Text style={{ ...textStyle, marginTop: 20 }}>{'Are you ready to get\nstarted.?'}</Text>
                            <View style={{ flexDirection: 'row', marginHorizontal: vw(90), marginTop: 30, marginBottom: 34, justifyContent: 'space-between' }}>
                                <RegisterCheckboxItem
                                    name={'Yes'}
                                    isSelected={this.props.selectedRegisterStep11GetReadyToStartData == undefined ? false : this.props.selectedRegisterStep11GetReadyToStartData == true ? true : false}
                                    onPress={() => {
                                        this.props.selectRegisterStep11GetReadyToStartData(true)
                                    }}
                                    width={'50%'}
                                />
                                <RegisterCheckboxItem
                                    name={'No'}
                                    isSelected={this.props.selectedRegisterStep11GetReadyToStartData == undefined ? false : this.props.selectedRegisterStep11GetReadyToStartData == true ? false : true}
                                    onPress={() => {
                                        this.props.selectRegisterStep11GetReadyToStartData(false)
                                    }}
                                    width={'50%'}
                                />
                            </View>
                        </View>
                    </View>
                    <RegisterStepBottomBg />
                    <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 80, marginTop: -50 }}>
                        <RegisterButton
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            height={vw(87)}
                            width={vw(87)}
                            borderRadius={vw(87) / 2}
                            onPress={this.goToStep12}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    selectedRegisterStep11GetReadyToStartData: state.auth.selectedRegisterStep11GetReadyToStartData,
    selectedRegisterStep11MedicationData: state.auth.selectedRegisterStep11MedicationData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    selectRegisterStep11MedicationData: (data) => {
        dispatch(selectRegisterStep11MedicationData(data))
    },
    selectRegisterStep11GetReadyToStartData: (data) => {
        dispatch(selectRegisterStep11GetReadyToStartData(data))
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep11GetStarted)