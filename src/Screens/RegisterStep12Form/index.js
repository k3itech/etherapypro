//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Button from '../../Components/Button'
import mailImage from '../../assets/img/mailWhite.png'
import passwordImage from '../../assets/img/passwordWhite.png'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import RegisterButton from '../../Components/RegisterButton'
import Input from '../../Components/Input'
import RegisterStepBottomBg from '../../Components/RegisterStepBottomBg'
import Functions from '../../Utility/Functions'
import {
    registerAPI,
    registerAPIErrorReset
} from '../../Redux/Actions'
import Validation from '../../Utility/Validation'
import OverlayLoading from '../../Components/OverlayLoading'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class RegisterStep12Form extends Component {

    constructor(props) {
        super(props)

        this.state = {
            userName: '',
            email: '',
            password: '',
            confirmPassword: '',
            mobileNo: ''
        }
    }

    static getDerivedStateFromProps(props, state) {
        // Any time the current user changes,
        // Reset any parts of state that are tied to that user.

        if (props.registerErrorMessage != undefined) {
            setTimeout(() => {
                CustomAlert.alert(props.registerErrorMessage)
                props.registerAPIErrorReset()
            }, 100)
        }

        if (props.userData != undefined) {
            //props.navigation.popToTop()
            //props.navigation.navigate('App', { screen: 'Home' })
            props.navigation.navigate('RegisterStep13')
            props.registerAPIErrorReset()
        }

        return null
    }

    privacyPolicyTapped = () => {

    }

    termsTapped = () => {

    }

    validation = () => {

    }

    letsBeginTapped = () => {
        let validate = Validation.registerFormValidation(this.state)
        if (validate.error == false) {
            Functions.isNetworkConnected().then(isNetwork => {
                if (isNetwork) {
                    let data = {
                        username: this.state.userName,
                        email: this.state.email,
                        password: this.state.password,
                        mobileNo: this.state.mobileNo,
                        role: 'user',
                        status: true,
                        feeling: this.props.selectedRegisterStep1FeelingsData,
                        challenge: [this.props.selectedRegisterStep2ChallengesData],
                        arealife: this.props.selectedRegisterStep3AreaLifeData,
                        description: this.props.selectedRegisterStep4ChangeOneThingData,
                        sucideattempt: this.props.selectedRegisterStep5SuicideAttemptData,
                        counsellingattempt: this.props.selectedRegisterStep5CounsellingData,
                        age: this.props.selectedRegisterStep6AgeData,
                        country: this.props.selectedRegisterStep6CountryData,
                        state: this.props.selectedRegisterStep6StateData,
                        relationshipstatus: this.props.selectedRegisterStep7RelationshipData,
                        genderidentity: this.props.selectedRegisterStep8GenderData,
                        sexualorientation: this.props.selectedRegisterStep9SexualOrientationData,
                        religousspitual: this.props.selectedRegisterStep10ReligiousData,
                        painorillness: this.props.selectedRegisterStep10IllnessPainData,
                        medicinestatus: this.props.selectedRegisterStep11MedicationData,
                        ready: this.props.selectedRegisterStep11GetReadyToStartData,
                        deleted: false
                    }
                    this.props.registerAPI(data)
                }
            })
        }
    }

    render() {
        const { innerPageContainer } = styles
        return (
            <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                <OverlayLoading visible={this.props.loading} />
                <SafeAreaView></SafeAreaView>
                <RegisterHeader />
                <View style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: vw(40) }}>
                            <View style={{ height: 30 }} />
                            <Input
                                editable={true}
                                leftIconImageSource={mailImage}
                                autoCapitalize={'none'}
                                placeholder='Your Username'
                                isSecure={false}
                                onChangeText={(text) => {
                                    this.setState({
                                        userName: text
                                    })
                                }}
                                keyboardType='default'
                                value={this.state.userName}
                                fromRegister={true}
                            />
                            <View style={{ height: 14 }} />
                            <Input
                                editable={true}
                                leftIconImageSource={mailImage}
                                autoCapitalize={'none'}
                                placeholder='Your Email'
                                isSecure={false}
                                onChangeText={(text) => {
                                    this.setState({
                                        email: text
                                    })
                                }}
                                keyboardType='email-address'
                                value={this.state.email}
                                fromRegister={true}
                            />
                            <View style={{ height: 14 }} />
                            <Input
                                editable={true}
                                leftIconImageSource={mailImage}
                                autoCapitalize={'none'}
                                placeholder='Your Mobile Number'
                                isSecure={false}
                                onChangeText={(text) => {
                                    this.setState({
                                        mobileNo: text
                                    })
                                }}
                                keyboardType='number-pad'
                                value={this.state.mobileNo}
                                fromRegister={true}
                            />
                            <View style={{ height: 14 }} />
                            <Input
                                editable={true}
                                fromRegister={true}
                                leftIconImageSource={passwordImage}
                                autoCapitalize={'none'}
                                placeholder='Password'
                                isSecure={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        password: text
                                    })
                                }}
                                value={this.state.password}
                            />
                            <View style={{ height: 14 }} />
                            <Input
                                editable={true}
                                fromRegister={true}
                                leftIconImageSource={passwordImage}
                                autoCapitalize={'none'}
                                placeholder='Confirm Password'
                                isSecure={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        confirmPassword: text
                                    })
                                }}
                                value={this.state.confirmPassword}
                            />
                            <View style={{ height: 14 }} />
                            <Button
                                text={"Let's Begin"}
                                bgColor={Colors.whiteFour}
                                borderColor={Colors.whiteFour}
                                textColor={Colors.black}
                                fontFamily={Fonts.semibold}
                                fontSize={vw(16)}
                                height={vw(45)}
                                borderRadius={vw(5)}
                                onPress={this.letsBeginTapped}
                            />
                            <View style={{ height: 30 }} />
                            <Text style={{ fontFamily: Fonts.regular, fontSize: 14, color: Colors.whiteFour, textAlign: 'center' }}>{`By clicking on "Let's Begin" you affirm that you agree with the `}<Text style={{ textDecorationLine: 'underline' }} onPress={this.privacyPolicyTapped}>{'Privacy Policy'}</Text> {'and the '}<Text style={{ textDecorationLine: 'underline' }} onPress={this.termsTapped}>{'Terms of Service'}</Text>{' of eTherapyPro.'}</Text>
                            <View style={{ height: 14 }} />
                        </View>
                    </View>
                    <RegisterStepBottomBg />
                    {/* <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 80, marginTop: -50 }}>
                        <RegisterButton
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            height={vw(87)}
                            width={vw(87)}
                            borderRadius={vw(87) / 2}
                        />
                    </View> */}
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData,
    selectedRegisterStep1FeelingsData: state.auth.selectedRegisterStep1FeelingsData,
    selectedRegisterStep2ChallengesData: state.auth.selectedRegisterStep2ChallengesData,
    selectedRegisterStep3AreaLifeData: state.auth.selectedRegisterStep3AreaLifeData,
    selectedRegisterStep4ChangeOneThingData: state.auth.selectedRegisterStep4ChangeOneThingData,
    selectedRegisterStep5SuicideAttemptData: state.auth.selectedRegisterStep5SuicideAttemptData,
    selectedRegisterStep5CounsellingData: state.auth.selectedRegisterStep5CounsellingData,
    selectedRegisterStep6AgeData: state.auth.selectedRegisterStep6AgeData,
    selectedRegisterStep6CountryData: state.auth.selectedRegisterStep6CountryData,
    selectedRegisterStep6StateData: state.auth.selectedRegisterStep6StateData,
    selectedRegisterStep7RelationshipData: state.auth.selectedRegisterStep7RelationshipData,
    selectedRegisterStep8GenderData: state.auth.selectedRegisterStep8GenderData,
    selectedRegisterStep9SexualOrientationData: state.auth.selectedRegisterStep9SexualOrientationData,
    selectedRegisterStep10ReligiousData: state.auth.selectedRegisterStep10ReligiousData,
    selectedRegisterStep10IllnessPainData: state.auth.selectedRegisterStep10IllnessPainData,
    selectedRegisterStep11MedicationData: state.auth.selectedRegisterStep11MedicationData,
    selectedRegisterStep11GetReadyToStartData: state.auth.selectedRegisterStep11GetReadyToStartData,
    registerErrorMessage: state.auth.registerErrorMessage
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    registerAPI: (data) => {
        dispatch(registerAPI(data))
    },
    registerAPIErrorReset: () => {
        dispatch(registerAPIErrorReset())
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep12Form)