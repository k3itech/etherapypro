//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import CardInput from '../../Components/CardInput'
import Button from '../../Components/Button'
import CollapsibleView from '../../Components/CollapsibleView'
import { ScrollView } from 'react-native-gesture-handler'
import OverlayLoading from '../../Components/OverlayLoading'
import {
    addCardAPI,
    addCardAPIErrorReset
} from '../../Redux/Actions'
import Validation from '../../Utility/Validation'
import Functions from '../../Utility/Functions'

// create a component
class RegisterStep13 extends Component {

    constructor(props) {
        super(props)

        this.state = {
            cardHolderName: '',
            email: '',
            cardNumber: '',
            expiry: '',
            cvv: ''
        }
    }

    financialAidTapped = () => {

    }

    startCounselingOnPress = () => {
        let validate = Validation.addCardDetailsFormValidation(this.state)
        if (validate.error == false) {
            Functions.isNetworkConnected().then(isNetwork => {
                if (isNetwork) {
                    var expiryDate = this.state.expiry != null ? String(this.state.expiry).trim() : this.state.expiry
                    let enteredMonth = expiryDate.slice(NaN, 2)
                    let enteredYear = expiryDate.slice(2)
                    let data = {
                        name: this.state.cardHolderName,
                        address: {
                            line1: '',
                            postal_code: '',
                            city: '',
                            state: '',
                            country: ''
                        },
                        email: this.state.email,
                        cardNumber: this.state.cardNumber,
                        expMonth: enteredMonth,
                        expYear: enteredYear,
                        cvc: this.state.cvv
                    }
                    this.props.addCardAPI(data)
                }
            })
        }
    }

    render() {
        const {
            textStyle,
            innerPageContainer,
            paymentView
        } = styles
        return (
            <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                <SafeAreaView></SafeAreaView>
                <RegisterHeader />
                <OverlayLoading
                    visible={this.props.loading}
                />
                <ScrollView style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: vw(16) }}>
                            <Text style={{ ...textStyle, marginTop: 25 }}>{'Welcome to eTherapyPro\nbhauuu !'}</Text>
                            <View style={{ height: 16 }} />
                            <Text style={{ fontFamily: Fonts.regular, fontSize: 14, color: Colors.whiteFour, textAlign: 'center' }}>{`Need financial assistance?\n`}<Text style={{ textDecorationLine: 'underline' }} onPress={this.financialAidTapped}>{'Click here for Financial Aid'}</Text></Text>
                            <View style={paymentView}>
                                <Text style={{ fontSize: 16, fontFamily: Fonts.semibold }}>{'Cost : $0.00 Today'}</Text>
                                <Text style={{ fontSize: 14, fontFamily: Fonts.regular, marginTop: 2, marginBottom: 20 }}>{'$40 / week after your FREE trial (billed monthly)'}</Text>
                                <CardInput
                                    editable={true}
                                    autoCapitalize={'none'}
                                    placeholder='Card Holder Name'
                                    label={'Name on Card*'}
                                    onChangeText={(text) => {
                                        this.setState({
                                            cardHolderName: text
                                        })
                                    }}
                                    value={this.state.cardHolderName}
                                />
                                <View style={{ marginBottom: vw(12) }} />
                                <CardInput
                                    editable={true}
                                    autoCapitalize={'none'}
                                    placeholder='Email'
                                    label={'Email*'}
                                    onChangeText={(text) => {
                                        this.setState({
                                            email: text
                                        })
                                    }}
                                    value={this.state.email}
                                    keyboardType={'email-address'}
                                />
                                <View style={{ marginBottom: vw(12) }} />
                                <CardInput
                                    editable={true}
                                    autoCapitalize={'none'}
                                    placeholder='Card number'
                                    label={'Card Number*'}
                                    onChangeText={(text) => {
                                        this.setState({
                                            cardNumber: text
                                        })
                                    }}
                                    value={this.state.cardNumber}
                                    maxLength={16}
                                    keyboardType={'number-pad'}
                                />
                                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', marginTop: vw(12) }}>
                                    <View style={{ width: '70%' }}>
                                        <CardInput
                                            editable={true}
                                            autoCapitalize={'none'}
                                            placeholder='Month / Year'
                                            label={'Expiry *'}
                                            onChangeText={(text) => {
                                                this.setState({
                                                    expiry: text
                                                })
                                            }}
                                            value={this.state.expiry}
                                            maxLength={4}
                                            keyboardType={'number-pad'}
                                        />
                                    </View>
                                    <View style={{ width: '20%' }}>
                                        <CardInput
                                            editable={true}
                                            autoCapitalize={'none'}
                                            label={'CVV*'}
                                            placeholder='CVV'
                                            onChangeText={(text) => {
                                                this.setState({
                                                    cvv: text
                                                })
                                            }}
                                            value={this.state.cvv}
                                            maxLength={3}
                                            keyboardType={'number-pad'}
                                        />
                                    </View>
                                </View>
                                <View style={{ height: vw(20) }} />
                                <Button
                                    text={"Start Counseling"}
                                    bgColor={Colors.appGreen}
                                    borderColor={Colors.appGreen}
                                    textColor={Colors.whiteFour}
                                    fontFamily={Fonts.semibold}
                                    fontSize={vw(16)}
                                    height={vw(45)}
                                    borderRadius={vw(5)}
                                    onPress={this.startCounselingOnPress}
                                />
                                <Text style={{ fontSize: 10, fontFamily: Fonts.regular, marginTop: 20 }}>{`By clicking on "Start Counseling", you authorize eTherapyPro to automatically renew your subscription monthly using the above payment method at a rate of $160 ($40 per week x 4 weeks). The first charge will occur after your free trial. You may cancel online at any time to avoid charges. See Terms of Service for more details.`}</Text>
                            </View>
                            <CollapsibleView />
                            <View style={{ height: 20 }} />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep13)