import { StyleSheet } from 'react-native'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import { vw } from '../../common/ViewportUnits'

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    innerPageContainer: {
        borderWidth: 0,
        borderColor: 'red',
        backgroundColor: Colors.whiteFour
    },
    textStyle: {
        fontFamily: Fonts.bold,
        fontSize: 22,
        color: Colors.whiteFour,
        textAlign: 'center'
    },
    paymentView: {
        flexDirection: 'column',
        width: '100%',
        borderRadius: 5,
        shadowColor: Colors.shadowColor,
        shadowOffset: {
            width: 0,
            height: vw(6)
        },
        shadowOpacity: 1,
        elevation: 10,
        backgroundColor: Colors.whiteFour,
        borderWidth: 2,
        borderColor: Colors.whiteFour,
        marginBottom: 8,
        marginTop: 16,
        padding: 16
    }
})

// make this component available to the app
export default styles