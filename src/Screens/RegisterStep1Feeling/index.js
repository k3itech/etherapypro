//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    FlatList
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import RegisterButton from '../../Components/RegisterButton'
import RegisterCheckboxItem from '../../Components/RegisterCheckboxItem'
import RegisterStepBottomBg from '../../Components/RegisterStepBottomBg'
import OverlayLoading from '../../Components/OverlayLoading'
import Functions from '../../Utility/Functions'
import {
    registrationStaticDataAPI,
    registrationStaticDataAPIErrorReset,
    selectRegisterStep1FeelingsData
} from '../../Redux/Actions'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class RegisterStep1Feeling extends Component {

    constructor(props) {
        super(props)

        this.state = {
            selectedItems: []
        }
    }

    static getDerivedStateFromProps(props, state) {
        // Any time the current user changes,
        // Reset any parts of state that are tied to that user.

        if (props.registrationStaticDataErrorMessage != undefined) {
            setTimeout(() => {
                CustomAlert.alert(props.registrationStaticDataErrorMessage)
                props.registrationStaticDataAPIErrorReset()
            }, 100)
        }

        return null
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getRegistrationStaticDataFromAPI()
        })
    }

    componentWillUnmount() {
        this._unsubscribe()
    }

    getRegistrationStaticDataFromAPI = () => {
        Functions.isNetworkConnected().then(isNetwork => {
            if (isNetwork) {
                this.props.registrationStaticDataAPI()
            }
        })
    }

    renderItem = ({ item }) => {
        return (
            <RegisterCheckboxItem
                name={item}
                width={'33.33%'}
                isSelected={this.rowSelected(item)}
                onPress={() => this.selectItem(item)}
            />
        )
    }

    rowSelected = (item) => {
        if (item == this.props.selectedRegisterStep1FeelingsData) {
            return true
        }
        else {
            return false
        }
    }

    selectItem = (item) => {
        this.props.selectRegisterStep1FeelingsData(item)
    }

    goToStep2 = () => {
        if (this.props.selectedRegisterStep1FeelingsData == '' || this.props.selectedRegisterStep1FeelingsData == null || this.props.selectedRegisterStep1FeelingsData == undefined) {
            CustomAlert.alert('Please select today feeling')
        }
        else {
            this.props.navigation.navigate('RegisterStep2Challenge')
        }
    }

    render() {
        const {
            textStyle,
            innerPageContainer
        } = styles
        return (
            <>
                <OverlayLoading visible={this.props.loading} />
                <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                    <SafeAreaView></SafeAreaView>
                    <RegisterHeader />
                    <View style={innerPageContainer}>
                        <View style={{ backgroundColor: Colors.appGreen }}>
                            <View style={{ marginHorizontal: vw(40), borderWidth: 0 }}>
                                <Text style={{ ...textStyle, marginTop: 25 }}>{'Help us find the best\ntherapist for you!'}</Text>
                                <Text style={{ ...textStyle, marginTop: 17, fontFamily: Fonts.regular, fontSize: 14 }}>{"Our questionnaire is completely anonymous;\nit consists of 15 questions and it should take\nabout 5-10 minutes to complete.\n\nOnce you finish you will be automatically\nmatched with a counselor."}</Text>
                                <Text style={{ ...textStyle, marginTop: 37 }}>{'Tell us about you.'}</Text>
                                <Text style={{ ...textStyle, marginTop: 2, fontFamily: Fonts.semibold, fontSize: 16 }}>{'How are you feeling today?'}</Text>
                            </View>
                            <FlatList
                                style={{ borderWidth: 0, marginHorizontal: 16, marginTop: 20, marginBottom: 0 }}
                                data={this.props.registrationStaticData ? this.props.registrationStaticData.feeling : []}
                                renderItem={this.renderItem}
                                keyExtractor={(item, index) => `${index}`}
                                numColumns={3}
                                extraData={this.state}
                            />
                        </View>
                        <RegisterStepBottomBg />
                        <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 80, marginTop: -50 }}>
                            <RegisterButton
                                bgColor={Colors.appGreen}
                                borderColor={Colors.appGreen}
                                height={vw(87)}
                                width={vw(87)}
                                borderRadius={vw(87) / 2}
                                onPress={this.goToStep2}
                            />
                        </View>
                    </View>
                </View>
            </>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    registrationStaticData: state.auth.registrationStaticData,
    registrationStaticDataErrorMessage: state.auth.registrationStaticDataErrorMessage,
    selectedRegisterStep1FeelingsData: state.auth.selectedRegisterStep1FeelingsData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    registrationStaticDataAPI: () => {
        dispatch(registrationStaticDataAPI())
    },
    registrationStaticDataAPIErrorReset: () => {
        dispatch(registrationStaticDataAPIErrorReset())
    },
    selectRegisterStep1FeelingsData: (data) => {
        dispatch(selectRegisterStep1FeelingsData(data))
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep1Feeling)