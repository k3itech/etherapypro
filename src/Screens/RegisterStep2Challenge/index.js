//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    FlatList,
    ScrollView,
    TextInput
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import RegisterButton from '../../Components/RegisterButton'
import RegisterCheckboxItem from '../../Components/RegisterCheckboxItem'
import RegisterStepBottomBg from '../../Components/RegisterStepBottomBg'
import { selectRegisterStep2ChallengesData } from '../../Redux/Actions'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class RegisterStep2Challenge extends Component {

    constructor(props) {
        super(props)

        this.state = {
            otherValue: false
        }
    }

    renderItem = ({ item }) => {
        return (
            <RegisterCheckboxItem
                name={item}
                width={'50%'}
                isSelected={this.rowSelected(item)}
                onPress={() => this.selectItem(item)}
            />
        )
    }

    rowSelected = (item) => {
        if (item == this.props.selectedRegisterStep2ChallengesData) {
            return true
        }
        else {
            return false
        }
    }

    selectItem = (item) => {
        this.setState({
            otherValue: false
        })
        this.props.selectRegisterStep2ChallengesData(item)
    }

    goToStep3 = () => {
        if (this.props.selectedRegisterStep2ChallengesData == '' || this.props.selectedRegisterStep2ChallengesData == null || this.props.selectedRegisterStep2ChallengesData == undefined) {
            CustomAlert.alert('Please select challenges')
        }
        else {
            this.props.navigation.navigate('RegisterStep3AreaLife')
        }
    }

    render() {
        const {
            textStyle,
            innerPageContainer
        } = styles
        return (
            <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                <SafeAreaView></SafeAreaView>
                <RegisterHeader />
                <ScrollView style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: vw(20), borderWidth: 0 }}>
                            <Text style={{ ...textStyle, marginTop: 25 }}>{'Are you facing any of\nthese challenges?'}</Text>
                        </View>
                        <FlatList
                            style={{ borderWidth: 0, marginHorizontal: 16, marginTop: 20, marginBottom: 20 }}
                            data={this.props.registrationStaticData ? this.props.registrationStaticData.challenge : []}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => `${index}`}
                            numColumns={2}
                            extraData={this.props}
                        />
                        <View style={{ marginHorizontal: 20 }}>
                            <Text style={{ ...textStyle, marginTop: 0, fontFamily: Fonts.semibold, textAlign: 'left', fontSize: 16 }}>{'Other'}</Text>
                            <TextInput
                                onChangeText={(text) => {
                                    this.setState({
                                        otherValue: true
                                    })
                                    this.props.selectRegisterStep2ChallengesData(text)
                                }}
                                style={{
                                    height: 45,
                                    fontFamily: Fonts.semibold,
                                    fontSize: vw(14),
                                    color: Colors.black,
                                    backgroundColor: Colors.whiteFour,
                                    marginTop: 10,
                                    borderRadius: 5,
                                    marginBottom: 30,
                                    textAlignVertical: 'center',
                                    paddingLeft: 8,
                                    paddingRight: 0
                                }}
                                placeholder={''}
                                keyboardType={'default'}
                                value={this.state.otherValue == true ? this.props.selectedRegisterStep2ChallengesData : ''}
                                multiline={false}
                                editable={true}
                            />
                        </View>
                    </View>
                    <RegisterStepBottomBg />
                    <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 80, marginTop: -50 }}>
                        <RegisterButton
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            height={vw(87)}
                            width={vw(87)}
                            borderRadius={vw(87) / 2}
                            onPress={this.goToStep3}
                        />
                    </View>
                    <View style={{ borderWidth: 0, height: 80, backgroundColor: Colors.whiteFour }} />
                </ScrollView>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    registrationStaticData: state.auth.registrationStaticData,
    selectedRegisterStep2ChallengesData: state.auth.selectedRegisterStep2ChallengesData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    selectRegisterStep2ChallengesData: (data) => {
        dispatch(selectRegisterStep2ChallengesData(data))
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep2Challenge)