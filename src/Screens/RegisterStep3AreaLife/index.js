//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    FlatList
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import RegisterButton from '../../Components/RegisterButton'
import RegisterCheckboxItem from '../../Components/RegisterCheckboxItem'
import RegisterStepBottomBg from '../../Components/RegisterStepBottomBg'
import { selectRegisterStep3AreaLifeData } from '../../Redux/Actions'
import CustomAlert from '../../Utility/CustomAlert'


// create a component
class RegisterStep3AreaLife extends Component {

    constructor(props) {
        super(props)
    }

    renderItem = ({ item }) => {
        return (
            <RegisterCheckboxItem
                name={item}
                width={'50%'}
                isSelected={this.rowSelected(item)}
                onPress={() => this.selectItem(item)}
            />
        )
    }

    rowSelected = (item) => {
        if (this.props.selectedRegisterStep3AreaLifeData.length != 0) {
            let found = this.props.selectedRegisterStep3AreaLifeData.find(item1 => (item1 == item))
            if (found == undefined) {
                return false
            }
            else {
                return true
            }
        }
        else {
            return false
        }
    }

    selectItem = (item) => {
        let found = this.props.selectedRegisterStep3AreaLifeData.findIndex(item1 => (item1 == item))
        let selectedRegisterStep3AreaLifeData = []
        if (found >= 0) {
            selectedRegisterStep3AreaLifeData = [...this.props.selectedRegisterStep3AreaLifeData];
            selectedRegisterStep3AreaLifeData.splice(found, 1);
        }
        else {
            selectedRegisterStep3AreaLifeData = [...this.props.selectedRegisterStep3AreaLifeData, item]
        }
        this.props.selectRegisterStep3AreaLifeData(selectedRegisterStep3AreaLifeData)
    }

    goToStep4 = () => {
        if (this.props.selectedRegisterStep3AreaLifeData.length == 0) {
            CustomAlert.alert('Please select the area in which you want to improve')
        }
        else {
            this.props.navigation.navigate('RegisterStep4ChangeOneThing')
        }
    }

    render() {
        const {
            textStyle,
            innerPageContainer
        } = styles
        return (
            <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                <SafeAreaView></SafeAreaView>
                <RegisterHeader />
                <View style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: vw(40), borderWidth: 0 }}>
                            <Text style={{ ...textStyle, marginTop: 25 }}>{'Which of these areas of life\ndo you want to improve?'}</Text>
                        </View>
                        <FlatList
                            style={{ borderWidth: 0, marginHorizontal: 16, marginTop: 20, marginBottom: 20 }}
                            data={this.props.registrationStaticData ? this.props.registrationStaticData.arealife : []}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => `${index}`}
                            numColumns={2}
                            extraData={this.props}
                        />
                    </View>
                    <RegisterStepBottomBg />
                    <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 80, marginTop: -50 }}>
                        <RegisterButton
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            height={vw(87)}
                            width={vw(87)}
                            borderRadius={vw(87) / 2}
                            onPress={this.goToStep4}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    registrationStaticData: state.auth.registrationStaticData,
    selectedRegisterStep3AreaLifeData: state.auth.selectedRegisterStep3AreaLifeData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    selectRegisterStep3AreaLifeData: (data) => [
        dispatch(selectRegisterStep3AreaLifeData(data))
    ]
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep3AreaLife)