//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    ScrollView,
    TextInput
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import RegisterButton from '../../Components/RegisterButton'
import RegisterStepBottomBg from '../../Components/RegisterStepBottomBg'
import { selectRegisterStep4ChangeOneThingData } from '../../Redux/Actions'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class RegisterStep4ChangeOneThing extends Component {

    constructor(props) {
        super(props)
    }

    goToStep5 = () => {
        if (this.props.selectedRegisterStep4ChangeOneThingData == '' || this.props.selectedRegisterStep4ChangeOneThingData == null || this.props.selectedRegisterStep4ChangeOneThingData == undefined) {
            CustomAlert.alert('Please enter one thing that you want to improve')
        }
        else {
            this.props.navigation.navigate('RegisterStep5SuicideCounselling')
        }
    }

    render() {
        const {
            textStyle,
            innerPageContainer
        } = styles
        return (
            <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                <SafeAreaView></SafeAreaView>
                <RegisterHeader />
                <ScrollView style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: vw(40), borderWidth: 0 }}>
                            <Text style={{ ...textStyle, marginTop: 25 }}>{'If you could change just one\nthing about your life right\nnow, what would it be?'}</Text>
                        </View>
                        <View style={{ marginHorizontal: 20 }}>
                            <TextInput
                                onChangeText={(text) => {
                                    this.props.selectRegisterStep4ChangeOneThingData(text)
                                }}
                                style={{
                                    height: 180,
                                    fontFamily: Fonts.semibold,
                                    fontSize: vw(14),
                                    color: Colors.black,
                                    backgroundColor: Colors.whiteFour,
                                    marginTop: 20,
                                    borderRadius: 5,
                                    marginBottom: 14,
                                    textAlignVertical: 'top',
                                    padding: 8
                                }}
                                placeholder={''}
                                keyboardType={'default'}
                                value={this.props.selectedRegisterStep4ChangeOneThingData}
                                multiline={true}
                                editable={true}
                                maxLength={300}
                            />
                            <Text style={{ ...textStyle, marginTop: 0, fontFamily: Fonts.semibold, textAlign: 'left', fontSize: 16, marginBottom: 40 }}>{'0 of 300 max characters'}</Text>
                        </View>
                    </View>
                    <RegisterStepBottomBg />
                    <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 80, marginTop: -50 }}>
                        <RegisterButton
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            height={vw(87)}
                            width={vw(87)}
                            borderRadius={vw(87) / 2}
                            onPress={this.goToStep5}
                        />
                    </View>
                    <View style={{ borderWidth: 0, height: 80, backgroundColor: Colors.whiteFour }} />
                </ScrollView>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    selectedRegisterStep4ChangeOneThingData: state.auth.selectedRegisterStep4ChangeOneThingData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    selectRegisterStep4ChangeOneThingData: (data) => {
        dispatch(selectRegisterStep4ChangeOneThingData(data))
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep4ChangeOneThing)