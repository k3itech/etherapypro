//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import RegisterButton from '../../Components/RegisterButton'
import RegisterCheckboxItem from '../../Components/RegisterCheckboxItem'
import RegisterStepBottomBg from '../../Components/RegisterStepBottomBg'
import {
    selectRegisterStep5SuicideAttemptData,
    selectRegisterStep5CounsellingData
} from '../../Redux/Actions'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class RegisterStep5SuicideCounselling extends Component {

    constructor(props) {
        super(props)
    }

    goToStep6 = () => {
        if (this.props.selectedRegisterStep5SuicideAttemptData == undefined) {
            CustomAlert.alert('Please select suicide attempt value')
        }
        else if (this.props.selectedRegisterStep5CounsellingData == undefined) {
            CustomAlert.alert('Please select counselling value')
        }
        else {
            this.props.navigation.navigate('RegisterStep6AgeCountryState')
        }
    }

    render() {
        const {
            textStyle,
            innerPageContainer
        } = styles
        return (
            <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                <SafeAreaView></SafeAreaView>
                <RegisterHeader />
                <View style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: vw(40), borderWidth: 0 }}>
                            <Text style={{ ...textStyle, marginTop: 25 }}>{"Have you recently had\nany thoughts of\nself-harm or suicide?"}</Text>
                            <View style={{ flexDirection: 'row', marginHorizontal: vw(90), marginTop: 30, marginBottom: 34, justifyContent: 'center', borderWidth: 0 }}>
                                <RegisterCheckboxItem
                                    name={'Yes'}
                                    isSelected={this.props.selectedRegisterStep5SuicideAttemptData == undefined ? false : this.props.selectedRegisterStep5SuicideAttemptData == true ? true : false}
                                    onPress={() => { 
                                        this.props.selectRegisterStep5SuicideAttemptData(true)
                                    }}
                                    width={'50%'}
                                />
                                <RegisterCheckboxItem
                                    name={'No'}
                                    isSelected={this.props.selectedRegisterStep5SuicideAttemptData == undefined ? false : this.props.selectedRegisterStep5SuicideAttemptData == true ? false : true}
                                    onPress={() => { 
                                        this.props.selectRegisterStep5SuicideAttemptData(false)
                                    }}
                                    width={'50%'}
                                />
                            </View>
                            <Text style={{ ...textStyle, marginTop: 20 }}>{"Have you ever been in\ntherapy or counseling\nbefore?"}</Text>
                            <View style={{ flexDirection: 'row', marginHorizontal: vw(90), marginTop: 30, marginBottom: 34, justifyContent: 'space-between' }}>
                                <RegisterCheckboxItem
                                    name={'Yes'}
                                    isSelected={this.props.selectedRegisterStep5CounsellingData == undefined ? false : this.props.selectedRegisterStep5CounsellingData == true ? true : false}
                                    onPress={() => { 
                                        this.props.selectRegisterStep5CounsellingData(true)
                                    }}
                                    width={'50%'}
                                />
                                <RegisterCheckboxItem
                                    name={'No'}
                                    isSelected={this.props.selectedRegisterStep5CounsellingData == undefined ? false : this.props.selectedRegisterStep5CounsellingData == true ? false : true}
                                    onPress={() => {
                                        this.props.selectRegisterStep5CounsellingData(false)
                                    }}
                                    width={'50%'}
                                />
                            </View>
                        </View>
                    </View>
                    <RegisterStepBottomBg />
                    <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 80, marginTop: -50 }}>
                        <RegisterButton
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            height={vw(87)}
                            width={vw(87)}
                            borderRadius={vw(87) / 2}
                            onPress={this.goToStep6}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    selectedRegisterStep5SuicideAttemptData: state.auth.selectedRegisterStep5SuicideAttemptData,
    selectedRegisterStep5CounsellingData: state.auth.selectedRegisterStep5CounsellingData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    selectRegisterStep5CounsellingData: (data) => {
        dispatch(selectRegisterStep5CounsellingData(data))
    },
    selectRegisterStep5SuicideAttemptData: (data) => {
        dispatch(selectRegisterStep5SuicideAttemptData(data))
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep5SuicideCounselling)