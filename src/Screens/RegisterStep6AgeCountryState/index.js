//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    ScrollView,
    TextInput
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import RegisterButton from '../../Components/RegisterButton'
import InputSelection from '../../Components/InputSelection'
import RegisterStepBottomBg from '../../Components/RegisterStepBottomBg'
import {
    selectRegisterStep6AgeData,
    selectRegisterStep6CountryData,
    selectRegisterStep6StateData
} from '../../Redux/Actions'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class RegisterStep6AgeCountryState extends Component {

    constructor(props) {
        super(props)

        this.state = {
            ageData: [],
            countryData: [],
            stateData: []
        }
    }

    componentDidMount() {
        this._unsubscribe = this.props.navigation.addListener('focus', () => {
            if (this.props.registrationStaticData) {
                if (this.state.ageData.length == 0) {
                    let ageData = []
                    this.props.registrationStaticData.age.map(item => {
                        let data = {
                            label: item,
                            value: item
                        }
                        ageData.push(data)
                    })
                    this.setState({
                        ageData: ageData
                    })
                }
                if (this.state.countryData.length == 0) {
                    let countryData = []
                    this.props.registrationStaticData.country.map(item => {
                        let data = {
                            label: item,
                            value: item
                        }
                        countryData.push(data)
                    })
                    this.setState({
                        countryData: countryData
                    })
                }
                if (this.state.stateData.length == 0) {
                    let stateData = []
                    this.props.registrationStaticData.state.map(item => {
                        let data = {
                            label: item,
                            value: item
                        }
                        stateData.push(data)
                    })
                    this.setState({
                        stateData: stateData
                    })
                }
            }
        })
    }

    componentWillUnmount() {
        this._unsubscribe()
    }

    goToStep7 = () => {
        if (this.props.selectedRegisterStep6AgeData == '' || this.props.selectedRegisterStep6AgeData == null || this.props.selectedRegisterStep6AgeData == undefined) {
            CustomAlert.alert('Please select your age')
        }
        else if (this.props.selectedRegisterStep6CountryData == undefined || this.props.selectedRegisterStep6CountryData == '' || this.props.selectedRegisterStep6CountryData == null) {
            CustomAlert.alert('Please select country')
        }
        else if (this.props.selectedRegisterStep6StateData == undefined || this.props.selectedRegisterStep6StateData == null || this.props.selectedRegisterStep6StateData == '') {
            CustomAlert.alert('Please select state')
        }
        else {
            this.props.navigation.navigate('RegisterStep7Relationhip')
        }
    }

    render() {
        const {
            textStyle,
            innerPageContainer
        } = styles
        const placeholderAge = {
            label: 'Select Your Age',
            value: null,
            color: Colors.black
        }
        const placeholderCountry = {
            label: 'Select Your Country',
            value: null,
            color: Colors.black
        }
        const placeholderState = {
            label: 'Select Your State',
            value: null,
            color: Colors.black
        }
        return (
            <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                <SafeAreaView></SafeAreaView>
                <RegisterHeader />
                <ScrollView style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: 20, borderWidth: 0 }}>
                            <Text style={{ ...textStyle, marginTop: 25, marginBottom: 16 }}>{'How old are you?'}</Text>
                            <View>
                                <InputSelection
                                    disabled={false}
                                    items={this.state.ageData}
                                    placeholder={placeholderAge}
                                    onValueChange={(value) => {
                                        this.props.selectRegisterStep6AgeData(value)
                                    }}
                                    value={this.props.selectedRegisterStep6AgeData}
                                />
                                <Text style={{ ...textStyle, marginTop: 6, fontFamily: Fonts.semibold, textAlign: 'left', fontSize: 12 }}>{'You must be at least 18 years old to use eTherapyPro.'}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: 20, borderWidth: 0 }}>
                            <Text style={{ ...textStyle, marginTop: 25, marginBottom: 16 }}>{'What country'}</Text>
                            <View>
                                <InputSelection
                                    disabled={false}
                                    items={this.state.countryData}
                                    placeholder={placeholderCountry}
                                    onValueChange={(value) => {
                                        this.props.selectRegisterStep6CountryData(value)
                                    }}
                                    value={this.props.selectedRegisterStep6CountryData}
                                />
                                <Text style={{ ...textStyle, marginTop: 0, fontFamily: Fonts.semibold, textAlign: 'left', fontSize: 12 }}>{''}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: 20, borderWidth: 0 }}>
                            <Text style={{ ...textStyle, marginTop: 25, marginBottom: 16 }}>{'What state'}</Text>
                            <View>
                                <InputSelection
                                    disabled={false}
                                    items={this.state.stateData}
                                    placeholder={placeholderState}
                                    onValueChange={(value) => {
                                        this.props.selectRegisterStep6StateData(value)
                                    }}
                                    value={this.props.selectedRegisterStep6StateData}
                                />
                                <Text style={{ ...textStyle, marginTop: 6, fontFamily: Fonts.semibold, textAlign: 'left', fontSize: 12 }}>{''}</Text>
                            </View>
                        </View>
                    </View>
                    <RegisterStepBottomBg />
                    <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 80, marginTop: -50 }}>
                        <RegisterButton
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            height={vw(87)}
                            width={vw(87)}
                            borderRadius={vw(87) / 2}
                            onPress={this.goToStep7}
                        />
                    </View>
                    <View style={{ borderWidth: 0, height: 80, backgroundColor: Colors.whiteFour }} />
                </ScrollView>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    registrationStaticData: state.auth.registrationStaticData,
    selectedRegisterStep6AgeData: state.auth.selectedRegisterStep6AgeData,
    selectedRegisterStep6CountryData: state.auth.selectedRegisterStep6CountryData,
    selectedRegisterStep6StateData: state.auth.selectedRegisterStep6StateData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    selectRegisterStep6AgeData: (data) => {
        dispatch(selectRegisterStep6AgeData(data))
    },
    selectRegisterStep6CountryData: (data) => {
        dispatch(selectRegisterStep6CountryData(data))
    },
    selectRegisterStep6StateData: (data) => {
        dispatch(selectRegisterStep6StateData(data))
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep6AgeCountryState)