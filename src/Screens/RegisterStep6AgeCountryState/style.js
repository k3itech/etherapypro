import { StyleSheet } from 'react-native'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    innerPageContainer: {
        borderWidth: 0,
        borderColor: 'red',
        backgroundColor: Colors.whiteFour
    },
    textStyle: {
        fontFamily: Fonts.bold,
        fontSize: 22,
        color: Colors.whiteFour,
        textAlign: 'center'
    },
    rightIconBtnStyle: {
      position: 'absolute',
      right: 12,
      height: 45,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 0,
      marginTop: 20
    }
})

// make this component available to the app
export default styles