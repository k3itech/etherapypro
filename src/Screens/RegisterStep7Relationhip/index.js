//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView,
    FlatList
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import RegisterHeader from '../../Components/RegisterHeader'
import RegisterButton from '../../Components/RegisterButton'
import RegisterCheckboxItem from '../../Components/RegisterCheckboxItem'
import RegisterStepBottomBg from '../../Components/RegisterStepBottomBg'
import { selectRegisterStep7RelationshipData } from '../../Redux/Actions'
import CustomAlert from '../../Utility/CustomAlert'

// create a component
class RegisterStep7Relationhip extends Component {

    constructor(props) {
        super(props)

        this.state = {
            selectedItems: []
        }
    }

    renderItem = ({ item }) => {
        return (
            <RegisterCheckboxItem
                name={item}
                width={'50%'}
                isSelected={this.rowSelected(item)}
                onPress={() => this.selectItem(item)}
            />
        )
    }

    rowSelected = (item) => {
        if (this.props.selectedRegisterStep7RelationshipData == item) {
            return true
        }
        else {
            return false
        }
    }

    selectItem = (item) => {
        this.props.selectRegisterStep7RelationshipData(item)
    }

    goToStep8 = () => {
        if (this.props.selectedRegisterStep7RelationshipData == '' || this.props.selectedRegisterStep7RelationshipData == null || this.props.selectedRegisterStep7RelationshipData == undefined) {
            CustomAlert.alert('Please select your relationship status')
        }
        else {
            this.props.navigation.navigate('RegisterStep8Gender')
        }
    }    

    render() {
        const {
            textStyle,
            innerPageContainer
        } = styles
        return (
            <View style={{ backgroundColor: Colors.whiteFour, flex: 1 }}>
                <SafeAreaView></SafeAreaView>
                <RegisterHeader />
                <View style={innerPageContainer}>
                    <View style={{ backgroundColor: Colors.appGreen }}>
                        <View style={{ marginHorizontal: vw(40), borderWidth: 0 }}>
                            <Text style={{ ...textStyle, marginTop: 25 }}>{"What is your\nrelationship status?"}</Text>
                        </View>
                        <FlatList
                            style={{ borderWidth: 0, marginHorizontal: 16, marginTop: 20, marginBottom: 20 }}
                            data={this.props.registrationStaticData ? this.props.registrationStaticData.relationshipstatus : []}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => `${index}`}
                            numColumns={2}
                            extraData={this.state}
                        />
                    </View>
                    <RegisterStepBottomBg />
                    <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end', paddingRight: 80, marginTop: -50 }}>
                        <RegisterButton
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            height={vw(87)}
                            width={vw(87)}
                            borderRadius={vw(87) / 2}
                            onPress={this.goToStep8}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    registrationStaticData: state.auth.registrationStaticData,
    selectedRegisterStep7RelationshipData: state.auth.selectedRegisterStep7RelationshipData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    selectRegisterStep7RelationshipData: (data) => {
        dispatch(selectRegisterStep7RelationshipData(data))
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegisterStep7Relationhip)