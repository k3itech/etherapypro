//import liraries
import React, { Component } from 'react'
import {
    View,
    FlatList,
    SafeAreaView,
    Text, Alert
} from 'react-native'
import { connect } from 'react-redux'
import styles from './style'
import HeaderNew from '../../Components/HeaderNew'
import SettingsItem from '../../Components/SettingsItem'
import star from '../../assets/img/star.png'
import fullscreen from '../../assets/img/full-screen.png'
import account from '../../assets/img/account.png'
import adduser from '../../assets/img/add-user.png'
import bell from '../../assets/img/bell.png'
import heart from '../../assets/img/heart.png'
import password1 from '../../assets/img/password1.png'
import subscription from '../../assets/img/subscription.png'
import support from '../../assets/img/support.png'
import seen from '../../assets/img/seen.png'
import Colors from '../../common/Colors'
import Fonts from '../../common/Fonts'
import { vw } from '../../common/ViewportUnits'
import { APP_TITLE } from '../../Utility/Constants'
import Functions from '../../Utility/Functions'
import { LogoutSuccess } from '../../Redux/Actions'

// create a component
class Settings extends Component {

    constructor(props) {
        super(props)

        this.state = {
            fullScreen: false,
            readReceipts: true,
            appPasscode: false
        }
    }

    static getDerivedStateFromProps(props, state) {
        // Any time the current user changes,
        // Reset any parts of state that are tied to that user.

        if (props.userData == undefined) {
            props.navigation.popToTop()
            props.navigation.navigate('Auth', { screen: 'Welcome' })
        }

        return null
    }

    renderItem = ({ item }) => {
        return (
            <SettingsItem
                showSwith={item.isSwitch}
                itemLabel={item.name}
                isHeader={item.isHeader}
                itemImage={item.itemImage}
                isDEscription={item.isDEscription}
                description={item.description}
                switchValue={item.switchValue}
                onPress={() => this.onPress(item)}
                switchOnChange={(value) => { this.switchOnChange(value, item) }}
            />
        )
    }

    switchOnChange = (value, item) => {
        if (item.id == 3) {
            // full screen
            this.setState({
                fullScreen: value
            })
        }
        else if (item.id == 4) {
            // read receipts
            this.setState({
                readReceipts: value
            })
        }
        else if (item.id == 10) {
            // app passcode
            this.setState({
                appPasscode: value
            })
        }
    }

    onPress = (item) => {
        console.log('item = ', item);
        if (item.id == 2) {
            // starred messages
            this.props.navigation.navigate('StarredMessages')
        }
        else if (item.id == 6) {
            // account details
            this.props.navigation.navigate('AccountDetails')
        }
        else if (item.id == 7) {
            // manage subscription
            this.props.navigation.navigate('ManageSubscription')
        }
        else if (item.id == 8) {
            // notifications
        }
        else if (item.id == 11) {
            // change password
            this.props.navigation.navigate('ChangePassword')
        }
        else if (item.id == 12) {
            // rate the app
        }
        else if (item.id == 13) {
            // refer a friend
        }
        else if (item.id == 14) {
            // customer support
        }
    }

    logoutOnPress = () => {
        Alert.alert(
            APP_TITLE,
            'Do you really want to logout?',
            [
                {
                    text: 'Yes',
                    onPress: () => {
                        this.props.LogoutSuccess()
                    },
                    style: 'cancel'
                },
                {
                    text: 'No',
                    onPress: () => {

                    },
                    style: 'cancel'
                }
            ],
            { cancelable: false }
        )
    }

    render() {
        const { main } = styles
        let items = [
            {
                name: 'Messaging',
                id: 1,
                isHeader: true,
                isSwitch: false,
                itemImage: '',
                isDEscription: false,
                description: '',
                switchValue: false
            },
            {
                name: 'Starred messages',
                id: 2,
                isHeader: false,
                isSwitch: false,
                itemImage: star,
                isDEscription: false,
                description: '',
                switchValue: false
            },
            {
                name: 'Fullscreen writing',
                id: 3,
                isHeader: false,
                isSwitch: true,
                itemImage: fullscreen,
                isDEscription: false,
                description: '',
                switchValue: this.state.fullScreen
            },
            {
                name: 'Read receipts',
                id: 4,
                isHeader: false,
                isSwitch: true,
                itemImage: seen,
                isDEscription: true,
                description: 'By turning of read receipts, you\nwill not able to see when\nsomeone has read you',
                switchValue: this.state.readReceipts
            },
            {
                name: 'ACCOUNT',
                id: 5,
                isHeader: true,
                isSwitch: false,
                itemImage: star,
                isDEscription: false,
                description: '',
                switchValue: false
            },
            {
                name: 'Account details',
                id: 6,
                isHeader: false,
                isSwitch: false,
                itemImage: account,
                isDEscription: false,
                description: '',
                switchValue: false
            },
            {
                name: 'Manage subscription',
                id: 7,
                isHeader: false,
                isSwitch: false,
                itemImage: subscription,
                isDEscription: false,
                description: '',
                switchValue: false
            },
            {
                name: 'Notifications',
                id: 8,
                isHeader: false,
                isSwitch: false,
                itemImage: bell,
                isDEscription: false,
                switchValue: false
            },
            {
                name: 'Security',
                id: 9,
                isHeader: true,
                isSwitch: false,
                itemImage: star,
                isDEscription: false,
                description: '',
                switchValue: false
            },
            {
                name: 'App passcode',
                id: 10,
                isHeader: false,
                isSwitch: true,
                itemImage: password1,
                isDEscription: false,
                switchValue: this.state.appPasscode
            },
            {
                name: 'Change Password',
                id: 11,
                isHeader: false,
                isSwitch: false,
                itemImage: password1,
                isDEscription: false,
                switchValue: this.state.appPasscode
            },
            {
                name: 'Rate the app',
                id: 12,
                isHeader: false,
                isSwitch: false,
                itemImage: heart,
                isDEscription: false,
                description: '',
                switchValue: false
            },
            {
                name: 'Refer a friend',
                id: 13,
                isHeader: false,
                isSwitch: false,
                itemImage: adduser,
                isDEscription: false,
                description: '',
                switchValue: false
            },
            {
                name: 'Customer support',
                id: 14,
                isHeader: false,
                isSwitch: false,
                itemImage: support,
                isDEscription: false,
                description: '',
                switchValue: false
            }
        ]
        return (
            <>
                <SafeAreaView></SafeAreaView>
                <View style={{ height: 80 }}>
                    <HeaderNew
                        onPressLeftIcon={() => {
                            this.props.navigation.goBack()
                        }}
                        isTitle={true}
                        title={'Settings'}
                        isLeftIcon={true}
                    />
                </View>
                <View style={main}>
                    <FlatList
                        style={{ height: '100%', borderWidth: 0, marginTop: 10 }}
                        data={items}
                        renderItem={this.renderItem}
                        keyExtractor={(item) => `${item.id}`}
                        extraData={this.state}
                        ListFooterComponent={
                            <View>
                                <Text style={{ borderWidth: 0, textAlign: 'center', height: 60, lineHeight: 50, color: Colors.appGreen, borderBottomColor: 'rgba(0, 0, 0, 0.2)', fontFamily: Fonts.bold, fontSize: vw(16) }} onPress={this.logoutOnPress}>Log Out</Text>
                            </View>
                        }
                    />
                    <SafeAreaView></SafeAreaView>
                </View>
            </>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({
    LogoutSuccess: () => {
        dispatch(LogoutSuccess())
    }
})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Settings)