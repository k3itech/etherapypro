import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    main: {
        flex: 1,
        borderWidth: 0,
        flexDirection: 'column',
        justifyContent: 'space-between'
    }
})

// make this component available to the app
export default styles