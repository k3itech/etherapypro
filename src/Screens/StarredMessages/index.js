//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import RegisterButton from '../../Components/RegisterButton'
import HeaderNew from '../../Components/HeaderNew'
import favoriteImage from '../../assets/img/favorite.png'

// create a component
class StarredMessages extends Component {

    constructor(props) {
        super(props)

        this.state = {

        }
    }

    render() {
        const {
            main,
            innerPageContainer
        } = styles
        return (
            <View style={main}>
                <SafeAreaView></SafeAreaView>
                <View style={{ height: 80 }}>
                    <HeaderNew
                        onPressLeftIcon={() => {
                            this.props.navigation.goBack()
                        }}
                        isTitle={true}
                        title={'Starred messages'}
                        isLeftIcon={true}
                    />
                </View>
                <View style={innerPageContainer}>
                    <View style={{ marginHorizontal: 20 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 40 }}>
                            <RegisterButton
                                bgColor={null}
                                borderColor={null}
                                shadowColor={false}
                                height={vw(180)}
                                width={vw(200)}
                                disabled={true}
                                buttonImageWidth={200}
                                buttonImageHeight={180}
                                buttonImage={favoriteImage}
                            />
                        </View>
                        <Text style={{ fontSize: 22, fontFamily: Fonts.bold, textAlign: 'center', marginTop: 20 }}>{`You haven't starred\nany messages yet`}</Text>
                        <Text style={{ fontSize: 16, fontFamily: Fonts.semibold, textAlign: 'center', marginTop: 18, color: 'rgba(0, 0, 0, 0.7)' }}>{'Lorem ipsum dolor sit amet, labore\nconsectetur adipiscing elit, sed do et\neiusmod tempo ncididunt ut'}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StarredMessages)