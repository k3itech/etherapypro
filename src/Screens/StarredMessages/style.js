import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    innerPageContainer: {
        borderWidth: 0,
        borderColor: 'red'
    }
})

// make this component available to the app
export default styles