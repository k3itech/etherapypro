//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import {
    vw,
    vh
} from '../../common/ViewportUnits'
import PageContainer from '../../Components/PageContainer'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import Button from '../../Components/Button'
import UniveralImageBg from '../../Components/UniveralImageBg'
import welcome_bg from '../../assets/img/welcome_bg.png'

// create a component
class Welcome extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const {
            main,
            innerPageContainer
        } = styles
        return (
            <>
                <UniveralImageBg
                    isLogoShowing={true}
                    isTitleShowing={true}
                    height={'100%'}
                    bgImage={welcome_bg}
                />
                    <View style={main}>
                        <PageContainer padding={vw(0)}>
                            <View style={innerPageContainer}>
                                <Text style={{ textAlign: 'center', fontSize: vw(22), fontFamily: Fonts.bold }}>{'Chat with a licensed counselor today'}</Text>
                                <View style={{ height: vw(30) }} />
                                <Button
                                    text={'Get Started'}
                                    bgColor={Colors.appGreen}
                                    borderColor={Colors.appGreen}
                                    textColor={Colors.whiteFour}
                                    fontSize={vw(16)}
                                    fontFamily={Fonts.semibold}
                                    height={vw(45)}
                                    borderRadius={vw(5)}
                                    onPress={() => {
                                        // GetStarted RegisterStep1Feeling
                                        this.props.navigation.navigate('GetStarted')
                                    }}
                                />
                                <View style={{ height: vw(16) }} />
                                <Button
                                    text={'How it Works'}
                                    bgColor={Colors.whiteFour}
                                    borderColor={Colors.appGreen}
                                    textColor={Colors.appGreen}
                                    fontSize={vw(16)}
                                    height={vw(45)}
                                    fontFamily={Fonts.semibold}
                                    borderRadius={vw(5)}
                                    onPress={() => { }}
                                />
                                <View style={{ height: vw(36) }} />
                                <SafeAreaView></SafeAreaView>
                            </View>
                        </PageContainer>
                    </View>
            </>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Welcome)