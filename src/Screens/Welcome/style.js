import { StyleSheet } from 'react-native'
import { vw } from '../../common/ViewportUnits'

const styles = StyleSheet.create({
    main: {
        flex: 1,
        borderWidth: 0,
        position: 'absolute',
        width: '100%',
        bottom: 0
    },
    innerPageContainer: {
        paddingHorizontal: vw(32),
        flex: 1,
        justifyContent: 'flex-end'
    }
})

// make this component available to the app
export default styles