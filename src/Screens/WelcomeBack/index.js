//import liraries
import React, { Component } from 'react'
import {
    View,
    Text,
    SafeAreaView
} from 'react-native'
import { connect } from 'react-redux'
import { vw } from '../../common/ViewportUnits'
import styles from './style'
import Fonts from '../../common/Fonts'
import Colors from '../../common/Colors'
import RegisterButton from '../../Components/RegisterButton'
import Button from '../../Components/Button'
import HeaderNew from '../../Components/HeaderNew'
import ProgressImage from '../../assets/img/progress.png'

// create a component
class WelcomeBack extends Component {

    constructor(props) {
        super(props)

        this.state = {
            cardNumber: '',
            expiry: '',
            cvv: ''
        }
    }

    financialAidTapped = () => {

    }

    startCounselingOnPress = () => {

    }

    render() {
        const {
            main,
            innerPageContainer
        } = styles
        return (
            <View style={main}>
                <SafeAreaView></SafeAreaView>
                <View style={{ height: 80 }}>
                    <HeaderNew
                        onPressLeftIcon={() => {
                            this.props.navigation.goBack()
                        }}
                        isTitle={false}
                        isLeftIcon={true}
                    />
                </View>
                <View style={innerPageContainer}>
                    <View style={{ marginHorizontal: 20 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                            <RegisterButton
                                bgColor={Colors.blue}
                                borderColor={Colors.blue}
                                shadowColor={false}
                                height={vw(97)}
                                width={vw(97)}
                                borderRadius={vw(97) / 2}
                                disabled={true}
                                buttonImageWidth={42}
                                buttonImageHeight={60}
                                buttonImage={ProgressImage}
                            />
                        </View>
                        <Text style={{ fontSize: 22, fontFamily: Fonts.bold, textAlign: 'center', marginTop: 12 }}>{'Welcome back to\n EtherapyPro'}</Text>
                        <Text style={{ fontSize: 16, fontFamily: Fonts.semibold, textAlign: 'center', marginTop: 18, color: 'rgba(0, 0, 0, 0.7)' }}>{'Lorem ipsum dolor sit amet, labore\nconsectetur adipiscing elit, sed do et\neiusmod tempo ncididunt ut\nlabore dolore. Lorem ipsum dolor sit\namet, labore consectetur adipiscing\nelit, sed do et eiusmod tempo\nncididunt ut labore dolore.'}</Text>
                        <View style={{ height: 40 }} />
                        <Button
                            text={'Continue'}
                            bgColor={Colors.appGreen}
                            borderColor={Colors.appGreen}
                            textColor={Colors.whiteFour}
                            fontSize={vw(16)}
                            fontFamily={Fonts.semibold}
                            height={vw(45)}
                            borderRadius={vw(5)}
                            onPress={() => { }}
                        />
                        <View style={{ height: 40 }} />
                        <Button
                            text={'Chat with a matching agent'}
                            bgColor={null}
                            borderColor={Colors.appGreen}
                            textColor={Colors.appGreen}
                            fontSize={vw(16)}
                            fontFamily={Fonts.semibold}
                            height={vw(45)}
                            borderRadius={vw(5)}
                            onPress={() => { }}
                        />
                    </View>
                </View>
            </View>
        )
    }
}

// get state/props from redux state(Reducers)
const mapStateToProps = state => ({
    loading: state.auth.loading,
    userData: state.auth.userData
})

// function to call redux functions(Actions)
const mapDispatchToProps = (dispatch) => ({

})

// make this component available to the app
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WelcomeBack)