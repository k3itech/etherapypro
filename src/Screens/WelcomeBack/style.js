import { StyleSheet } from 'react-native'
import Colors from '../../common/Colors'

const styles = StyleSheet.create({
    main: {
        flex: 1
    },
    innerPageContainer: {
        borderWidth: 0,
        borderColor: 'red'
    }
})

// make this component available to the app
export default styles