import { Dimensions } from "react-native"

export const APP_TITLE = 'eTherapy'

export const PAGINATION_LIMT = 20
export const NUMBER_OF_RECORDS_LIMIT_CHAT = 20

// API url
export const BASE_URL = 'http://167.99.88.134:4003'
export const BASE_URL_SOCKET = 'ws://167.99.88.134:8080'

// DATE FORMATS
export const UNIVERSAL_DATE_FORMAT = 'DD MMM YYYY hh:mm A'
export const SMALL_DATE_FORMAT = 'DD MMM'
export const DATE_FORMAT = 'DD MMM YYYY'
export const TIME_FORMAT = 'hh:mm A'

// screen width and height
export const SCREEN_HEIGHT = Dimensions.get('window').height
export const SCREEN_WIDTH = Dimensions.get('window').width

export const NETWORK_MESSAGE = 'Unable to connect with the server. Check your internet connection and try again.'

// global msg
export const GLOBAL_ERROR_MSG = 'Some error occured'
export const OK_TITLE = 'Ok'

// chat message types
export const TEXT_MESSAGE_TYPE = 'text'
export const AUDIO_MESSAGE_TYPE = 'audio'
export const VIDEO_MESSAGE_TYPE = 'video'
export const IMAGE_MESSAGE_TYPE = 'image'