import { Component } from 'react'
import { Alert } from "react-native"
import {
    APP_TITLE,
    OK_TITLE
} from './Constants'

// create a component for showing custom alert
class CustomAlert extends Component {

    //Show alert message
    static alert(message) {
        Alert.alert(
            APP_TITLE,
            message,
            [
                {
                    text: OK_TITLE,
                    onPress: () => {

                    }
                }
            ],
            { cancelable: false }
        )
    }
}

// make this component available to the app
export default CustomAlert