import messaging from '@react-native-firebase/messaging'
import firebase from '@react-native-firebase/app'
import PushNotification from 'react-native-push-notification'
import AsyncStorage from '@react-native-community/async-storage'
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import store from './ReduxStore'
import { setAlreadyLoginUserData } from '../Redux/Actions'
import Functions from './Functions'
import { Platform } from 'react-native'

// create a component for firebase notifications 
export default class Firebase {

    async checkPermission() {
        this.requestPermission()
        this.configurePushNotifications()
    }

    async requestPermission() {
        const authStatus = await messaging().requestPermission();
        console.log('authStatus = ', authStatus);

        const enabled = authStatus === 1 || authStatus === 2;
        console.log('enabled = ', enabled);

        if (enabled) {
            console.log('Authorization status:', authStatus);
            this.getToken()
        }
    }

    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        console.log('fcmToken = ', fcmToken);
        if (!fcmToken) {
            //fcmToken = await firebase.messaging().ios.getAPNSToken()
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                // user has a device token
                console.log('getToken() fcmToken = ', fcmToken);
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
            else {
                console.log('getToken() fcmtoken error = ', getToken);
            }
        }
        else {
            console.log('fcmToken = ', fcmToken);
        }
    }

    async configurePushNotifications() {
        PushNotification.configure({
            onNotification: (notification) => {
                console.log("NOTIFICATION:", notification);
                this.backgroundModeNotificationClick(notification)
            }
        })
    }

    async createNotificationListeners() {
        messaging().onMessage(async remoteNotification => {
            console.log('messaging().onMessage notification = ', remoteNotification);

            let { data, notification, messageId } = remoteNotification

            let title = Platform.OS == 'android' ? notification.title : notification.title
            let messageBody = Platform.OS == 'android' ? notification.body ? notification.body : '' : notification.body ? notification.body : ''

            console.log('title = ', title);
            console.log('body = ', messageBody);

            if (global.received_push_notification_id) {
                if (global.received_push_notification_id == messageId) {

                }
                else {
                    this.presentLocalNotification(title, messageBody, data, messageId)
                }
            }
            else {
                this.presentLocalNotification(title, messageBody, data, messageId)
            }
        })

        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log('messaging().onNotificationOpenedApp Notification caused app to open from background state:', remoteMessage);
            this.backgroundModeNotificationClick(remoteMessage)
        })

        // Check whether an initial notification is available
        messaging().getInitialNotification().then(remoteMessage => {
            if (remoteMessage) {
                console.log('messaging().getInitialNotification Notification caused app to open from quit state:', remoteMessage);
                this.backgroundModeNotificationClick(remoteMessage)
            }
        })

        // Register background handler
        messaging().setBackgroundMessageHandler(async remoteMessage => {
            console.log('messaging().setBackgroundMessageHandler Message handled in the background!', remoteMessage);
        })
    }

    presentLocalNotification = (title, messageBody, data, messageId) => {
        if (Platform.OS == 'android') {
            global.received_push_notification_id = messageId
            PushNotification.localNotification({
                autoCancel: true,
                largeIcon: "ic_launcher",
                smallIcon: "ic_launcher",
                priority: "high",
                importance: "high",
                ignoreInForeground: false,
                title: title,
                message: messageBody,
                userInfo: data
            })
        }
        else {
            global.received_push_notification_id = messageId
            PushNotificationIOS.presentLocalNotification({
                alertTitle: title,
                alertBody: messageBody,
                userInfo: data
            });
        }
    }

    async backgroundModeNotificationClick(notification) {

        console.log('backgroundModeNotificationClick = ', notification);

        if (global.user_token == undefined || global.user_token == '' || global.user_token == null) {
            this.checkUserLoggedInOrNot(notification)
        }
        else {
            this.redirectToScreens(notification)
        }
    }

    checkUserLoggedInOrNot = (notification) => {
        setTimeout(() => {
            // check user is signed in or not
            Functions.isSignedIn().then((result) => {
                if (!result || result == null) {

                }
                else {
                    Functions.getUserToken().then(token => {
                        console.log('token = ', token);

                        if (token == null || token == undefined || token == '') {

                        }
                        else {
                            global.user_token = token

                            store.dispatch(setAlreadyLoginUserData(JSON.parse(result), token))
                            Functions.connectToSocketMethod()

                            this.redirectToScreens(notification)
                        }
                    })
                }
            })
        }, 1000);
    }

    redirectToScreens = (notification) => {
        console.log('redirectToScreens = ', notification);

        let data = notification.userInfo ? notification.userInfo : notification.data
        console.log('data = ', data);
        console.log('data.type = ', data.type);
    }
}