import { Platform } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import NetInfo from '@react-native-community/netinfo'
import ImagePicker from 'react-native-image-picker'
import KeyboardManager from 'react-native-keyboard-manager'
import CustomAlert from './CustomAlert'
import {
    SCREEN_WIDTH,
    NETWORK_MESSAGE,
    BASE_URL
} from './Constants'
import {
    startSocketIO,
    connectSocket,
    disconnectSocket,
    joinEmit
} from './Socket'
import store from './ReduxStore'

var axios = require('axios')

// Add a response interceptor
axios.interceptors.response.use(
    response => {
        console.log(response, 'request interceptor response')
        // Do something with response data
        return response
    },
    error => {
        // Do something with response error
        return Promise.reject(error)
    }
)

// create a component for global functions that are used from anywhere throuout the app
export default class Functions {

    static USER_LOGGEDIN_KEY = 'isLoggedIn'
    static USER_DATA_KEY = 'userData'
    static USER_TOKEN_KEY = 'userToken'
    static USER_REMEMBER_KEY = 'userRemember'

    /**
     * axios -> POST API method
     */
    static postApi(url, data = {}, headers = {}) {

        headers = {
            ...headers,
            'os': Platform.OS,
            'version': global.app_version,
            'language': 'en'
        }

        console.log('headers = ', headers)
        console.log('parameters = ', data)
        console.log('post url - ', `${BASE_URL}${url}`)

        return axios.post(`${BASE_URL}${url}`, data, { headers: headers })
    }

    /**
     * axios -> GET API method
     */
    static getApi(url, headers = {}) {

        headers = {
            ...headers,
            'os': Platform.OS,
            'version': global.app_version,
            'language': 'en',
            'Authorization': global.user_token
        }

        console.log('headers = ', headers)
        console.log('get url - ', `${BASE_URL}${url}`)

        return axios.get(`${BASE_URL}${url}`, { headers: headers })
    }

    /**
     * axios -> PUT API method
     */
    static putApi(url, data = {}, headers = {}) {

        headers = {
            ...headers,
            'os': Platform.OS,
            'version': global.app_version,
            'language': 'en'
        }
        console.log('headers = ', headers)
        console.log('parameters = ', data)
        console.log('put url - ', `${BASE_URL}${url}`)

        return axios.put(`${BASE_URL}${url}`, data, { headers: headers })
    }

    /**
     * axios -> DELETE API method
     */
    static deleteApi(url, headers = {}) {

        headers = {
            ...headers,
            'os': Platform.OS,
            'version': global.app_version,
            'language': 'en',
            'Authorization': global.user_token
        }

        console.log('headers = ', headers)
        console.log('delete url - ', `${BASE_URL}${url}`)

        return axios.delete(`${BASE_URL}${url}`, { headers: headers })
    }

    /**
     * axios -> PATCH API method
     */
    static patchApi(url, data = {}, headers = {}) {

        headers = {
            ...headers,
            'os': Platform.OS,
            'version': global.app_version,
            'language': 'en'
        }

        console.log('headers = ', headers)
        console.log('parameters = ', data)
        console.log('patch url - ', `${BASE_URL}${url}`)

        return axios.patch(`${BASE_URL}${url}`, data, { headers: headers })
    }

    /**
     * check if user is signed in
     */
    static onSignIn = (user) => {
        Functions.setStorage(Functions.USER_LOGGEDIN_KEY, 'true')
        if (user) {
            Functions.setStorage(Functions.USER_DATA_KEY, user)
        }
    }

    /**
     * save user remembered data
     */
    static rememberUser = (username) => {
        Functions.setStorage(Functions.USER_REMEMBER_KEY, username)
    }

    /**
     * save user token
     */
    static saveUserToken = (userToken) => {
        Functions.setStorage(Functions.USER_TOKEN_KEY, userToken)
    }

    // delete logged in user data from async storage when user is logging out
    static onSignOut = () => {
        AsyncStorage.removeItem(Functions.USER_LOGGEDIN_KEY)
        AsyncStorage.removeItem(Functions.USER_DATA_KEY)
        AsyncStorage.removeItem(Functions.USER_TOKEN_KEY)

        global.user_token = undefined
        global.backgroundUnixTime = 0
        global.received_push_notification_id = undefined

        disconnectSocket()
    }

    // check user is signed in or not
    static isSignedIn = () => {
        return AsyncStorage.getItem(Functions.USER_DATA_KEY)
    }

    // delete logged in user data from async storage when user is logging out
    static deleteUserRememberData = () => {
        AsyncStorage.removeItem(Functions.USER_REMEMBER_KEY)
    }

    // check user is signed in or not
    static getRememberUserData = () => {
        return AsyncStorage.getItem(Functions.USER_REMEMBER_KEY)
    }

    /**
     * get user token
     */
    static getUserToken = () => {
        return AsyncStorage.getItem(Functions.USER_TOKEN_KEY)
    }

    // function to set async storage value
    static setStorage = async (key, value) => {
        try {
            value = value;
            await AsyncStorage.setItem(key, value)
        }
        catch (error) {
            // Error saving data
        }
    }

    // function to get async storage value
    static getStorage = async (key) => {
        try {
            const value = await AsyncStorage.getItem(key)
            if (value !== null) {
                // We have data!!
                return value;
            }
            else {
                return false
            }
        }
        catch (error) {
            // Error retrieving data
            return false
        }
    }

    // Check network connectivity
    static isNetworkConnected() {
        return new Promise(
            function (isNetwork) {
                NetInfo.fetch().then(state => {
                    if (state.isConnected) {
                        isNetwork(true)
                    }
                    else {
                        CustomAlert.alert(NETWORK_MESSAGE)
                        isNetwork(false)
                    }
                })
            }
        )
    }

    //Image picker dialog to select file
    static imagePickerLibrary = (mediaType) => {
        var options = {
            title: 'Select Image',
            mediaType: mediaType,
            videoQuality: 'medium',
            allowsEditing: false,
            durationLimit: 30,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            },
            cancelButtonTitle: 'Cancel',
            takePhotoButtonTitle: undefined,
            chooseFromLibraryButtonTitle: 'Choose from Library'
        }

        return new Promise(
            function (isPicked) {
                ImagePicker.launchImageLibrary(options, response => {
                    console.log('Response imagePickerLibrary launchImageLibrary = ', response)
                    if (response.didCancel) {
                        console.log('imagePickerLibrary launchImageLibrary User canceled image picker')
                        isPicked({
                            isPicked: false,
                            data: undefined
                        })
                    }
                    else if (response.error) {
                        console.log('imagePickerLibrary launchImageLibrary Error: ', response.error)
                        isPicked({
                            isPicked: false,
                            data: undefined
                        })
                    }
                    else {
                        isPicked({
                            isPicked: true,
                            data: response
                        })
                    }
                })
            }
        )
    }

    //Image picker dialog to select file
    static openImagePicker = () => {
        var options = {
            title: 'Select Image',
            mediaType: 'photo',
            videoQuality: 'medium',
            allowsEditing: false,
            durationLimit: 30,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            },
            cancelButtonTitle: 'Cancel',
            takePhotoButtonTitle: 'Take Photo',
            chooseFromLibraryButtonTitle: 'Choose from Library'
        }

        return new Promise(
            function (isPicked) {
                ImagePicker.showImagePicker(options, response => {
                    console.log('Response = ', response)
                    if (response.didCancel) {
                        console.log('User canceled image picker')
                        isPicked({
                            isPicked: false,
                            data: undefined
                        })
                    }
                    else if (response.error) {
                        console.log('Error: ', response.error)
                        isPicked({
                            isPicked: false,
                            data: undefined
                        })
                    }
                    else {
                        isPicked({
                            isPicked: true,
                            data: response
                        })
                    }
                })
            }
        )
    }

    //Image picker dialog to capture camera picture
    static imagePickerCamera = (mediaType) => {
        var options = {
            title: 'Select Image',
            mediaType: mediaType,
            videoQuality: 'medium',
            allowsEditing: false,
            durationLimit: 30,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            },
            cancelButtonTitle: 'Cancel',
            takePhotoButtonTitle: 'Take Photo',
            chooseFromLibraryButtonTitle: undefined
        }

        return new Promise(
            function (isPicked) {
                ImagePicker.launchCamera(options, response => {
                    console.log('Response launchCamera = ', response)
                    if (response.didCancel) {
                        console.log('User canceled image picker')
                        isPicked({
                            isPicked: false,
                            data: undefined
                        })
                    }
                    else if (response.error) {
                        console.log('launchCamera Error: ', response.error)
                        isPicked({
                            isPicked: false,
                            data: undefined
                        })
                    }
                    else {
                        isPicked({
                            isPicked: true,
                            data: response
                        })
                    }
                })
            }
        )
    }

    /**
     * resize image width height according to screen size at upload time
     */
    static getImageResizeDataShow = (data) => {
        let dataRerturn = {
            width: SCREEN_WIDTH - 100,
            height: (data.height / 2) > 300 ? 300 : (data.height / data.width) * (SCREEN_WIDTH - 100)
        }
        return dataRerturn
    }

    /**
     * resize image width height according to screen size at showing time
     */
    static getImageResizeHeight = (data) => {
        let width = 0
        let height = 0

        if (data.isVertical) {
            width = data.width
            height = data.height
        }
        else {
            width = data.height
            height = data.width
        }

        if (width > SCREEN_WIDTH) {
            let dataRerturn = {
                width: SCREEN_WIDTH,
                height: (height / width) * (SCREEN_WIDTH)
            }
            return dataRerturn
        }
        else {
            let dataRerturn = {
                width: width,
                height: height
            }
            return dataRerturn
        }
    }

    /**
     * enable Keyboard Manager in iOS
     */
    static enableKeyboardManager = () => {
        if (Platform.OS == 'ios') {
            KeyboardManager.setEnable(true)
            KeyboardManager.setEnableAutoToolbar(true)
        }
    }

    /**
     * disable Keyboard Manager in iOS
     */
    static disableKeyboardManager = () => {
        if (Platform.OS == 'ios') {
            KeyboardManager.setEnable(false)
            KeyboardManager.setEnableAutoToolbar(false)
        }
    }

    /**
     * connect to Socket
     */
    static connectToSocketMethod = () => {
        startSocketIO()
        connectSocket()
    }
}