import socketIO from 'socket.io-client'
import { BASE_URL, BASE_URL_SOCKET } from './Constants'
import {
    globalSocketConnectionStatusUpdate,
    connectedToSocket,
    saveConnectedSocketID,
    getChatHistoryData
} from '../Redux/Actions'
import store from './ReduxStore'

// Initialize Socket IO:
const socket = socketIO(BASE_URL_SOCKET, {
    transports: ['websocket'],
    jsonp: false,
    // reconnection: true
})

// export the function to connect and use socket IO:
export const startSocketIO = () => {

    connectSocket()

    // reconnect socket listener
    socket.on('reconnect', (attemptNumber) => {
        console.log('reconnect attemptNumber = ', attemptNumber)
    })

    // connecting socket listener
    socket.on('connecting', () => {
        console.log('connecting to socket')
    })

    // connect socket listener
    socket.on('connect', () => {
        console.log('connection to socket success.')
        store.dispatch(globalSocketConnectionStatusUpdate(1))
        store.dispatch(connectedToSocket())
    })

    // disconnect socket listener 
    socket.on('disconnect', () => {
        console.log('connection to server lost.')
        store.dispatch(globalSocketConnectionStatusUpdate(0))
        if (store.getState().auth.userData) {
            connectSocket()
        }
    })

    socket.on('get-status', (response) => {
        console.log('get-status response = ', response);
        store.dispatch(saveConnectedSocketID(response.socket_id))
    })

    socket.on('get-message-success', (response) => {
        console.log('get-message-success response = ', response)
        store.dispatch(getChatHistoryData(response.message))
    })
}
//
// connect socket
export const connectSocket = () => {
    console.log('socket.connected = ', socket.connected);
    // if (socket.connected == false) {
    socket.open()
    socket.connect()
    // }
}

// disconnect socket
export const disconnectSocket = () => {
    socket.disconnect()
}

export const joinChat = () => {
    console.log('join-chat emit called');

    let joinID = 'r12467544ut4'//store.getState().socket.connectedSocketID
    let user_id = store.getState().auth.userData.userDetails._id

    console.log('joinID = ', joinID);
    console.log('user_id = ', user_id);

    socket.emit('join-chat', { 'joinId': joinID, 'user_id': user_id })
}

export const sendMessageEmit = (counsellor_id, counsellorname, message, message_type) => {
    console.log('create-message emit called');

    let joinID = 'r12467544ut4'//store.getState().socket.connectedSocketID
    let user_id = store.getState().auth.userData.userDetails._id
    let username = store.getState().auth.userData.userDetails.username

    console.log('joinID = ', joinID)
    console.log('user_id = ', user_id)
    console.log('username = ', username)
    console.log('counsellor_id = ', counsellor_id)
    console.log('counsellorname = ', counsellorname)
    console.log('message = ', message)
    console.log('message_type = ', message_type)

    socket.emit('create-message', { 'user_id': user_id, 'username': username, 'counsellor_id': counsellor_id, 'counsellorname': counsellorname, 'joinId': joinID, 'message': message, 'message_type': message_type, 'id': user_id })
}