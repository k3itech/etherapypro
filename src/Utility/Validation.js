import { Component } from 'react'
import CustomAlert from './CustomAlert'
import moment from 'moment'

// create a component for form validations
class Validation extends Component {

    // validation for signin form
    static signInFormValidation(state) {

        let isValid = true

        var username = state.username != null ? String(state.email).trim() : state.username

        var password = state.password != null ? String(state.password).trim() : state.password

        let message = ''

        if (username == null || username == '') {
            isValid = false
            message = 'Please enter Username'
        }
        else if (password == null || password == '') {
            isValid = false
            message = 'Please enter Password'
        }
        else if (password.length < 6) {
            isValid = false
            message = 'Minimum Password lenght is 6 character, Please enter long password'
        }

        if (!isValid) {
            CustomAlert.alert(message)
            return {
                error: true
            }
        }
        else {
            return {
                error: false
            }
        }
    }

    // validation for forgot password form
    static forgotPasswordFormValidation(state) {

        let isValid = true

        var name = state.name != null ? String(state.name).trim() : state.name

        var email = state.email != null ? String(state.email).trim() : state.email

        var password = state.password != null ? String(state.password).trim() : state.password

        var confirmPassword = state.confirmPassword != null ? String(state.confirmPassword).trim() : state.confirmPassword

        var email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        let message = ''

        if (name == null || name == '') {
            isValid = false
            message = 'Please enter Name'
        }
        else if (email == null || email == '') {
            isValid = false
            message = 'Please enter Email'
        }
        else if (!email_regex.test(email)) {
            isValid = false
            message = 'Please enter valid Email'
        }
        else if (password == null || password == '') {
            isValid = false
            message = 'Please enter Password'
        }
        else if (password.length < 6) {
            isValid = false
            message = 'Minimum Password lenght is 6 character, Please enter long password'
        }
        else if (confirmPassword == null || confirmPassword == '') {
            isValid = false
            message = 'Please enter Confirm Password'
        }
        else if (password != confirmPassword) {
            isValid = false
            message = `Password didn't matched`
        }

        if (!isValid) {
            CustomAlert.alert(message)
            return {
                error: true
            }
        }
        else {
            return {
                error: false
            }
        }
    }

    // validation for register form
    static registerFormValidation(state) {

        let isValid = true

        var userName = state.userName != null ? String(state.userName).trim() : state.userName

        var email = state.email != null ? String(state.email).trim() : state.email

        var password = state.password != null ? String(state.password).trim() : state.password

        var confirmPassword = state.confirmPassword != null ? String(state.confirmPassword).trim() : state.confirmPassword

        var mobileNo = state.mobileNo != null ? String(state.mobileNo).trim() : state.mobileNo

        var email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        let message = ''

        if (userName == null || userName == '') {
            isValid = false
            message = 'Please enter user name'
        }
        else if (email == null || email == '') {
            isValid = false
            message = 'Please enter Email'
        }
        else if (!email_regex.test(email)) {
            isValid = false
            message = 'Please enter valid Email'
        }
        else if (mobileNo == null || mobileNo == '') {
            isValid = false
            message = 'Please enter Mobile Number'
        }
        else if (mobileNo.length < 8) {
            isValid = false
            message = 'Please enter valid Mobile Number'
        }
        else if (password == null || password == '') {
            isValid = false
            message = 'Please enter Password'
        }
        else if (password.length < 6) {
            isValid = false
            message = 'Minimum Password lenght is 6 character, Please enter long password'
        }
        else if (confirmPassword == null || confirmPassword == '') {
            isValid = false
            message = 'Please enter Confirm Password'
        }
        else if (password != confirmPassword) {
            isValid = false
            message = `Password didn't matched`
        }

        if (!isValid) {
            CustomAlert.alert(message)
            return {
                error: true
            }
        }
        else {
            return {
                error: false
            }
        }
    }

    // validation for change password form
    static changePasswordFormValidation(state) {

        let isValid = true

        var currentPassword = state.currentPassword != null ? String(state.currentPassword).trim() : state.currentPassword

        var newPassword = state.newPassword != null ? String(state.newPassword).trim() : state.newPassword

        var confirmPassword = state.confirmPassword != null ? String(state.confirmPassword).trim() : state.confirmPassword

        let message = ''

        if (currentPassword == null || currentPassword == '') {
            isValid = false
            message = 'Please enter Current Password'
        }
        else if (newPassword == null || newPassword == '') {
            isValid = false
            message = 'Please enter New Password'
        }
        else if (newPassword.length < 6) {
            isValid = false
            message = 'Minimum Password lenght is 6 character, Please enter long password'
        }
        else if (confirmPassword == null || confirmPassword == '') {
            isValid = false
            message = 'Please enter Confirm Password'
        }
        else if (confirmPassword != newPassword) {
            isValid = false
            message = `Password didn't matched`
        }

        if (!isValid) {
            CustomAlert.alert(message)
            return {
                error: true
            }
        }
        else {
            return {
                error: false
            }
        }
    }

    // validation for add card details
    static addCardDetailsFormValidation(state) {

        let isValid = true

        var cardHolderName = state.cardHolderName != null ? String(state.cardHolderName).trim() : state.cardHolderName

        var email = state.email != null ? String(state.email).trim() : state.email

        var cardNumber = state.cardNumber != null ? String(state.cardNumber).trim() : state.cardNumber

        var expiryDate = state.expiry != null ? String(state.expiry).trim() : state.expiry

        var cvvText = state.cvv != null ? String(state.cvv).trim() : state.cvv

        var email_regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        var validExpiry = true

        if (expiryDate.length == 4) {
            let enteredMonth = expiryDate.slice(NaN, 2)
            console.log('enteredMonth = ', enteredMonth)

            let enteredYear = expiryDate.slice(2)
            console.log('enteredYear = ', enteredYear)

            let day = moment().date()
            console.log('day= =', day)
            
            console.log('selected date = ', moment(`20${enteredYear}-${enteredMonth}`))
            
            if (moment(`20${enteredYear}-${enteredMonth}`).isValid() == true) {
                console.log('valid')

                let year = moment().year()
                let month = moment().month()

                console.log('year = ', year.toString().slice(2))
                console.log('month = ', month)

                if (enteredYear > year.toString().slice(2)) {
                    console.log('large -> valid')
                    validExpiry = true
                }
                else {
                    if (enteredYear == year.toString().slice(2)) {
                        console.log('equal')
                        if (enteredMonth > month) {
                            console.log('month -> valid')
                            validExpiry = true
                        }
                        else {
                            console.log('month -> not valid')
                            validExpiry = false
                        }
                    }
                    else {
                        console.log('small -> not valid')
                        validExpiry = false
                    }
                }
            }
            else {
                console.log('not valid -> not valid')
                validExpiry = false
            }
        }
        else {
            validExpiry = false
        }

        let message = ""

        if (cardHolderName == null || cardHolderName == "") {
            isValid = false
            message = 'Please enter Card Holder Name'
        }
        else if (email == null || email == '') {
            isValid = false
            message = 'Please enter Email'
        }
        else if (!email_regex.test(email)) {
            isValid = false
            message = 'Please enter valid Email'
        }
        else if (cardNumber == null || cardNumber == "") {
            isValid = false
            message = 'Please enter Card Number'
        }
        else if (cardNumber.length < 16) {
            isValid = false
            message = 'Please enter 16 digit Card Number'
        }
        else if (expiryDate == null || expiryDate == "") {
            isValid = false
            message = 'Please enter Expiry Date'
        }
        else if (expiryDate.length < 4) {
            isValid = false
            message = 'Please enter 4 digit Expiry Date'
        }
        else if (validExpiry == false) {
            isValid = false
            message = 'Please enter valid Expiry Date';
        }
        else if (cvvText == null || cvvText == "") {
            isValid = false
            message = 'Please enter CVV'
        }
        else if (cvvText.length < 3) {
            isValid = false
            message = 'Please enter 3 digit CVV'
        }

        if (!isValid) {
            CustomAlert.alert(message)
            return { 
                error: true 
            }
        }
        else {
            return { 
                error: false 
            }
        }
    }
}

// make this component available to the app
export default Validation