import { Component } from 'react'

export default class Colors extends Component {

    static whiteFour = '#ffffff'
    static black = '#000'

    static appGreen = '#0E9748'
    static appGreenLight = 'rgba(14, 151, 72, 0.1)'
    static appDarkGreen = '#135F39'
    
    static shadowColor = '#00000029'

    static blue = '#2197F1'
    static blue1 = '#2a3f6f'

    static gray = '#cccccc'
    static gray1 = '#cbcbcf'
    static gray2 = '#f8f8fc'
    static gray3 = '#747474'
    static gray4 = '#e9e9ef'
    static gray5 = '#eeeeef'
    static gray6 = '#31384a'
    static gray7 = '#e7e7e7'
    static gray8 = '#848484'
    static gray9 = '#f7f7fa'
    static gray10 = '#aeaeb1'

    static slateGrey = 'rgb(47, 66, 112)'

    static dark = '#222222'
    static dark1 = '#2f2f2f'

    static inputBoxPlaceholderColor = 'rgba(47, 66, 112, 0.5)'
}