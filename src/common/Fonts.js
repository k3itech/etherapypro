import { Component } from 'react'

export default class Fonts extends Component {
    static light = 'OpenSans-Light'
    static regular = 'OpenSans'
    static semibold = 'OpenSans-Semibold'
    static bold = 'OpenSans-Bold'
    static extrabold = 'OpenSans-Extrabold'
}