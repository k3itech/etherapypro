import { Dimensions } from 'react-native'

export const vw = number => Dimensions.get('window').width * (number / 375)
export const vh = number => Dimensions.get('window').height * (number / 667)